﻿using System;
using System.Collections.Generic;

namespace BST
{
    public class BinarySearchTree<T> : IBinarySearchTree<T> where T : IComparable<T>
    {
        private T value;
        private IBinarySearchTree<T> left;
        private IBinarySearchTree<T> right;

        public BinarySearchTree(T value, IBinarySearchTree<T> left, IBinarySearchTree<T> right)
        {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        public BinarySearchTree(T value)
        {
            this.value = value;
            this.left = null;
            this.right = null;
        }

        public T Value
        {
            get => this.value;
        }

        public IBinarySearchTree<T> Left
        {
            get => this.left;
        }

        public IBinarySearchTree<T> Right
        {
            get => this.right;
        }

        public int Height
        {
            get
            {
                return GetHeight(this);
            }
        }
        public int GetHeight(IBinarySearchTree<T> root)
        {
            return Math.Max(root.Left != null ? GetHeight(root.Left) : -1,
                    root.Right != null ? GetHeight(root.Right) : -1)
                    + 1;
        }

        public IList<T> GetInOrder()
        {
            List<T> list = new List<T>();
            if (this.left == null && this.right == null)
            {
                list.Add(value);
                return list;
            }
            if (this.value != null)
            {
                list.AddRange(this.left.GetInOrder());
                list.Add(this.value);
                if (this.right != null)
                {
                    list.AddRange(this.right.GetInOrder());
                }
            }
            return list;
        }

        public IList<T> GetPostOrder()
        {
            List<T> list = new List<T>();
            if (this.left == null && this.right == null)
            {
                list.Add(this.value);
                return list;
            }
            if (this.value != null)
            {
                list.AddRange(this.left.GetPostOrder());

                if (this.right != null)
                {
                    list.AddRange(this.right.GetPostOrder());
                }
                list.Add(this.value);
            }
            return list;
        }

        public IList<T> GetPreOrder()
        {
            List<T> list = new List<T>();
            if (this.left == null && this.right == null)
            {
                list.Add(this.value);
                return list;
            }
            if (this.value != null)
            {
                list.Add(this.value);
                list.AddRange(this.left.GetPreOrder());

                if (this.right != null)
                {
                    list.AddRange(this.right.GetPreOrder());
                }

            }
            return list;
        }

        public IList<T> GetBFS()
        {
            List<T> list = new List<T>();
            Queue<IBinarySearchTree<T>> queue = new Queue<IBinarySearchTree<T>>();
            queue.Enqueue(this);

            while (queue.Count != 0)
            {
                var current = queue.Dequeue();
                list.Add(current.Value);
                if (current.Left != null)
                {
                    queue.Enqueue(current.Left);
                }
                if (current.Right != null)
                {
                    queue.Enqueue(current.Right);
                }
            }
            return list;
        }

        public void Insert(T element)
        {

            if (this.value == null)
            {
                BinarySearchTree<T> tree = new BinarySearchTree<T>(element);
            }

            else
            {
                if (this.value.CompareTo(element) == 1)
                {
                    if (this.left == null)
                    {
                        this.left = new BinarySearchTree<T>(element);
                    }
                    else
                    {
                        this.left.Insert(element);
                    }
                }
                else
                {
                    if (this.right == null)
                    {
                        this.right = new BinarySearchTree<T>(element);
                    }
                    else
                    {
                        this.right.Insert(element);
                    }
                }
            }
        }

        public bool Search(T element)
        {
            if (this.value == null)
            {
                return false;
            }

            if (this.value.CompareTo(element) == -1)
            {
                if (this.right != null)
                {
                    return this.right.Search(element);
                }
                else
                {
                    return false;
                }
            }

            if (this.value.CompareTo(element) == 1)
            {
                if (this.left != null)
                {
                    return this.left.Search(element);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        // Advanced task!
        //public bool Remove(T value)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
