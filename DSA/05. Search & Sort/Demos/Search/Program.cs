﻿using System;

namespace DSA
{
    class Program
    {
        static void Main()
        {
            int[] array = new int[10] { 1, 8, 0, 4, 5, 7, 2, 3, 6, 9 };
            int target = 9;

            // Linear search
            Console.WriteLine($"Linear search: {LinearSearch(array, target)}");

            // Binary search
            // NOTE: Remember that a binary search algorithm requires a sorted array
            Array.Sort(array);

            Console.WriteLine($"Binary search: {BinarySearchIterative(array, target)}");
        }

        // Linear search
        static string LinearSearch(int[] array, int target)
        {
            for (int index = 0; index < array.Length; index++)
            {
                int current = array[index];
                if (current == target)
                {
                    return $"The target ({target}) was found after {index + 1} steps! Total number of steps: {array.Length}";
                }
            }

            return $"The target ({target}) wasn't found! Steps performed: {array.Length}";
        }

        // Binary search (iterative approach)
        static string BinarySearchIterative(int[] array, int target)
        {
            int left = 0;
            int right = array.Length - 1;

            int stepsCounter = 0;
            while (left <= right)
            {
                stepsCounter++;

                int middle = (left + right) / 2;

                if (array[middle] == target)
                {
                    return $"The target ({target}) was found after {stepsCounter} steps! Total number of steps: {array.Length}";
                }

                if (array[middle] < target)
                {
                    left = middle + 1;
                }
                else if (array[middle] > target)
                {
                    right = middle - 1;
                }
            }

            return $"The target ({target}) wasn't found! Steps performed: {array.Length}";
        }

        static bool BinarySearchRecursive(int[] array, int target, int leftIndex, int rightIndex)
        {
            if (leftIndex > rightIndex)
            {
                return false;
            }

            int middleIndex = (leftIndex + rightIndex) / 2;

            if (array[middleIndex] == target)
            {
                return true;
            }

            if (array[middleIndex] > target)
            {
                return BinarySearchRecursive(array, target, leftIndex, rightIndex: middleIndex - 1);
            }
            else if (array[middleIndex] < target)
            {
                return BinarySearchRecursive(array, target, leftIndex: middleIndex + 1, rightIndex);
            }

            throw new Exception("Should never get here");
        }
    }
}
