﻿namespace DSA.Searching
{
    public class LinearSearch
    {
        public static bool Contains(int[] array, int target)
        {
            foreach (var item in array)
            {
                if (item == target)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
