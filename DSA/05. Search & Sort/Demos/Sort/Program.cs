﻿using DSA.Sorting;
using System;
using System.Diagnostics;
using System.Linq;

namespace DSA
{
    class Program
    {
        static void Main()
        {
            var array = GetRandomArray(desiredLength: 100);

            int[] carbonCopy1 = new int[array.Length];
            int[] carbonCopy2 = new int[array.Length];
            int[] carbonCopy3 = new int[array.Length];
            int[] carbonCopy4 = new int[array.Length];
            int[] carbonCopy5 = new int[array.Length];

            Array.Copy(array, carbonCopy1, array.Length);
            Array.Copy(array, carbonCopy2, array.Length);
            Array.Copy(array, carbonCopy3, array.Length);
            Array.Copy(array, carbonCopy4, array.Length);
            Array.Copy(array, carbonCopy5, array.Length);

            var timer = new Stopwatch();
            timer.Start();
            Array.Sort(carbonCopy1);
            timer.Stop();
            Console.WriteLine($"    Array.Sort: {timer.Elapsed}");

            timer = new Stopwatch();
            timer.Start();
            Quicksort.Sort(carbonCopy2);
            timer.Stop();
            Console.WriteLine($"    Quick Sort: {timer.Elapsed}");

            timer = new Stopwatch();
            timer.Start();
            MergeSort.Sort(carbonCopy3);
            timer.Stop();
            Console.WriteLine($"    Merge Sort: {timer.Elapsed}");

            timer = new Stopwatch();
            timer.Start();
            SelectionSort.Sort(carbonCopy4);
            timer.Stop();
            Console.WriteLine($"Selection Sort: {timer.Elapsed}");

            timer = new Stopwatch();
            timer.Start();
            BubbleSort.Sort(carbonCopy5);
            timer.Stop();
            Console.WriteLine($"   Bubble Sort: {timer.Elapsed}");
        }
        static int[] GetRandomArray(int desiredLength = 100)
        {
            Random rnd = new Random();
            
            return Enumerable
                .Range(1, desiredLength)
                .Select(x => rnd.Next(0, int.MaxValue))
                .ToArray();
        }
    }
}
