﻿using System;
using System.Linq;

namespace CoffeOrderChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            var takeOut = new int[] { 1, 3, 5 };
            var dineIn = new int[] { 2, 4, 6 };
            var servedOrders = new int[] { 1, 2, 3, 5, 4, 6 };

            Console.WriteLine($"Is orders served in correct order: {CheckOrders(takeOut, dineIn, servedOrders)}");
        }
        private static bool CheckOrders(int[] takeOut, int[] dineIn, int[] served)
        {
            if (served.Length == 0)
            {
                return true;
            }

            if (takeOut.Length > 0 && takeOut[0] == served[0])
            {
                return CheckOrders(RemoveFirstFromArray(takeOut), dineIn, RemoveFirstFromArray(served));
            }

            if (dineIn.Length > 0 && dineIn[0] == served[0])
            {
                return CheckOrders(takeOut, RemoveFirstFromArray(dineIn), RemoveFirstFromArray(served));
            }
            return false;
        }

        private static int[] RemoveFirstFromArray(int[] array)
        {
            int[] newArray = new int[array.Length - 1];
            Array.Copy(array, 1, newArray, 0, array.Length - 1);
            return newArray;
        }
    }
}

