﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InflightEntertainment
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter fligth length: ");
            int fligthLength = int.Parse(Console.ReadLine());
            Console.Write($"Enter sequence of movies: ");
            int[] movies = Console.ReadLine().Split().Select(int.Parse).ToArray();

        }

        static bool CheckTimes(int[] movieTimes, int flightLength)
        {
            var movieLengthsSeen = new HashSet<int>();

            foreach (int firstMovieLength in movieTimes)
            {
                int matchingSecondMovieLength = flightLength - firstMovieLength;
                if (movieLengthsSeen.Contains(matchingSecondMovieLength))
                {
                    return true;
                }
                else
                {
                    movieLengthsSeen.Add(firstMovieLength);
                }
            }

            return false;
        }
    }
}

