﻿using System;

namespace LinkedListCycles
{
    class Program
    {
        static void Main(string[] args)
        {
            Node node = CreateCirclyLinkedList();

            Console.WriteLine($"Is current linked list cycle: {CheckIsLinkedListCycle(node)}");
        }

        static bool CheckIsLinkedListCycle(Node head)
        {
            Node currentNode = head;
            Node fast = currentNode;
            Node slow = currentNode;
            while (slow != null && fast != null && fast.Next != null)
            {
                slow = slow.Next;
                fast = fast.Next.Next;

                // If slow and fast meet at same
                // point then loop is present
                if (slow == fast)
                {
                    return true;
                }
            }
            return false;
        }

        static Node CreateCirclyLinkedList()
        {
            Node head = new Node(1);
            Node currentNode = head;
            for (int i = 2; i <= 6; i++)
            {
                currentNode.Next = new Node(i);
                currentNode = currentNode.Next;
            }

            currentNode.Next = head;
            return head;
        }
        class Node
        {
            public Node(int value)
            {
                this.Value = value;
            }
            public int Value
            {
                get;
                private set;
            }
            public Node Next
            {
                get;
                set;
            }
        }
    }
}
