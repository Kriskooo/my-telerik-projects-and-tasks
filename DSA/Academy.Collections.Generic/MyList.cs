﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Academy.Collections.Generic
{
    public class MyList<T> : IEnumerable<T>
    {
        private T[] items;
        private int length;
        private int count = 0;

        public MyList()
        {

            this.Capacity = 4;
            items = new T[Capacity];
        }

        public MyList(int capacity)
        {
            this.Capacity = capacity;
            items = new T[Capacity];
        }

        public int Count
        {
            get
            {
                return this.count;
            }
        }


        public int Capacity
        {
            get
            {
                return this.length;
            }
            private set
            {
                this.length = value;
            }
        }

        public T this[int i]
        {
            get { return this.items[i]; }
            private set
            {
                this.items[i] = value;
                this.length++;
            }
        }

        /// <summary>
        /// Adds a item to the collection.
        /// </summary>
        /// <param name="value">Add object to the collection.</param>
        /// <returns>void</returns>
        public void Add(T value)
        {
            this.items[count] = value;
            count++;

            if (this.count == Capacity)
            {
                Resize();
            }
        }

        /// <summary>
        /// Resize default array capacity when it is full. The new capacity will be multiplay by 2
        /// </summary>
        /// <returns>void</returns>
        private void Resize()
        {
            this.Capacity *= 2;
            var temp = new T[Capacity];
            items = temp;
        }

        /// <summary>
        /// Remove Item in collection if it exist
        /// </summary>
        /// <param name="item"></param>
        /// <returns>True if item exist and it is removed or False when item does not exist</returns>
        public bool Remove(T item)
        {
            bool isTrue = false;
            for (int i = 0; i < count; i++)
            {
                if (items[i].Equals(item))
                {
                    isTrue = true;
                    if (i == items.Length - 1)
                    {
                        items[i] = default;
                        count--;
                        var temp = new T[Capacity];
                        items = temp;
                    }
                    else
                    {
                        items[i] = items[i + 1];
                        count--;
                        var temp = new T[Capacity];
                        items = temp;
                    }
                }
            }

            return isTrue;
        }

        /// <summary>
        /// Check the array from 0 to n if given item in the array.
        /// </summary>
        /// <param name="item"></param>
        /// <returns> Retunr index if it exists or -1 when it is not</returns>
        public int IndexOf(T item)
        {
            for (int i = 0; i < count; i++)
            {
                if (items[i].Equals(item))
                {
                    return i;
                }

            }
            return -1;
        }

        /// <summary>
        /// Check the array from n to 0 if given item exist in the array
        /// </summary>
        /// <param name="item"></param>
        /// <returns> Return index if it exists or -1 when it is not</returns>
        public int LastIndexOf(T item)
        {
            for (int i = count - 1; i >= 0; i--)
            {
                if (items[i].Equals(item))
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Remove Item on given index
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Throw the exception if given index is out if range</returns>
        public void RemoveAt(int index)
        {
            for (int i = index; i < count; i++)
            {
                if (i == items.Length - 1)
                {
                    items[i] = default;
                }

                items[i] = items[i + 1];
            }
            count--;
        }

        /// <summary>
        /// Counter start from 0
        /// </summary>
        public void Clear()
        {
            items = new T[Capacity];
            count = 0;
        }

        public void Populate(T value)
        {
            for (int i = 0; i < Capacity; i++)
            {
                items[i] = value;
            }
        }

        /// <summary>
        /// Assigns the given value of type T to each element of the specified array.
        /// </summary>
        /// <param name="value"></param>
        public void Swap(int index0, int index1)
        {
            var temp = items[index0];
            items[index0] = items[index1];
            items[index1] = temp;
        }

        public bool Contains(T value)
        {
            foreach (var item in items)
            {
                if (item.Equals(value))
                {
                    return true;
                }
            }
            return false;

        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }
    }
}
