﻿namespace HashSet.Skeleton
{
	public interface IHashSet<T>
	{
		//
		// Summary:
		//     Adds an item to the current set and returns a value 
		//     to indicate if the item was successfully added.
		//
		// Parameters:
		//     item: The element to add to the set.
		//
		// Returns:
		//     true if the element is added to the set.
		//     false if the element is already in the set.
		bool Add(T item);

		//
		// Summary:
		//     Determines whether the set contains a specific item.
		//
		// Returns:
		//     true if item is found in the set; otherwise, false.
		bool Contains(T item);

		//
		// Summary:
		//     Removes the first occurrence of the item from the set.
		//
		// Parameters:
		//     item: The item to remove from the set.
		//
		// Returns:
		//     true if item was successfully removed from the set.
		//     false if item is not found in the set.
		bool Remove(T item);

		//
		// Summary:
		//     Returns the number of elements contained in the set.
		int Count { get; set; }
	}
}
