﻿using System;
using System.Collections.Generic;

namespace HashSet.Skeleton
{
	public class MyHashSet<T> : IHashSet<T>
	{
		private List<T> myHashSet = new List<T>(); 
			
		public MyHashSet()
		{
			 
		}
		public MyHashSet(int initialBucketsLength)
		{
			this.Count = initialBucketsLength;
		}

		public bool Add(T item)
		{
			bool result = false;

			if(!myHashSet.Contains(item))
			{
				myHashSet.Add(item);
				result = true;
				this.Count++;
			}

			return result;
		}

		public bool Contains(T item)
		{
			bool result = false;

			if (myHashSet.Contains(item))
			{
				result = true;
			}

			return result;

		}

		public bool Remove(T item)
		{
			bool result = false;

			if(myHashSet.Contains(item))
			{
				result = true;
				myHashSet.Remove(item);
				this.Count--;
			}

			return result;
		}

		public int Count
		{
			get;
			set;
		}
	}
}
