﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA_FinalPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            var numberOfSoldiers = int.Parse(Console.ReadLine());
            var soldiers = Console.ReadLine().Split();
            var sb = new StringBuilder();

            var sergeants = new Queue<string>();
            var corporals = new Queue<string>();
            var privates = new Queue<string>();


            for (int i = 0; i < numberOfSoldiers; i++)
            {
                if (soldiers[i][0] == 'S')
                {
                    sergeants.Enqueue(soldiers[i]);
                }

                else if (soldiers[i][0] == 'C')
                {
                    corporals.Enqueue(soldiers[i]);
                }

                else
                {
                    privates.Enqueue(soldiers[i]);
                }
            }

            foreach (var soldier in sergeants)
            {
                sb.Append($"{soldier} ");
            }

            foreach (var soldier in corporals)
            {
                sb.Append($"{soldier} ");
            }

            foreach (var soldier in privates)
            {
                sb.Append($"{soldier} ");
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
