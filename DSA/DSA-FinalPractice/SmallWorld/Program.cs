﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmallWorld
{
    struct Coordinates
    {
        public Coordinates(int row, int col)
        {
            this.Row = row;
            this.Col = col;
        }

        public int Row { get; }
        public int Col { get; }

    }
    class Program
    {
        static int[][] matrix;

        static int[] mDimenstions;

        static List<int> sizes = new List<int>();

        static Coordinates point;

        static int counter = 0;
        static int currentNum = -1;
        static void Main(string[] args)
        {

            mDimenstions = Console.ReadLine().Split().Select(int.Parse).ToArray();
            matrix = new int[mDimenstions[0]][];
            for (int i = 0; i < mDimenstions[0]; i++)
            {
                matrix[i] = Console.ReadLine().ToCharArray().Select(x => int.Parse(x.ToString())).ToArray();
            }
            StartPath(0, 0);
            foreach (var item in sizes.OrderByDescending(x => x))
            {
                Console.WriteLine(item);
            }

        }

        private static void StartPath(int row, int col)
        {
            if (point.Row == -1)
            {
                return;
            }
            currentNum = matrix[point.Row][point.Col];


            if (currentNum == 0)
            {
                point = GetIndex(point.Row, point.Col);
                StartPath(point.Row, point.Col);
            }
            else
            {
                Trace(point.Row, point.Col);


                sizes.Add(counter);

                counter = 0;

                point = GetIndex(point.Row, point.Col);
                StartPath(point.Row, point.Col);
            }
        }

        private static void Trace(int row, int col)
        {
            if (!Range(row, col))
            {
                return;
            }
            if (matrix[row][col] == 0)
            {
                return;
            }

            matrix[row][col] = 0;
            counter++;

            Trace(row, col - 1);
            Trace(row - 1, col);
            Trace(row, col + 1);
            Trace(row + 1, col);
        }

        private static Coordinates GetIndex(int row, int col)
        {
            if (Range(row, col + 1))
            {
                return new Coordinates(row, col + 1);
            }
            else
            {
                if (Range(row + 1, col))
                {
                    return new Coordinates(row + 1, 0);
                }
                else
                {
                    return new Coordinates(-1, -1);
                }
            }
        }

        static bool Range(int row, int col)
        {
            bool rowInRange = row >= 0 && row < mDimenstions[0];
            bool colInRange = col >= 0 && col < mDimenstions[1];
            return rowInRange && colInRange;
        }
    }
}

