﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] number = Console.ReadLine().Split().Select(int.Parse).ToArray();
            var output = new Dictionary<int, int>();
            int n = number[1];
            int k = number[0];
            int curr = 1;
            int secCur = 1;

            output.Add(curr++, k);

            while (true)
            {
                if (output.ContainsKey(n))
                {
                    Console.WriteLine(output[n]);
                    break;
                }
                output.Add(curr++, output[secCur] + 1);
                output.Add(curr++, (2 * output[secCur]) + 1);
                output.Add(curr++, output[secCur] + 2);
                secCur++;
            }
        }
    }
}
