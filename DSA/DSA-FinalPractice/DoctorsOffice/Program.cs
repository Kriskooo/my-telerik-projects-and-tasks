﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoctorsOffice
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> pacient = new List<string>();
            Dictionary<string, int> names = new Dictionary<string, int>();
            StringBuilder sb = new StringBuilder();

            string input = string.Empty;
            while ((input = Console.ReadLine()) != "End")
            {
                string[] token = input.Split();
                if (token[0] == "Append")
                {
                    string name = token[1];
                    if (!names.ContainsKey(name))
                    {
                        names[name] = 0;
                    }
                    pacient.Add(name);
                    names[name] += 1;
                    sb.AppendLine("OK");
                }
                else if (token[0] == "Insert")
                {
                    string name = token[2];
                    int position = int.Parse(token[1]);
                    if (position > pacient.Count)
                    {
                        sb.AppendLine("Error");
                        continue;
                    }

                    if (!names.ContainsKey(name))
                    {
                        names[name] = 0;
                    }

                    pacient.Insert(position, name);
                    names[name] += 1;
                    sb.AppendLine("OK");
                }
                else if (token[0] == "Find")
                {
                    string name = token[1];
                    if (!names.ContainsKey(name))
                    {
                        sb.AppendLine("0");
                        continue;
                    }

                    sb.AppendLine(names[name].ToString());
                }
                else if (token[0] == "Examine")
                {
                    int count = int.Parse(token[1]);
                    if (count > pacient.Count)
                    {
                        sb.AppendLine("Error");
                        continue;
                    }

                    var removedPacient = pacient.Take(count).ToList();
                    pacient.RemoveRange(0, count);
                    foreach (var pac in removedPacient)
                    {
                        names[pac] -= 1;
                    }

                    sb.AppendLine(string.Join(" ", removedPacient));
                }
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
