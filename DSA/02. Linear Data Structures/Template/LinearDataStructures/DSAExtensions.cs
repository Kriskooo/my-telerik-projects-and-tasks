﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinearDataStructures.Common;

namespace LinearDataStructures.Extensions
{
    public static class DSAExtensions
    {
        public static bool AreListsEqual<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2)
        {
            Node<T> a = list1.Head, b = list2.Head;

            while (a != null && b != null)
            {
                if (!(a.Value.Equals(b.Value)))
                {
                    return false;
                }
                a = a.Next;
                b = b.Next;
            }

            return (a == null && b == null);
        }

        public static Node<T> FindMiddleNode<T>(SinglyLinkedList<T> list)
        {
            int count = 0;
            Node<T> mid = list.Head;
            Node<T> head = list.Head;

            while (head != null)
            {
                if ((count % 2) == 1)
                {
                    mid = mid.Next;
                }

                count++;
                head = head.Next;
            }
            return mid;
        }

        public static SinglyLinkedList<T> MergeLists<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2) where T : IComparable
        {
            SinglyLinkedList<T> sorted = new SinglyLinkedList<T>();
            Node<T> list1Node = list1.Head, list2Node = list2.Head;

            while (list1Node != null || list2Node != null)
            {
                if (list1Node == null)
                {
                    sorted.AddFirst(list2Node.Value);
                    list2Node = list2Node.Next;
                    continue;
                }

                if (list2Node == null)
                {
                    sorted.AddFirst(list1Node.Value);
                    list1Node = list1Node.Next;
                    continue;
                }

                if (list1Node.Value.CompareTo(list2Node.Value) == 0)
                {
                    sorted.AddFirst(list1Node.Value);
                    sorted.AddFirst(list2Node.Value);
                    list1Node = list1Node.Next;
                    list2Node = list2Node.Next;
                }

                else if (list1Node.Value.CompareTo(list2Node.Value) == -1)
                {
                    sorted.AddFirst(list1Node.Value);
                    list1Node = list1Node.Next;
                }

                else
                {
                    sorted.AddFirst(list2Node.Value);
                    list2Node = list2Node.Next;
                }
            }

            return ReverseList(sorted);
        }

        public static SinglyLinkedList<T> ReverseList<T>(SinglyLinkedList<T> list)
        {
            SinglyLinkedList<T> reverseList = new SinglyLinkedList<T>();

            Node<T> node = list.Head;

            while (node != null)
            {
                reverseList.AddFirst(node.Value);
                node = node.Next;
            }

            return reverseList;
        }

        public static bool AreValidParentheses(string expression)
        {
            char[] symbol = expression.ToCharArray();
            SinglyLinkedList<char> brackets = new SinglyLinkedList<char>();


            foreach (var symbols in symbol)
            {
                var lastBrackets = brackets.Head;
                if (lastBrackets == null && (symbols == ')' || symbols == '('))
                {
                    if (symbols == ')')
                    {
                        return false;
                    }

                    brackets.AddFirst(symbols);
                    continue;
                }

                if (symbols == '(')
                {
                    if (lastBrackets.Value == ')')
                    {
                        return false;
                    }

                    brackets.AddFirst(symbols);
                    continue;
                }

                if (symbols == ')')
                {
                    if (lastBrackets.Value == '(')
                    {
                        brackets.RemoveFirst();
                        continue;
                    }

                    if (lastBrackets.Value == ')')
                    {
                        return false;
                    }

                }
            }

            if (brackets.Head != null)
            {
                return false;
            }

            return true;
        }

        public static string RemoveBackspaces(string sequence, char backspaceChar)
        {
            var stack = new Stack<char>();
            foreach (var c in sequence)
            {
                if (c != backspaceChar)
                {
                    stack.Push(c);
                }
                else
                {
                    if (stack.Count > 0)
                    {
                        stack.Pop();
                    }
                }
            }

            return string.Join("", stack.Reverse());
        }
    }
}
