using Academy.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Add_Should
    {
        [TestMethod]
        public void CorrectlyAddItem()
        {
            //Arrange
            var list = new MyList<int>();

            //Act
            list.Add(5);

            //Assert
            Assert.AreEqual(1, list.Count);
        }

        [TestMethod]
        public void ResizeWhenCollectionIsFull()
        {
            //Arrange
            var list = new MyList<int>();

            //Act
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);

            //Assert
            Assert.AreEqual(8, list.Capacity);
        }
    }
}
