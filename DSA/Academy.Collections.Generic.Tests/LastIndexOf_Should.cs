﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class LastIndexOf_Should
    {
        [TestMethod]
        public void ReturnCorrectValue()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(1);

            //Act
            var result = list.LastIndexOf(1);

            //Assert
            Assert.AreEqual(2, result);
        }
    }
}
