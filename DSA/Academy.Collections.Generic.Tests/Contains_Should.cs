﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Contains_Should
    {
        [TestMethod]
        public void ReturnCorrectBool()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            //Act & Assert
            Assert.AreEqual(true, list.Contains(3));
            Assert.AreEqual(false, list.Contains(15));
            Assert.AreEqual(true, list.Contains(1));
            Assert.AreEqual(false, list.Contains(4));
        }
    }
}