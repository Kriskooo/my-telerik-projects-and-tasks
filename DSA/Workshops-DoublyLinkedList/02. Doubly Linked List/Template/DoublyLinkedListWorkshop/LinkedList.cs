﻿using System;
using System.Collections;

namespace DoublyLinkedListWorkshop
{
    public class LinkedList<T> : IList<T>
    {
        private Node head;
        private Node tail;

        public T Head
        {
            get
            {
                if (this.head == null)
                {
                    throw new InvalidOperationException();
                }

                return this.head.Value;
            }
        }

        public T Tail
        {
            get
            {
                if (this.tail == null)
                {
                    throw new InvalidOperationException();
                }
                return this.tail.Value;
            }
        }

        public int Count { get; private set; }


        public void AddFirst(T value)
        {
            Node node = new Node(value);

            if (this.head == null)
            {
                this.head = node;
                this.tail = node;
            }
            else
            {
                node.Next = this.head;
                this.head.Prev = node;
                this.head = node;
            }

            this.Count++;
        }

        public void AddLast(T value)
        {
            Node node = new Node(value);
            if (this.tail == null)
            {
                this.head = node;
                this.tail = node;
            }
            else
            {
                node.Prev = this.tail;
                this.tail.Next = node;
                this.tail = node;
            }

            this.Count++;
        }

        public void Add(int index, T value)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (index <= 0)
            {
                this.AddFirst(value);
            }

            else if (index >= this.Count - 1)
            {
                this.AddLast(value);
            }

            else
            {
                Node node = this.head;
                int indexer = 0;
                while (indexer != index)
                {
                    node = node.Next;
                    indexer++;
                }

                Node newNode = new Node(value);
                newNode.Next = node;
                newNode.Prev = node.Prev;
                node.Prev.Next = newNode;
                node.Prev = newNode;
                this.Count++;
            }
        }

        public T Get(int index)
        {
            if (index < 0 || index > this.Count - 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            int indexer = 0;
            Node node = this.head;
            while (indexer != index)
            {
                node = node.Next;
                indexer++;
            }

            return node.Value;
        }

        public int IndexOf(T value)
        {
            if (this.Count == 0)
            {
                return -1;
            }

            int index = 0;
            Node node = this.head;

            while (index < this.Count)
            {
                if (node.Value.Equals(value))
                {
                    return index;
                }

                node = node.Next;
                index++;
            }

            return -1;
        }

        public T RemoveFirst()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException();
            }

            T value = this.head.Value;
            if (Count == 1)
            {
                this.head = null;
                this.tail = null;
            }
            else
            {
                this.head = this.head.Next;
                this.head.Prev = null;
            }

            this.Count--;
            return value;
        }

        public T RemoveLast()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException();
            }

            T value = this.tail.Value;
            if (Count == 1)
            {
                this.head = null;
                this.tail = null;
            }
            else
            {
                this.tail = this.tail.Prev;
                this.head.Next = null;
            }

            this.Count--;
            return value;
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator()
        {
            return new ListEnumerator(this.head);
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((System.Collections.Generic.IEnumerable<T>)this).GetEnumerator();
        }

        // Use private nested class so that LinkedList users
        // don't know about the LinkedList internal structure
        private class Node
        {
            public Node(T value)
            {
                this.Value = value;
            }

            public T Value
            {
                get;
                private set;
            }

            public Node Next
            {
                get;
                set;
            }

            public Node Prev
            {
                get;
                set;
            }
        }

        // List enumerator that enables traversing all nodes of a list in a foreach loop
        private class ListEnumerator : System.Collections.Generic.IEnumerator<T>
        {
            private Node start;
            private Node current;

            internal ListEnumerator(Node head)
            {
                this.start = head;
                this.current = null;
            }

            public T Current
            {
                get
                {
                    if (this.current == null)
                    {
                        throw new InvalidOperationException();
                    }
                    return this.current.Value;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (current == null)
                {
                    current = this.start;
                    return true;
                }

                if (current.Next == null)
                {
                    return false;
                }

                current = current.Next;
                return true;
            }

            public void Reset()
            {
                this.current = null;
            }
        }
    }
}