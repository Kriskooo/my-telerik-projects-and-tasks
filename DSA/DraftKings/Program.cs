﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FindPeaks
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] findPeaks = Console.ReadLine().Split(new char[] { '[', ',', ']' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            List<string> result = new List<string>();

            for (int i = 0; i < findPeaks.Length; i++)
            {
                int left = int.MinValue;
                if (i > 0)
                {
                    left = findPeaks[i - 1];
                }
                int curr = findPeaks[i];
                int right = int.MinValue;

                if (i <= findPeaks.Length - 2)
                {
                    right = findPeaks[i + 1];
                }
                if (curr > left && curr > right)
                {
                    result.Add($"{ i},{ findPeaks[i]}");
                }
            }
            Console.WriteLine(string.Join(";", result));
        }
    }
}
