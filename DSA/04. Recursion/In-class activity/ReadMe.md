<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Recursion Tasks

## 1. Solve expression

You are given an expression in the format {number}{operator}({expression}).
The expression will always be valid, you have to calculate it.

|  Input | Output |
|---|---|
| 45+(55) | 100 | 
| 45+(24*(12+3)) | 405 |  
| 12*(35-(46*(5+15))) | -10620 | 

## 2. Print HTML

In the project template, you will find class `HtmlNode`. The class has a name, a list of attributes, and a list of child nodes. Print the sample root node in the following format (ignore the colors):

```html
<html>
    <head>
        <title></title>
    </head>
    <body class="container">
        <div class="navbar-container" id="navbar"></div>
        <div class="main-container" id="main-container"></div>
    </body>
</html>
```