﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackQueueWorkshop.Queue
{
    public class LinkedQueue<T> : IQueue<T>
    {
        private Node<T> head, tail;
        private int size = 0;

        public int Size => this.size;

        public bool IsEmpty
        {
            get
            {
                if (this.head == null)
                {
                    return true;
                }
                return false;
            }
        }

        public void Enqueue(T element)
        {
            Node<T> tempNode = new Node<T>();
            tempNode.Data = element;

            if (tail == null)
            {
                this.head = tempNode;
                this.tail = tempNode;
            }

            else
            {
                this.tail.Next = tempNode;
                this.tail = tempNode;
            }           
            size++;
        }

        public T Dequeue()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }

            T dequeue = this.head.Data;
            this.head = default;            
            size--;

            return dequeue;
        }

        public T Peek()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }
            T pop = this.head.Data;

            return pop;
        }
    }
}
