﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackQueueWorkshop.Queue
{
    public class ArrayQueue<T> : IQueue<T>
    {
        private T[] items = new T[4];
        private int tail = 0;

        public int Size => this.tail;

        public bool IsEmpty
        {
            get
            {
                if (this.tail - 1 < 0)
                {
                    return true;
                }

                return false;
            }
        }

        public void Enqueue(T element)
        {
            if (items.Length - 1 == tail)
            {
                Array.Resize(ref items, items.Length * 2);
            }

            items[tail] = element;
            tail++;
        }

        public T Dequeue()
        {
            if (tail == 0)
            {
                throw new InvalidOperationException();
            }

            T value = this.items[0];
            return value;
        }

        public T Peek()
        {
            if (tail == 0)
            {
                throw new InvalidOperationException();
            }

            T value = this.items[0];
            return value;
        }
    }
}
