﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackQueueWorkshop.Stack
{
    public class LinkedStack<T> : IStack<T>
    {
        private Node<T> top;
        private int size = 0;

        public int Size => this.size;


        public bool IsEmpty
        {
            get
            {
                if (this.top == null)
                {
                    return true;
                }
                return false;
            }
        }

        public void Push(T element)
        {
            Node<T> tempNode = new Node<T>();
            tempNode.Data = element;
            tempNode.Next = top;
            top = tempNode;
            size++;
        }

        public T Pop()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }

            T pop = this.top.Data;
            this.top = default;
            size--;
            return pop;
        }

        public T Peek()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }
            T pop = this.top.Data;
            //this.top = default;
            //size--;
            return pop;
        }
    }
}
