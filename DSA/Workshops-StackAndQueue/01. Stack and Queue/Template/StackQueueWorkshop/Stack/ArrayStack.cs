﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackQueueWorkshop.Stack
{
    public class ArrayStack<T> : IStack<T>
    {
        private T[] items = new T[4];
        private int top = 0;

        public int Size { get => this.top; }

        public bool IsEmpty
        {
            get
            {
                return top == 0;
            }
        }

        public void Push(T element)
        {
            if (items.Length - 1 == top)
            {
                Array.Resize(ref items, items.Length * 2);
            }

            items[top] = element;
            top++;
        }

        public T Pop()
        {
            if (top == 0)
            {
                throw new InvalidOperationException();
            }
            top--;
            //var poped = items[top];
            //items[top] = default;
            //return poped;
            return items[top];
        }

        public T Peek()
        {
            if (top == 0)
            {
                throw new InvalidOperationException();
            }
            return items[top - 1];
        }
    }
}

