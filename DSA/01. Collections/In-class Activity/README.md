<img src="https://i.imgur.com/yqIN5FX.png" width="300px" />

# Generic List Implementation

### 1. Description
Your task is to implement your own version of a generic list. Feel free to draw ideas and inspiration from the built-in [System.Collections.Generic.List\<T\>](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=netcore-3.0) and try to create your own generic list that behaves similarly. 

To get started, remember the fact that the [System.Collections.Generic.List\<T\>](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=netcore-3.0) internally uses an array to store and manipulate items. 

We've provided you with a template with 3 projects: 
- Generic dll - this is where your List implementation should go and essentially the only place you'll write code in. For the most part only method signature is provided. **Your task is to complete each method provided as well as to introduce a constructor. Feel free to add fields and/or properties if you need to. You must also document your implementation using XML tags**(Reference to the ```Add()``` method for an example).
- CLI dll - it contains our ```Main()``` method. Use it if you would like to test your implementation with any additional features that are not covered in the Tests project. 
- Tests dll - it contains tests that will help you figure out whether you're on the right track or not when implementing your list.

Following is the set of features your list should provide. **Please go through the whole document and read all requirements and tips carefully before writing any code.** After you've covered all the requirements, feel free to implement anything you think would be interesting and challenging.

### 2. Implement a constructor
#### Description
Initialize a new instance of the `MyList<T>` class that is empty and has **default initial capacity of 4**. You can use an int constant which is set as the capacity of the internal array if no value is passed. However, if a value is passed to the constructor its value should be used instead of the default capacity value. See this example:
```cs
var list = new MyList<int>();
// list.Count is 0
// list.Capacity is 4

var list2 = new MyList<int>(8);
// list2.Count is 0
// list.Capacity is 8
```

### 3. Count property
#### Description
Gets the number of elements contained in the `MyList<T>`.
```cs
public int Count { get; }
```
### 4. Capacity property
#### Description
Gets or sets the total number of elements the internal data structure can hold without resizing.

```cs
public int Capacity { get; set; }
```

### 5. Add method
#### Description
Adds an object to the end of `MyList<T>`.

```cs
public void Add (T item);
```
> **Notes** If the internal array of `MyList<T>` is full, you should _resize_ in order to make room for future additions. If this seems complicated keep reading through this document, there are hints in regards to resizing an array towards the end.

```cs
var list = new MyList<int>();
list.Add(3);
list.Add(1);
list.Add(4);
list.Add(2); // list.Capacity is 4
list.Add(5); // list.Capacity is 8
```

> **Hint** > The value can be `null` for reference types.

### 6. Contains method
#### Description
Determines whether an element is in the `MyList<T>`.

```cs
public bool Contains (T item);
```
Usage example:
```cs
list.Add(1);
list.Add(2);
list.Add(3);

list.Contains(2); // True
```

### 7. IndexOf method
#### Description
Searches for the specified object and returns the index of the first occurrence within the entire `MyList<T>`.
```cs
public int IndexOf (T item);
```
Usage example:
```cs
list.Add("Cat");
list.Add("Dog");
list.Add("Dog");
list.Add("Mouse");
list.Add("Cat");

list.IndexOf("Dog"); // 1
```

### 8. LastIndexOf method
#### Description
Searches for the specified object and returns the index of the last occurrence within the entire `MyList<T>`.
```cs
public int LastIndexOf (T item);
```
Usage example:
```cs
list.Add("Cat");
list.Add("Dog");
list.Add("Dog");
list.Add("Mouse");
list.Add("Cat");

list.LastIndexOf("Cat"); // 4
```
### 9. Remove method
#### Description
Removes the first occurrence of a specific object from the `MyList<T>`.
```cs
public bool Remove (T item);
```

> **Note** > Returns `true` if `item` is successfully removed; otherwise, `false`. This method also returns false if item was not found in the `MyList<T>`.

Usage example:
```cs
list.Add("Cat");
list.Add("Dog");
list.Add("Dog");
list.Add("Mouse");
list.Add("Cat");

list.Remove("Cat");

// list:    Dog
//          Dog
//          Mouse            
//          Cat
```
### 10. RemoveAt method
#### Description
Removes the element at the specified index of the `MyList<T>`.
```cs
public void RemoveAt (int index);
```
Usage example:
```cs
list.Add("Cat");
list.Add("Dog");
list.Add("Dog");
list.Add("Mouse");
list.Add("Cat");

list.RemoveAt(3); // remove Mouse

// list:    Cat
//          Dog
//          Dog
//          Cat
```
### 11. Clear method
#### Description
Removes all elements from the `MyList<T>`.
```cs
public void Clear ();
```
Usage example:
```cs
list.Add("Telerik"); // list.Count is 1
list.Add("Academy"); // list.Count is 2

list.Clear(); // list.Count is 0
```
### 12. Swap method
#### Description
Swaps the position of two elements.
```cs
public void Swap(int index0, int index1);
``` 
Usage example:
```cs
list.Add("Dog");
list.Add("Cat");
list.Add("Mouse");
// Dog, Cat, Mouse

list.Swap(0, 1); // Cat, Dog, Mouse
list.Swap(0, 2); // Mouse, Dog, Cat
```
> Hints

Now that you have gone through the requirements of the task, here are some hint points to jumpstart your development.

### 13. Resizing an Array

Implementing the `MyList<T>.Add(T)` and `MyList<T>.Remove(T)` methods *would require increasing or decreasing the size of the internal array many times*. 

One commonly used approach is to use the **`Array.Resize`** method. 

This method *creates a new array*, then copies the elements from the old array to the new one. Finally it changes the reference so that the address of the old array points to the address of the new array memory location. *`Array.Resize` is a misnomer.* It does not resize the array. It replaces the array with a new one of a different size.

> Making an array SMALLER:

```cs
using System;
class Program
{
    static void Main()
    {
        var array = new char[6];
        array[0] = 'b';
        array[1] = 'a';
        array[2] = 't';
        array[3] = 'm';
        array[4] = 'a';
        array[5] = 'n';
        // array contains ['b', 'a', 't', 'm', 'a', 'n']

        // Resize the array from 6 to 3 elements.
        Array.Resize(ref array, 3);

        // array now contains ['b', 'a', 't']
        Console.WriteLine(new string(array)); // bat
    }
}
```

>Similarly... making an array LARGER:

```cs
using System;
class Program
{
    static void Main()
    {
        var array = new char[3];
        array[0] = 'b';
        array[1] = 'a';
        array[2] = 't';
        // array contains ['b', 'a', 't']

        // Resize the array from 3 to 6 elements.
        Array.Resize(ref array, 6);

        array[3] = 'm';
        array[4] = 'a';
        array[5] = 'n';

        // array now contains ['b', 'a', 't', 'm', 'a', 'n']
        Console.WriteLine(new string(array)); // batman
    }
}

```
### 14. Indexing
#### Description
Do you remember how you could use **[ ]** when using the `System.Collections.Generic.List<T>`? In the skeleton we've provided **Indexing is already implemented**, however it's good to know exactly how it works. Let's first see an example of indexing:
```cs
var list = new System.Collections.Generic.List<int>() {3, 1, 4, 5, 2};
System.Console.WriteLine(list[3]); // would print 5
```
As you remember, **accessing elements by index is supported ONLY by arrays**. `System.Collections.Generic.List<T>` is not an array but fortunately **uses an array internally**.

So, to allows accessing elements by index, the `List<T>` has to expose its internal array's indexation mechanism. **It does it through the use of [Indexers](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/indexers/).** They resemble properties except that their acessors take parameters.
###### Example
```cs
   public T this[int i]
   {
      get { return arr[i]; }
      set { arr[i] = value; }
   }
```

> NOTES - ```IEnumerable<T>```
### 15. Using IEnumerable\<T> interface
- We've provided you with the IEnumerable<T> interface already but you are also allowed to, given you find it necessary, to override its methods.
```cs
public class MyList<T> : IEnumerable<T>
```