﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Academy.Collections.Generic
{
    public class MyList<T> : IEnumerable<T>
    {
        private const int defaultCapacity = 4;

        private T[] items; //array has capacity
        private int count; //how much elements we have used

        public MyList()
            : this(defaultCapacity)
        {
        }

        public MyList(int capacity)
        {
            this.items = new T[capacity];
            this.count = 0;
        }

        public int Count
        {
            get { return this.count; }
        }

        public int Capacity
        {
            get { return this.items.Length; }
        }

        //insert at position i
        //public void Insert(int index, T value)
        //{
        //    this.items[index] = value;
        //    this.length++;
        //}

        //public T Get(int index)
        //{
        //    return this.items[index];
        //}
        public T this[int i]
        {
            get { return this.items[i]; }
            set
            {
                if (i < 0 || i >= this.Count)
                {
                    throw new ArgumentOutOfRangeException();
                }
                this.items[i] = value;
            }
        }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="value">The object to be added to the collection.</param>
        /// <returns>void</returns>
        public void Add(T value)
        {
            if (this.count == this.Capacity)
            {
                this.Resize();
            }
            this.items[this.count] = value;
            this.count++;
        }

        /// <summary>
        /// Doubles the size of the collection or sets its capacity to its initial one if less
        /// </summary>
        /// <returns>void</returns>
        private void Resize()
        {
            int newCapacity = this.items.Length * 2;

            T[] newItems = new T[newCapacity];
            Array.Copy(this.items, 0, newItems, 0, this.items.Length);
            this.items = newItems;
        }

        /// <summary>
        /// Removes the first occurance of a given value in this list
        /// </summary>
        /// <param name="item">The item to be removed</param>
        /// <returns>True if it succeeds</returns>
        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the index of the first occurrence of a given value in this list
        /// </summary>
        /// <param name="item">The item to be found</param>
        /// <returns>The zero based index of the item</returns>
        public int IndexOf(T item)
        {
            return Array.IndexOf(this.items, item, 0, this.items.Length);
        }

        /// <summary>
        /// Returns the index of the last occurrence of a given value in this list
        /// </summary>
        /// <param name="item">The item to be found</param>
        /// <returns>The zero based index of the item</returns>
        public int LastIndexOf(T item)
        {
            return Array.LastIndexOf(this.items, item);
        }

        /// <summary>
        /// Removes the collection item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        /// <returns>void</returns>
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= this.count)
            {
                throw new ArgumentOutOfRangeException();
            }

            this.count--;

            Array.Copy(this.items, index + 1, this.items, index, this.count - index);

            this.items[this.count] = default(T); //optional
        }

        /// <summary>
        /// Removes all items from the collection
        /// </summary>
        /// <returns>void</returns>
        public void Clear()
        {
            if (count > 0)
            {
                Array.Clear(this.items, 0, count);
                count = 0;
            }
        }

        /// <summary>
        /// Assigns the given value of type T to each element of the specified array.
        /// </summary>
        /// <param name="value">The value to assign to each array element.</param>
        /// <returns>void</returns>
        public void Populate(T value)
        {
            if (this.items.Length > this.count)
            {
                Array.Fill(this.items, value);
                this.count = this.items.Length;
            }
        }

        /// <summary>
        /// Swaps the position of two elements
        /// </summary>
        /// <param name="index0">The zero-based index of the first item to swap.</param>
        /// <param name="index1">The zero-based index of the second item to swap.</param>
        /// <returns>void</returns>
        public void Swap(int index0, int index1)
        {
            if (this.count == 0 || index0 >= this.count || index1 >= this.count || index0 < 0 || index1 < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            var temp = this.items[index0];

            this.items[index0] = this.items[index1];
            this.items[index1] = temp;
        }

        /// <summary>
        /// Determines whether an element is in the collection.
        /// </summary>
        /// <param name="value">The object to locate in the current list. The element to locate can be null for reference types.</param>
        /// <returns>Returns true if the method succeeds. Else it returns false.</returns>
        public bool Contains(T value)
        {
            if (this.count == 0)
            {
                return false;
            }

            foreach (var item in this.items)
            {
                if (item.Equals(value))
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.Take(this.count).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.Take(this.count).GetEnumerator();
        }
    }

}
