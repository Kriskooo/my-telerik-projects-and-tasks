﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Academy.Collections.Generic
{
    public class MyList<T> : IEnumerable<T>
    {
        private T[] items;
        private int length;

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public int Capacity
        {
            get { throw new NotImplementedException(); }
        }

        public T this[int i]
        {
            get { return this.items[i]; }
            set
            {
                this.items[i] = value;
                //this.length++;
            }
        }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="value">The object to be added to the collection.</param>
        /// <returns>void</returns>
        public void Add(T value)
        {
            throw new NotImplementedException();
        }

        private void Resize()
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public int LastIndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public void Populate(T value)
        {
            throw new NotImplementedException();
        }

        public void Swap(int index0, int index1)
        {
            throw new NotImplementedException();
        }

        public bool Contains(T value)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }
    }
}
