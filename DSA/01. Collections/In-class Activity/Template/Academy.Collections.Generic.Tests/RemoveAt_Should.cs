﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class RemoveAt_Should
    {
        [TestMethod]
        public void CorrectlyRemoveItem()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            //Act
            list.RemoveAt(0);

            //Assert
            Assert.AreEqual(2, list.Count);
        }
    }
}
