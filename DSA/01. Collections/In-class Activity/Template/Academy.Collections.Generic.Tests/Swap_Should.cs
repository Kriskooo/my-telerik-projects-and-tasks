﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Swap_Should
    {
        [TestMethod]
        public void CorrectlySwapItem()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            //Act
            list.Swap(0, 2);

            //Assert
            Assert.AreEqual(3, list[0]);
        }
    }
}
