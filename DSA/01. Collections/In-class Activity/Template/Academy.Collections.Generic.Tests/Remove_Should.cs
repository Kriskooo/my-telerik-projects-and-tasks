﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Remove_Should
    {
        [TestMethod]
        public void CorrectlyRemoveItem()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(5);
            list.Add(2);

            //Act
            list.Remove(5);

            //Assert
            Assert.AreEqual(1, list.Count);
        }
    }
}
