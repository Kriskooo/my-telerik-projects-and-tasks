﻿using System;
using System.Collections.Generic;

namespace SortedSet
{
    class Program
    {
        static void Main(string[] args)
        {
            // SortedSet<T> - represents a collection of objects that is maintained in sorted order.
            // Duplicate elements are not allowed
            // ore info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.sortedset-1?view=netcore-3.1
            SortedSet<string> sortedSet = new SortedSet<string>();

            sortedSet.Add("James");
            sortedSet.Add("Adam");
            sortedSet.Add("Zoe");
            sortedSet.Add("James"); // <= this will not work as James is already present in the collection


            foreach (string name in sortedSet)
            {
                Console.WriteLine(name);
            }

            // James
            // Adam
            // Zoe

            // the collection contains only a single copy of James as no duplicates are allowed.
        }
    }
}
