﻿using System;
using System.Collections.Generic;

namespace SortedDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            // SortedDictionary<T> represents a collection of key/value pairs that are sorted on the key
            // Also provides all the methods from Dictionary<T>
            // More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.sorteddictionary-2?view=netcore-3.1
            SortedDictionary<int, string> students = new SortedDictionary<int, string>();

            students.Add(3, "John");
            students.Add(1, "Katherine");
            students.Add(2, "James");

            foreach (var item in students)
            {
                Console.WriteLine($"{item.Key} {item.Value}");
            }

            // 1 Katherine
            // 2 James
            // 3 John
        }
    }
}
