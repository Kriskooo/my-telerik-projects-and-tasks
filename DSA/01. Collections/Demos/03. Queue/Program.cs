﻿using System;
using System.Collections.Generic;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            //Queue<T> represents a variable size first-in, first-out (FIFO) collection of instances of the same specified type.
            //More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.queue-1?view=netcore-3.1

            Queue<int> numbers = new Queue<int>();

            numbers.Enqueue(1);
            numbers.Enqueue(2);
            numbers.Enqueue(3);

            //Peek returns the element at the beginning of the queue but does NOT remove it.
            numbers.Peek(); // 1

            int firstElement = numbers.Dequeue(); //firstElement is 1 as it was the last one added. The element has been removed from the queue and the count is now 2.
        }
    }
}
