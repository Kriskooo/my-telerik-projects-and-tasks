﻿using System;
using System.Collections.Generic;

namespace HashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            // HashSet<T> - represents a set of unique values - no duplicates allowed.
            // More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.hashset-1?view=netcore-3.1
            HashSet<string> hashSet = new HashSet<string>();

            hashSet.Add("John");
            hashSet.Add("Jill");
            hashSet.Add("Jane");
            hashSet.Add("Jill"); // <= this will not work, Jill is already in the collection.

            foreach (string name in hashSet)
            {
                Console.WriteLine(name);
            }

            // John
            // Jill
            // Jane
        }
    }
}
