﻿using System;
using System.Collections.Generic;

namespace List
{
    class Program
    {
        static void Main(string[] args)
        {
            // List<T> represents a strongly typed list of objects that can be accessed by index. Provides methods to search, sort, and manipulate lists.
            // More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=netcore-3.1

            List<int> numbers = new List<int>();

            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);
            numbers.Remove(2);

            numbers.Contains(3);
        }
    }
}
