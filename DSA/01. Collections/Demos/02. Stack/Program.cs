﻿using System;
using System.Collections.Generic;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            // Stack<T> represents a variable size last-in-first-out (LIFO) collection of instances of the same specified type.
            // More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.stack-1?view=netcore-3.1

            Stack<int> numbers = new Stack<int>();

            numbers.Push(1);
            numbers.Push(2);
            numbers.Push(3);

            // Peek returns the element at the top of the stack but does NOT remove it.
            numbers.Peek(); // 3

            int topElement = numbers.Pop(); // topElement is 3 as it was the last one added. The element has been removed from the stack and the count is now 2.

        }
    }
}
