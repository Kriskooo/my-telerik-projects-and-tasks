﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dictionary<T> represents a collection of keys and values. Dictionary provides a quick and easy access to a value element by a given key
            //More info - https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2?view=netcore-3.1

            Dictionary<int, string> students = new Dictionary<int, string>();

            //When you add elements to a dictionary you need to add them as key-value pairs.
            students.Add(1, "John");
            students.Add(2, "Katherine");
            students.Add(3, "James");

            //ContainsKey checks if a given key exists.
            students.ContainsKey(1); // returns true as the collection contains a pair with key 1
            students.ContainsKey(4); // returns false as the collection does not contain a pair with key 4
            
            //ContainsValue checks if a given value exists.
            students.ContainsValue("John"); // returns true as the string "John" is present in the collection

            students.Remove(3); // When removing an element only key is needed
        }
    }
}
