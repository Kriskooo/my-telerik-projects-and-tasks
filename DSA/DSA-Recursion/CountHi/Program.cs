﻿using System;

namespace CountHi
{
    class Program
    {
        static int count = 0;
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            Texting(text);
            Console.WriteLine(count);
        }

        static string Texting(string text)
        {
            if (text.Length < 2)
            {
                return " ";
            }

            if (text[0] == 'h' && text[1] == 'i')
            {
                count++;
            }

            return Texting(text.Substring(1));
        }
    }
}
