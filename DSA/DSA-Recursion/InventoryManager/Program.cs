﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryManager
{
    class Program
    {
        static void Main(string[] args)
        {
            var inventory = new List<Product>();
            var inventoryForType = new Dictionary<string, List<Tuple<double, string>>>();
            var names = new HashSet<string>();
            var types = new HashSet<string>();
            var sb = new StringBuilder();

            string input;
            while ((input = Console.ReadLine()) != "end")
            {
                var inputTokens = input.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                var command = inputTokens[0];
                if (command == "add")
                {
                    string name = inputTokens[1];

                    if (names.Contains(name))
                    {
                        sb.AppendLine($"Error: Item {name} already exists");
                        continue;
                    }

                    double price = double.Parse(inputTokens[2]);
                    string type = inputTokens[3];

                    var product = new Product(name, price, type);
                    inventory.Add(product);
                    names.Add(name);
                    types.Add(type);

                    if (!inventoryForType.ContainsKey(type))
                    {
                        inventoryForType[type] = new List<Tuple<double, string>>();
                    }

                    var currentTuple = new Tuple<double, string>(price, name);
                    inventoryForType[type].Add(currentTuple);

                    sb.AppendLine($"Ok: Item {name} added successfully");
                }
                else // filter:
                {
                    // by type:
                    if (inputTokens[2] == "type")
                    {
                        var searchedType = inputTokens[3];
                        if (!types.Contains(searchedType))
                        {
                            sb.AppendLine($"Error: Type {searchedType} does not exist");
                            continue;
                        }
                        else
                        {
                            var currentTypeList = inventoryForType[searchedType]
                                .OrderBy(x => x.Item1)
                                .ThenBy(y => y.Item2)
                                .Take(10)
                                .Select(z => $"{z.Item2}({z.Item1:f2})")
                                .ToArray();
                            sb.AppendLine($"Ok: {string.Join(", ", currentTypeList)}");
                        }
                    }
                    else if (inputTokens.Count() == 7) // from, to price range 
                    {
                        var minPrice = double.Parse(inputTokens[4]);
                        var maxPrice = double.Parse(inputTokens[6]);

                        var priceRangeSortedInvetory = inventory
                            .Where(x => x.Price >= minPrice && x.Price <= maxPrice)
                            .OrderBy(x => x.Price)
                            .Take(10)
                            .OrderBy(x => x.Price)
                            .ThenBy(x => x.Name)
                            .Select(x => $"{x.Name}({x.Price:f2})")
                            .ToArray();
                        sb.AppendLine($"Ok: {string.Join(", ", priceRangeSortedInvetory)}");
                    }
                    else
                    {
                        var price = double.Parse(inputTokens[4]);
                        if (inputTokens.Contains("to"))
                        {
                            var maxPriceSortedInventory = inventory
                                .Where(x => x.Price <= price)
                                .OrderBy(x => x.Price)
                                .Take(10)
                                .OrderBy(x => x.Price)
                                .ThenBy(x => x.Name)
                                .Select(x => $"{x.Name}({x.Price:f2})")
                                .ToArray();
                            sb.AppendLine($"Ok: {string.Join(", ", maxPriceSortedInventory)}");
                        }
                        else // from, so above
                        {
                            var maxPriceSortedInventory = inventory
                                .Where(x => x.Price >= price)
                                .OrderBy(x => x.Price)
                                .Take(10)
                                .OrderBy(x => x.Price)
                                .ThenBy(x => x.Name)
                                .Select(x => $"{x.Name}({x.Price:f2})")
                                .ToArray();
                            sb.AppendLine($"Ok: {string.Join(", ", maxPriceSortedInventory)}");
                        }
                    }
                }
            }
            Console.WriteLine(sb.ToString());
        }
        public struct Product
        {
            public Product(string name, double price, string type)
            {
                this.Name = name;
                this.Price = price;
                this.Type = type;
            }

            public string Name { get; set; }
            public double Price { get; set; }
            public string Type { get; set; }
        }
    }
}
