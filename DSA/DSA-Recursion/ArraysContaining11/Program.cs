﻿using System;
using System.Linq;

namespace ArraysContaining11
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = Console.ReadLine().Split(',').Select(int.Parse).ToArray();
            int index = int.Parse(Console.ReadLine());

            Console.WriteLine(Array(numbers, index));
        }

        static int Array(int[] numbers, int index)
        {
            if (index >= numbers.Length)
            {
                return 0;
            }

            if (numbers[index] == 11)
            {
                return 1 + Array(numbers, index + 1);
            }
            else
            {
                return Array(numbers, index + 1);
            }
        }
    }
}
