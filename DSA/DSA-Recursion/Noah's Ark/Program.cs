﻿using System;
using System.Collections.Generic;

namespace Noah_s_Ark
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            var animals = new SortedDictionary<string, int>();

            for (int i = 0; i < number; i++)
            {
                string output = Console.ReadLine();
                if (animals.ContainsKey(output))
                {
                    animals[output]++;
                }
                else
                {
                    animals.Add(output, 1);
                }
            }
            Console.WriteLine();
            foreach (var item in animals)
            {
                if (item.Value % 2 == 0)
                {
                    Console.WriteLine($"{item.Key} {item.Value} Yes");
                }
                else
                {
                    Console.WriteLine($"{item.Key} {item.Value} No");
                }
            }
        }
    }
}
