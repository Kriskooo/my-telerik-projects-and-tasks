﻿using System;

namespace CountOccurrences2
{
    class Program
    {
        public static int sum = 0;
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());

            Occur(number);
            Console.WriteLine(sum);
        }

        static int Occur(int number)
        {
            if (number == 0)
            {
                return 0;
            }

            if (number % 10 == 8)
            {
                if ((number / 10) % 10 == 8)
                {
                    sum++;
                }

                sum++;
            }

            return Occur(number / 10);
        }
    }
}
