﻿using System;
using System.Linq;

namespace ArrayValuesTimes10
{
    class Program
    {
        static bool count = false;

        static void Main(string[] args)
        {
            int[] numbers = Console.ReadLine().Split(',').Select(int.Parse).ToArray();

            Count(numbers, 0);
            if (count)
            {
                Console.WriteLine("true");
            }
            else
            {
                Console.WriteLine("false");
            }
        }

        static int[] Count(int[] numbers, int index)
        {
            if (index == numbers.Length - 1)
            {
                return numbers;
            }

            if (numbers[index] * 10 == numbers[index + 1])
            {
                count = true;
            }

            return Count(numbers, index + 1);
        }
    }
}
