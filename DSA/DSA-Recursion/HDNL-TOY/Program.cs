﻿using System;
using System.Collections;
using System.Linq;

namespace HDNL_Toy
{
    class Program
    {
        static void Main()
        {
            var lines = int.Parse(Console.ReadLine());
            if (lines < 1 || lines > 100000)
            {
                throw new ArgumentOutOfRangeException();
            }
            var stack = new Stack();

            for (int i = 0; i < lines; i++)
            {
                var input = Console.ReadLine();


                while (stack.Count > 0)
                {
                    var prev = int.Parse(stack.Peek().ToString().Substring(1));
                    var cur = int.Parse(input.Substring(1));

                    if (cur > prev)
                    {
                        Console.WriteLine($"<{input}>");
                        stack.Push(input);
                        break;
                    }
                    else if (cur <= prev)
                    {
                        Console.WriteLine($"</{stack.Pop()}>");
                    }

                }
                if (stack.Count == 0)
                {
                    Console.WriteLine($"<{input}>");
                    stack.Push(input);
                }



            }
            while (stack.Count > 0)
            {
                Console.WriteLine($"</{stack.Pop()}>");
            }
        }
    }
}
