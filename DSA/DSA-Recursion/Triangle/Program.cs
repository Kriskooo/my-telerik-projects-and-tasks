﻿using System;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            int blocks = int.Parse(Console.ReadLine());
            int sum = Factorial(blocks);
            Console.WriteLine(sum);
        }
        static int Factorial(int num)
        {
            if (num == 0)
            {
                return 0;
            }

            if (num == 1)
            {
                return 1;
            }

            return num + Factorial(num - 1);
        }
    }
}

