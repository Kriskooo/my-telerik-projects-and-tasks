﻿using System;

namespace BunnyEars
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfBunnies = int.Parse(Console.ReadLine());
            Console.WriteLine(Ears(numberOfBunnies));
        }

        static int Ears(int num)
        {
            if (num == 0)
            {
                return 0;
            }
            if (num == 1)
            {
                return 2;
            }

            return 2 + Ears(num - 1);
        }
    }
}
