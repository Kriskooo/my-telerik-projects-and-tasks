﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Fibonacci(long.Parse(Console.ReadLine())));
        }
        static long Fibonacci(long n)
        {
            if (n <= 1) return n;

            // Create the look-up table.
            long[] fibo = new long[n + 1];
            return Fibonacci(fibo, n);
        }
        static long Fibonacci(long[] fibo, long n)
        {
            if (n <= 1) return n;

            // If we have already calculated this value, return it.
            if (fibo[n] > 0) return fibo[n];

            // Calculate the result.
            long result =
                Fibonacci(fibo, n - 1) +
                Fibonacci(fibo, n - 2);

            // Save the value in the table.
            fibo[n] = result;

            // Return the result.
            return result;

        }
    }
}

