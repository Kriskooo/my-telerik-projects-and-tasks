﻿using System;

namespace ChangePi
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            Console.WriteLine(Texting(text));
        }

        static string Texting(string text)
        {
            if (text.Length <= 1)
            {
                return text;
            }

            if (text[0] == 'p' && text.Length >= 2 && text[1] == 'i')
            {
                return "3.14" + Texting(text.Substring(2, text.Length - 2));
            }

            return text[0] + Texting(text.Substring(1, text.Length - 1));
        }
    }
}
