﻿using System;

namespace SumDigits
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int sum = SumDigits(number);
            Console.WriteLine(sum);
        }

        static int SumDigits(int number)
        {
            if (number == 0)
            {
                return 0;
            }

            return (number % 10 + SumDigits(number / 10));
        }
    }
}

