﻿using System;
using System.Linq;

namespace ArrayWith6
{
    class Program
    {
        static bool contains = false;
        static void Main(string[] args)
        {
            var numbers = Console.ReadLine().Split(',').Select(int.Parse).ToArray();
            var index = 0;
            ArrayWith(numbers, index);

            if (contains)
            {
                Console.WriteLine("true");
            }
            else
            {
                Console.WriteLine("false");
            }
        }

        static int[] ArrayWith(int[] numbers, int index)
        {
            if (index < numbers.Length)
            {
                if (numbers[index] == 6)
                {
                    contains = true;
                    return numbers;
                }

                return ArrayWith(numbers, index + 1);
            }
            return numbers;
        }
    }
}
