﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DSAHomeworkExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = int.Parse(Console.ReadLine());
            int[] numbers = Console.ReadLine().Split(' ')
                .Select(int.Parse).ToArray();

            int[] jumps = new int[length];
            int maxNumber = numbers.Max();
            //int maxJumps = 0;

            for (int i = 0; i < length - 1 && numbers[i] != 0; i++)
            {
                int currentValue = numbers[i];
                var tempList = new List<int>();

                if (currentValue == maxNumber)
                {
                    jumps[i] = 0;
                    continue;
                }

                if (jumps[i] != 0)
                {
                    continue;
                }

                for (int j = i + 1; j < length; j++)
                {
                    int nextValue = numbers[j];

                    if (nextValue == maxNumber)
                    {
                        jumps[i]++;
                        break;
                    }

                    if (currentValue < nextValue)
                    {
                        if (jumps[j] != 0)
                        {
                            jumps[i] += jumps[j] + 1;
                            tempList.Clear();
                            break;
                        }
                        else
                        {
                            tempList.Add(j);
                            currentValue = nextValue;
                            jumps[i]++;
                        }
                    }
                }

                var currentJumps = jumps[i];
                for (int k = 0; k < tempList.Count; k++)
                {
                    jumps[tempList[k]] = --currentJumps;
                }
            }

            Console.WriteLine(jumps.Max());
            Console.WriteLine(string.Join(" ", jumps));
        }
    }
}
