﻿using System;
using System.Linq;

namespace ScroogeMcDuck
{
    class Program
    {
        public static int coins = 0;
        static void Main(string[] args)
        {
            var matrixInputs = Console.ReadLine().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            var rows = matrixInputs[0];
            var cols = matrixInputs[1];

            var matrix = new int[rows, cols];

            var startRow = 0;
            var startCol = 0;

            // populate matrix
            for (int row = 0; row < rows; row++)
            {
                var currentRow = Console.ReadLine().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                for (int col = 0; col < cols; col++)
                {
                    if (currentRow[col] == 0)
                    {
                        startRow = row;
                        startCol = col;
                    }
                    matrix[row, col] = currentRow[col];
                }
            }
            GatherCoins(startRow, startCol, matrix);
            Console.WriteLine();
        }

        public static int GatherCoins(int row, int col, int[,] matrix)
        {
            int[] possibleDirections = GetPossibleDirections(row, col, matrix);
            int currentRow = possibleDirections[0];
            int currentCol = possibleDirections[1];
            // check to leave all zero's around it
            if (currentRow == -1)//Row
            {
                Console.WriteLine(coins);
                Environment.Exit(0);
                // try and see if it works, if not return
            }
            if (currentRow != -1)
            {
                coins++;
                matrix[currentRow, currentCol] -= 1;
            }

            return GatherCoins(currentRow, currentCol, matrix);
        }

        private static int[] GetPossibleDirections(int row, int col, int[,] matrix)
        {
            //left, right, up, down
            var left = col - 1;
            var right = col + 1;
            var up = row - 1;
            var down = row + 1;

            int[] postions = new int[2];
            int tempValue = int.MinValue;
            int tempCol = -1;
            int tempRow = -1;
            string tempPositon = String.Empty;

            bool canLeft = CanMove(row, left, matrix);
            bool canRight = CanMove(row, right, matrix);
            bool canUp = CanMove(up, col, matrix);
            bool canDown = CanMove(down, col, matrix);
            if (canLeft && matrix[row, left] != 0) // Left validation
            {
                tempValue = matrix[row, left];
                tempRow = row;
                tempCol = left;
                tempPositon = "left";
            }
            if (canRight && matrix[row, right] != 0 && matrix[row, right] > tempValue)// Left validation
            {
                tempValue = matrix[row, right];
                tempRow = row;
                tempCol = right;
                tempPositon = "right";
            }
            if (canUp && matrix[up, col] != 0 && matrix[up, col] > tempValue)// Up validation
            {
                tempValue = matrix[up, col];
                tempRow = up;
                tempCol = col;
                tempPositon = "up";
            }
            if (canDown && matrix[down, col] != 0 && matrix[down, col] > tempValue)// Down validation
            {
                tempValue = matrix[down, col];
                tempRow = down;
                tempCol = col;
                tempPositon = "down";
            }

            int[] directions = new int[2];
            directions[0] = tempRow;
            directions[1] = tempCol;

            return directions;
        }

        private static bool CanMove(int row, int col, int[,] matrix) // use new values
        {
            if (row < 0 || row >= matrix.GetLength(0) || col < 0 || col >= matrix.GetLength(1))
            {
                return false;
            }

            return true;
        }
    }
}
