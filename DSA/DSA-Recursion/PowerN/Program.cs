﻿using System;

namespace PowerN
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int power = int.Parse(Console.ReadLine());

            if (number > 0 && power > 0)
            {
                Console.WriteLine(Power(number, power));
            }
        }

        static int Power(int number, int power)
        {
            if (power == 0)
            {
                return 1;
            }

            return number * Power(number, power - 1);
        }
    }
}
