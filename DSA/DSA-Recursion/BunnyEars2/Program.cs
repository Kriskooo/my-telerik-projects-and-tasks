﻿using System;

namespace BunnyEars2
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfBunnies = int.Parse(Console.ReadLine());
            Console.WriteLine(Ears(numberOfBunnies));
        }

        static int Ears(int num)
        {
            if (num == 0)
            {
                return 0;
            }
            if (num % 2 == 0)
            {
                return 3 + Ears(num - 1);

            }
            else
            {
                return 2 + Ears(num - 1);
            }

        }
    }
}
