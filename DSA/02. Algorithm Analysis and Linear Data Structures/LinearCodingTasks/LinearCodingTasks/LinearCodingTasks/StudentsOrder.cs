﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearCodingTasks
{
    public class StudentsOrder
    {
        static void Solve(string[] args)
        {
            string inputLine = Console.ReadLine();
            string[] inputElements = inputLine.Split();
            int n = int.Parse(inputElements[0]);
            int k = int.Parse(inputElements[1]);

            inputLine = Console.ReadLine();
            LinkedList<string> students = new LinkedList<string>(inputLine.Split());
            for (int i = 0; i < k; i++)
            {
                inputLine = Console.ReadLine();
                string[] studentsSwitch = inputLine.Split();
                LinkedListNode<string> studentToRemove = students.Find(studentsSwitch[0]);
                students.Remove(studentToRemove);
                LinkedListNode<string> insertStudent = students.Find(studentsSwitch[1]);
                students.AddBefore(insertStudent, studentsSwitch[0]);
            }

            Console.WriteLine(string.Join(" ", students));
        }
    }
}
