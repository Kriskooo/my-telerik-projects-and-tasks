﻿using System;
using System.Collections.Generic;

namespace LinearCodingTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string.Join(" ", AsteroidCollision(new int[] { 10, 2, -5 })));
        }

        public static int[] AsteroidCollision(int[] asteroids)
        {
            Stack<int> resultAsteroids = new Stack<int>();
            foreach (int asteroid in asteroids)
            {
                if (resultAsteroids.Count == 0)
                {
                    resultAsteroids.Push(asteroid);
                    continue;
                }

                int leftAsteroid = resultAsteroids.Peek();
                if (asteroid * leftAsteroid > 0)
                {
                    resultAsteroids.Push(asteroid);
                    continue;
                }

                if (leftAsteroid < 0 && asteroid > 0)
                {
                    resultAsteroids.Push(asteroid);
                    continue;
                }

                while (leftAsteroid > 0 && asteroid < 0)
                {
                    if (Math.Abs(leftAsteroid) > Math.Abs(asteroid))
                    {
                        break;
                    }

                    if (Math.Abs(leftAsteroid) == Math.Abs(asteroid))
                    {
                        resultAsteroids.Pop();
                        break;
                    }

                    if (Math.Abs(leftAsteroid) < Math.Abs(asteroid))
                    {
                        resultAsteroids.Pop();
                        if (resultAsteroids.Count == 0 || resultAsteroids.Peek() < 0)
                        {
                            resultAsteroids.Push(asteroid);
                            break;
                        }
                        else
                        {
                            leftAsteroid = resultAsteroids.Peek();

                        }
                    }
                }
            }

            int[] result = new int[resultAsteroids.Count];
            for (int i = result.Length - 1; i >= 0; i--)
            {
                result[i] = resultAsteroids.Pop();
            }
            return result;
        }
    }
}
