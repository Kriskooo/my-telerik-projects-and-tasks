﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinearCodingTasks
{
    public class Jumps
    {
        static void Solve(string[] args)
        {
            string input = Console.ReadLine();
            int n = int.Parse(input);
            input = Console.ReadLine();
            string[] inputNumbers = input.Split();
            int[] numbers = new int[n];
            for (int i = 0; i < n; i++)
            {
                numbers[i] = int.Parse(inputNumbers[i]);
            }

            int maxJumpsCount = 0;
            int[] jumpsCount = new int[n];
            for (int i = n - 2; i >= 0; i--)
            {
                int maxPrevJump = numbers[i];
                for (int j = i + 1; j < n; j++)
                {
                    if (numbers[j] > numbers[i] && numbers[j] > maxPrevJump)
                    {
                        if (jumpsCount[i] < jumpsCount[j] + 1)
                        {
                            jumpsCount[i] = jumpsCount[j] + 1;
                            if (jumpsCount[i] > maxJumpsCount)
                            {
                                maxJumpsCount = jumpsCount[i];
                            }
                        }
                        break;
                    }
                }
            }

            Console.WriteLine(maxJumpsCount);
            Console.WriteLine(string.Join(" ", jumpsCount));
        }
    }
}
