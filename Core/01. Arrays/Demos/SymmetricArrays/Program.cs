﻿using System;

namespace SymmetricArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            string yes = "Yes";
            string no = "No";

            for (int i = 0; i < n; i++)
            {
                string[] array = Console.ReadLine().Split(' ');

                //optional
                if (array.Length == 1)
                {
                    Console.WriteLine(yes);
                    continue;
                }

                bool symmetric = true;
                for (int j = 0; j < array.Length / 2; j++)
                {
                    string valueLeft = array[j];
                    string valueRight = array[array.Length - 1 - j];

                    if (valueLeft != valueRight)
                    {
                        symmetric = false;
                        break;
                    }
                }

                if (symmetric)
                {
                    Console.WriteLine(yes);
                }
                else
                {
                    Console.WriteLine(no);
                }
            }
        }
    }
}
