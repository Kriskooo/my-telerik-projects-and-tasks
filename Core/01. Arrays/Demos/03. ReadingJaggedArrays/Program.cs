using System;

namespace _03._ReadingJaggedArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = int.Parse(Console.ReadLine());

            string[][] jagged = new string[rows][];

            for (int i = 0; i < rows; i++)
            {
                string line = Console.ReadLine(); // 1,2,3,4,5,6

                // Split the line by "," or any other symbol 
                string[] arrStrings = line.Split(',');

                // Assign the split array to the jagged at position "i"
                jagged[i] = arrStrings;
            }

            // test the result from read
            foreach (string[] row in jagged)
            {
                foreach (string element in row)
                {
                    Console.Write("{0, 4}", element);
                }
                Console.WriteLine();
            }
        }
    }
}
