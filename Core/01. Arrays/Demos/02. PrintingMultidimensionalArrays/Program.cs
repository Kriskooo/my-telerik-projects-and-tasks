﻿using System;

namespace _02._PrintingMultidimensionalArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix =
            {
                  { 1, 2 },
                  { 3, 4 }
            };

            Console.WriteLine(matrix.Rank);
            Console.WriteLine(matrix.Length);

            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Console.Write("{0, 4}", matrix[row, col]);
                }

                Console.WriteLine();
            }

        }
    }
}
