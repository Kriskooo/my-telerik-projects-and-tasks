﻿using System;
using System.Linq;

namespace ReverseArray
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            string[] arr = input.Split();

            int[] intArray = new int[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                intArray[i] = int.Parse(arr[i]);
            }

            for (int i = 0; i < intArray.Length / 2; i++)
            {
                int temp = intArray[i];
                intArray[i] = intArray[intArray.Length - i - 1];
                intArray[intArray.Length - i - 1] = temp;

            }

            Console.WriteLine(String.Join(", ", intArray));
         }
    }
}
