﻿using System;

namespace _07._SessionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 7;

            int[] array = new int[5] { 1, 2, 3, 4, 5 };
            string arrayAsString = string.Join(";", array);
            Console.WriteLine(arrayAsString);

            int[,,] cube = new int[3, 3, 3]
            {
                {
                    {1, 2, 3 },
                    {4, 5, 6 },
                    {7, 8, 9 },
                },
                {
                    {10, 11, 12 },
                    {13, 14, 15 },
                    {16, 17, 18 },
                },
                {
                    {19, 20, 21 },
                    {22, 23, 24 },
                    {25, 26, 27 },
                }
            };

            Console.WriteLine(cube.Rank);
        }
    }
}
