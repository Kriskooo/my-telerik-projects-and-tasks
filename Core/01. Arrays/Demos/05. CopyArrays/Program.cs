﻿using System;

namespace CopyArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = a;
            a = 6;

            Console.WriteLine(a);
            Console.WriteLine(b);

            Console.WriteLine(a == b);

            string str1 = "abc";
            string str2 = str1;
            str1 = "cba";

            Console.WriteLine(str1);
            Console.WriteLine(str2);

            Console.WriteLine(str1 == str2);

            int[] arr = { 1, 2, 3 };
            int[] arrCopy = arr; // (int[])arr.Clone();

            arr[1] = 5;

            // The result is 5 because the two arrays 
            // points to the same address in memory
            Console.WriteLine(arr[1]);
            Console.WriteLine(arrCopy[1]);

            // True -> The same address in memory
            Console.WriteLine(arr == arrCopy);
            Console.WriteLine(arr.Equals(arrCopy));
        }
    }
}
