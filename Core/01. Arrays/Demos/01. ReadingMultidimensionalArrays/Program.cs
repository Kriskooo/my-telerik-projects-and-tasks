﻿using System;

namespace _01._ReadingMultidimensionalArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            string line = Console.ReadLine();
            int rows = int.Parse(line);
            
            line = Console.ReadLine();
            int columns = int.Parse(line);

            int[,] matrix = new int[rows, columns];

            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    // interpolated string
                    Console.Write($"matrix[{row},{column}] = ");

                    line = Console.ReadLine();
                    matrix[row, column] = int.Parse(line);
                }
            }

        }
    }
}
