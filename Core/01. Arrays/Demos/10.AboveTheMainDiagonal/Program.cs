﻿using System;

namespace _10.AboveTheMainDiagonal
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            ulong[,] matrix = new ulong[n, n];

            ulong sum = 0;
            ulong rowInitialValue = 1;
            for (int row = 0; row < n; row++)
            {
                matrix[row, 0] = rowInitialValue;
                for (int col = 1; col < n; col++)
                {
                    matrix[row, col] = matrix[row, col - 1] * 2;
                }
                rowInitialValue = rowInitialValue * 2;
            }

            for (int i = n - 2; i >= 0; i--)
            {
                for (int j = n - 1; j > i; j--)
                {
                    sum += matrix[i, j];
                }
            }
            Console.WriteLine(sum);
        }
    }
}
