<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Getting Started with Git Bash

## Create GitLab Account

Create an account in [https://gitlab.com](https://gitlab.com)

## Install Visual Studio Code
[Download and install Visual Studio Code](https://code.visualstudio.com/download)
## Install Git

1. [Download git](​https://git-scm.com/downloads).
1. Run the git installer.
1. Follow the instructions below.

!['git-install'](./imgs/git-install.gif)

## Configure Git
1. Open the Windows Start Menu and type **Git Bash**
1. Type the following commands, but change the `user` and `email` with yours.

```bash
git config --global user.name "FirstName LastName"
```

```bash
git config --global user.email "Email Address"
```

!['git-configure'](./imgs/git-configure.gif)

## Clone an existing repository
In this section you will create a copy of the Alpha .NET Repository.
1. Open [https://gitlab.com/TelerikAcademy/alpha-31-cs](https://gitlab.com/TelerikAcademy/alpha-31-cs) and log in with your GitLab account.
1. Open **Git Bash** and type:

```
git clone https://gitlab.com/TelerikAcademy/alpha-31-cs "C:/Users/<YOUR USERNAME>/Desktop/alpha-31-cs"
```

Windows will prompt you for your Git Lab credentials (only once).

If your have entered your credentials correctly then the command will proceed to download every necessary file from _https://gitlab.com/TelerikAcademy/alpha-31-cs_ and store it in a local folder called _alpha-31-cs_ on your Desktop

!['gitlab'](./imgs/git-credentials.png)

## Create a new repository

> When creating a new repository you must first register it with GitLab using the [New Project page](https://gitlab.com/projects/new)

!['gitlab'](./imgs/new-repo.png)

> There's no limit to the number of new repositories you can create. That's why it is important to give them meaningful names.

> Setting the **Visibility Level** to **Private** is recomended for beginners who don't feel comfortable sharing their code with others.

> It is against the Telerik Academy Code of Conduct to upload anything related to your exam tasks.

## Make changes to repository

1. Start by cloning your newly created repository. 

> Use what you have learned when you cloned the Alpha .NET Repository earlier.

2. Open the README file located in the folder where you have cloned the repostory. Right click on the README file and select the text editor you feel most comfortable with.

> It is recommended to adopt Visual Studio Code as quickly as possible as it combines the simplicity of a source code editor with the power of a development environment. [Why use Visual Studio Code?](https://code.visualstudio.com/docs/editor/whyvscode)

!['gitlab'](./imgs/open-readme.png)

3. Modify the README file by typing `Updated the README`

!['gitlab'](./imgs/modify-readme.png)

4. Click with the right mouse button somewhere within the folder where the README is located and select **Git Bash Here**.

!['gitlab'](./imgs/git-bash-here.png)

Type **`git status`** - this will show you all the files that have been added, modified or deleted.

!['gitlab'](./imgs/git-status.png)

## Merge conflicts

When working in a team, you will often find yourself in situations where you and your teammate have been working on the _same piece of code_ independently from one another. When both of you commit your work Git will automatically try to resolve the differences between your commit and the commit of your teammate. If automatic resolution is not possible then a _merge conflict_ arises.

> A merge conflict is an event that occurs when Git is unable to automatically resolve differences in code between two commits.

To see this in action, first open your repository in the browser. While still in the browser, open the README file and click the **Edit** button. Make some changes and commit them.

!['gitlab'](./imgs/git-merge-conflict.gif)

## Now try to sync the changes with your local repository. How you can do that?

**HINTS**

You could use the commands below and figure out how to use them

```bash
git pull
git add .
git push
git commit -m "Descriptive commit message"
```
