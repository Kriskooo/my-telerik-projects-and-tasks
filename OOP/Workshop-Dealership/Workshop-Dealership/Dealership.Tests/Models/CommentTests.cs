﻿using System;
using Dealership.Models;
using Dealership.Models.Common;
using Dealership.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dealership.Tests.Models
{
    [TestClass]
    public class Comment_Tests
    {

        [TestMethod]
        public void CommentImpl_ShouldImplementCommentInterface()
        {
            Comment comment = new Comment("content", "author");
            Assert.IsInstanceOfType(comment, typeof(IComment));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenContentIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Comment(null, "author"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenContentIsBelow3()
        {
            Assert.ThrowsException<ArgumentException>(() => new Comment(new String(new char[Constants.CONTENT_LEN_MIN - 1]), "author"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenContentIsAbove200()
        {
            Assert.ThrowsException<ArgumentException>(() => new Comment(new String(new char[Constants.CONTENT_LEN_MAX + 1]), "author"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenAuthorIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Comment(new String(new char[Constants.CONTENT_LEN_MIN + 2]), null));
        }

    }
}
