﻿using System;
using Dealership.Models.Contracts;
using Dealership.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dealership.Tests.Models
{
    [TestClass]
    public class CarTests
    {
        [TestMethod]
        public void CarImpl_ShouldImplementCarInterface()
        {
            Car car = new Car("make", "model", 100, 4);
            // Assert
            Assert.IsInstanceOfType(car, typeof(ICar));
        }

        [TestMethod]
        public void CarImpl_ShouldImplementVehicleInterface()
        {
            Car car = new Car("make", "model", 100, 4);
            // Assert
            Assert.IsInstanceOfType(car, typeof(IVehicle));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeIsNull()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car(null, "model", 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("m", "model", 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("makeLongCar12345", "model", 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelIsNull()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", null, 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "", 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsAbove15()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "longLongModel123", 100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsNegative()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "model", -100, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsAbove1000000()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "model", 1000001, 4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenSeatsIsNegative()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "model", 1000, -4));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenSeatsIsAbove10()
        {
            // Act
            Assert.ThrowsException<ArgumentException>(() => new Car("make", "model", 1000, 11));
        }
    }
}
