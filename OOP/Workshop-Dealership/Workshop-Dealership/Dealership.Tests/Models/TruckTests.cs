﻿using System;
using Dealership.Models.Contracts;
using Dealership.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Models.Common;

namespace Dealership.Tests.Models
{
    [TestClass]
    public class TruckTests
    {

        [TestMethod]
        public void TruckImpl_ShouldImplementTruckInterface()
        {
            Truck truck = new Truck("make", "model", 100, 10);
            Assert.IsInstanceOfType(truck, typeof(ITruck));
        }

        [TestMethod]
        public void TruckImpl_ShouldImplementVehicleInterface()
        {
            Truck truck = new Truck("make", "model", 100, 10);
            Assert.IsInstanceOfType(truck, typeof(IVehicle));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck(null, "model", 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", null, 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck(new String(new char[Constants.MAKE_NAME_LEN_MIN - 1]), "model", 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck(new String(new char[Constants.MAKE_NAME_LEN_MAX + 1]), "model", 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", new String(new char[Constants.MODEL_NAME_LEN_MIN - 1]), 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsAbove15()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", new String(new char[Constants.MODEL_NAME_LEN_MAX + 1]), 100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsNegative()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", "model", -100, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsAbove1000000()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", "model", 1000001, 10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenWeightCapacityIsNegative()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", "model", 100, -10));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenWeightCapacityIsAbove100()
        {
            Assert.ThrowsException<ArgumentException>(() => new Truck("make", "model", 100, 101));
        }

    }
}
