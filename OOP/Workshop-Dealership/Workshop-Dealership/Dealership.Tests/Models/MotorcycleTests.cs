﻿using System;
using Dealership.Models.Contracts;
using Dealership.Models;
using Dealership.Models.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dealership.Tests.Models
{
    [TestClass]
    public class MotorcycleTests
    {

        [TestMethod]
        public void MotorcycleImpl_ShouldImplementMotorcycleInterface()
        {
            Motorcycle motorcycle = new Motorcycle("make", "model", 100, "category");
            Assert.IsInstanceOfType(motorcycle, typeof(IMotorcycle));
        }

        [TestMethod]
        public void MotorcycleImpl_ShouldImplementVehicleInterface()
        {
            Motorcycle motorcycle = new Motorcycle("make", "model", 100, "category");
            Assert.IsInstanceOfType(motorcycle, typeof(IVehicle));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle(null, "model", 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle(new String(new char[Constants.MAKE_NAME_LEN_MIN - 1]), "model", 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle(new String(new char[Constants.MAKE_NAME_LEN_MAX + 1]), "model", 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", null, 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", new String(new char[Constants.MODEL_NAME_LEN_MIN - 1]), 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenModelLengthIsAbove15()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", new String(new char[Constants.MODEL_NAME_LEN_MAX + 1]), 100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsNegative()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", "model", -100, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPriceIsAbove1000000()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", "model", 1000001, "category"));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenCategoryIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", "model", 100, null));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenCategoryLengthIsBelow_3()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", "model", 100, new String(new char[Constants.CATEGORY_LEN_MIN - 1])));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenCategoryLengthIsAbove10()
        {
            Assert.ThrowsException<ArgumentException>(() => new Motorcycle("make", "model", 100, new String(new char[Constants.CATEGORY_LEN_MAX + 1])));
        }
    }
}
