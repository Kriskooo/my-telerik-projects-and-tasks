﻿using System;
using Dealership.Models.Common.Enums;
using Dealership.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Models.Common;

namespace Dealership.Tests.Models
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void Constructor_ShouldThrow_WhenUsernameIsNull()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User(null,
                        "firstName",
                        "lastName",
                        "password",
                        Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenUsernameIsBelowMinLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User(new String(new char[Constants.USERNAME_LEN_MIN - 1]),
                   "firstName",
                   "lastName",
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenUsernameIsAboveMaxLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User(new String(new char[Constants.USERNAME_LEN_MAX + 1]),
                   "firstName",
                   "lastName",
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenUsernameDoesNotMatchPattern()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("U$$ernam3",
                   "firstName",
                   "lastName",
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenFirstNameIsNull()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   null,
                   "lastName",
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenFirstnameIsBelowMinLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("Username",
                   new String(new char[Constants.FIRSTNAME_LEN_MIN - 1]),
                   "lastName",
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenFirstnameIsAboveMaxLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("Username",
                   new String(new char[Constants.FIRSTNAME_LEN_MAX + 1]),
                   "lastName",
                   "password",
                   Role.Normal));
        }


        [TestMethod]
        public void Constructor_ShouldThrow_WhenLastNameIsNull()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   null,
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenLastNameIsBelowMinLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   new String(new char[Constants.LASTNAME_LEN_MIN - 1]),
                   "password",
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenLastNameIsAboveMaxLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   new String(new char[Constants.LASTNAME_LEN_MAX + 1]),
                   "password",
                   Role.Normal));
        }


        [TestMethod]
        public void Constructor_ShouldThrow_WhenPasswordIsNull()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   "lastName",
                   null,
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPasswordIsBelowMinLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   "lastName",
                   new String(new char[Constants.PASSWORD_LEN_MIN - 1]),
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPasswordIsAboveMaxLength()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("username",
                   "firstName",
                   "lastName",
                   new String(new char[Constants.PASSWORD_LEN_MAX + 1]),
                   Role.Normal));
        }

        [TestMethod]
        public void Constructor_ShouldThrow_WhenPasswordDoesNotMatchPattern()
        {
            //Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() => new User("Username",
                   "firstName",
                   "lastName",
                   "Pa-$$_w0rD",
                   Role.Normal));
        }
    }
}
