﻿using Dealership.Models.Common.Enums;
using Dealership.Models;
using Dealership.Models.Contracts;

namespace Dealership.Tests.Utils
{
    public class Factory
    {
        public static ICar CreateCar()
        {
            return new Car("Make", "Model", 20, 4);
        }
        public static IUser CreateNormalUser()
        {
            return new User("username", "firstname", "lastname", "password", Role.Normal);
        }
        public static IMotorcycle CreateMotorcycle()
        {
            return new Motorcycle("Kawasaki", "Z1000", 2500, "Race");
        }
        public static ITruck CreateTruck()
        {
            return new Truck("Volvo", "FH4", 11800, 50);
        }
        public static IComment CreateComment(IUser user)
        {
            return new Comment("{{I like this vehicle!}}", user.Username);
        }
    }
}
