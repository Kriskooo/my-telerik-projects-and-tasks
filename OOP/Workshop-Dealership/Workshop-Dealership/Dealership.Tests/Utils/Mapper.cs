﻿using System.Collections.Generic;
using Dealership.Models.Contracts;

namespace Dealership.Tests.Utils
{
    public class Mapper
    {
        public static List<string> MapTruckToParamsList(ITruck truck)
        {
            List<string> result = MapVehicleToParamsList(truck);
            result.Add(truck.WeightCapacity.ToString());
            return result;
        }
        public static List<string> MapCarToParamsList(ICar car)
        {
            List<string> result = MapVehicleToParamsList(car);
            result.Add(car.Seats.ToString());
            return result;
        }
        public static List<string> MapMotorcycleToParamsList(IMotorcycle motorcycle)
        {
            List<string> result = MapVehicleToParamsList(motorcycle);
            result.Add(motorcycle.Category);
            return result;
        }
        private static List<string> MapVehicleToParamsList(IVehicle vehicle)
        {
            return new List<string>() {
                            vehicle.Type.ToString(),
                            vehicle.Make,
                            vehicle.Model,
                            vehicle.Price.ToString()
                        };
        }
        public static List<string> MapUserToParamsList(IUser user)
        {
            return new List<string>()
            {
                user.Username,
                user.FirstName,
                user.LastName,
                user.Password
            };
        }
        public static List<string> MapCommentToParamsList(IComment comment)
        {
            return new List<string>()
            {
                comment.Content,
                comment.Author,
                1.ToString()
            };
        }
    }
}
