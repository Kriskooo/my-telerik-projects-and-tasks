﻿using System.Collections.Generic;
using Dealership.Core.Factories;
using Dealership.Core;
using Dealership.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Core.Contracts;
using Dealership.Commands.Contracts;
using Dealership.Commands;
using Dealership.Tests.Utils;

namespace Dealership.Tests.Core
{
    [TestClass]
    public class DealershipFactoryTests
    {
        private IDealershipRepository dealershipRepository;
        private IDealershipFactory dealershipFactory;
        private ICommandFactory commandFactory;

        private void Prepare()
        {
            this.commandFactory = new CommandFactory();
            this.dealershipFactory = new DealershipFactory();
            this.dealershipRepository = new DealershipRepository();
        }

        [TestMethod]
        public void CreateCar_ShouldCreate_WhenInputIsValid()
        {
            this.Prepare();

            LogInUser(Factory.CreateNormalUser());

            // Act
            ICommand createVehicle = commandFactory.CreateCommand("AddVehicle", dealershipFactory, dealershipRepository);
            ICar testCar = Factory.CreateCar();
            List<string> parameters = Mapper.MapCarToParamsList(testCar);

            // Act
            createVehicle.Execute(parameters);

            //Assert
            ICar toCompare = (ICar)dealershipRepository.LoggedUser.Vehicles[0];
            Assert.AreEqual(testCar.Make, toCompare.Make);
            Assert.AreEqual(testCar.Seats, toCompare.Seats);

        }

        [TestMethod]
        public void CreateMotorcycle_ShouldCreate_WhenInputIsValid()
        {
            this.Prepare();

            LogInUser(Factory.CreateNormalUser());

            // Act
            ICommand createVehicle = commandFactory.CreateCommand("AddVehicle", dealershipFactory, dealershipRepository);
            IMotorcycle testMotorcycle = Factory.CreateMotorcycle();
            List<string> parameters = Mapper.MapMotorcycleToParamsList(testMotorcycle);

            // Act
            createVehicle.Execute(parameters);

            //Assert
            IMotorcycle toCompare = (IMotorcycle)dealershipRepository.LoggedUser.Vehicles[0];
            Assert.AreEqual(testMotorcycle.Make, toCompare.Make);
            Assert.AreEqual(testMotorcycle.Category, toCompare.Category);

        }

        [TestMethod]
        public void CreateTruck_ShouldCreate_WhenInputIsValid()
        {
            this.Prepare();

            LogInUser(Factory.CreateNormalUser());

            // Act
            ICommand createVehicle = commandFactory.CreateCommand("AddVehicle", dealershipFactory, dealershipRepository);
            ITruck testTruck = Factory.CreateTruck();
            List<string> parameters = Mapper.MapTruckToParamsList(testTruck);

            // Act
            createVehicle.Execute(parameters);

            //Assert
            ITruck toCompare = (ITruck)dealershipRepository.LoggedUser.Vehicles[0];
            Assert.AreEqual(testTruck.Make, toCompare.Make);
            Assert.AreEqual(testTruck.WeightCapacity, toCompare.WeightCapacity);

        }

        [TestMethod]
        public void CreateUser_ShouldCreate_WhenInputIsValid()
        {
            this.Prepare();

            // Act
            ICommand createVehicle = commandFactory.CreateCommand("RegisterUser", dealershipFactory, dealershipRepository);
            IUser testUser = Factory.CreateNormalUser();
            List<string> parameters = Mapper.MapUserToParamsList(testUser);

            // Act
            createVehicle.Execute(parameters);

            //Assert
            IUser toCompare = dealershipRepository.Users[0];
            Assert.AreEqual(toCompare.Username, testUser.Username);
        }

        [TestMethod]
        public void CreateComment_ShouldCreate_WhenInputIsValid()
        {
            this.Prepare();

            IUser user = Factory.CreateNormalUser();
            IVehicle vehicle = Factory.CreateCar();
            LogInUser(user);
            AddVehicleToUser(user, vehicle);

            // Act
            ICommand createVehicle = commandFactory.CreateCommand("AddComment",
                    dealershipFactory,
                    dealershipRepository);
            IComment testComment = Factory.CreateComment(user);
            List<string> parameters = Mapper.MapCommentToParamsList(testComment);

            // Act
            createVehicle.Execute(parameters);

            //Assert
            IComment toCompare = dealershipRepository.LoggedUser.Vehicles[0].Comments[0];
            Assert.AreEqual(toCompare.Content, testComment.Content);
            Assert.AreEqual(toCompare.Author, testComment.Author);
        }

        private void LogInUser(IUser userToLogIn)
        {
            dealershipRepository.AddUser(userToLogIn);
            ICommand login = new LoginCommand(dealershipFactory, dealershipRepository);
            List<string> parameters = new List<string>();
            parameters.Add(userToLogIn.Username);
            parameters.Add(userToLogIn.Password);
            login.Execute(parameters);
        }

        private void AddVehicleToUser(IUser user, IVehicle vehicle)
        {
            ICommand createVehicle = commandFactory.CreateCommand("AddVehicle",
                    dealershipFactory,
                    dealershipRepository);
            ICar testCar = Factory.CreateCar();
            List<string> parameters = Mapper.MapCarToParamsList(testCar);

            createVehicle.Execute(parameters);
        }
    }
}
