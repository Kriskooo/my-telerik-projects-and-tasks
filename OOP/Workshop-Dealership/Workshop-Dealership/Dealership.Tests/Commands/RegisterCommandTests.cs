﻿using System.Collections.Generic;
using Dealership.Core.Factories;
using Dealership.Core;
using Dealership.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Commands.Contracts;

namespace Dealership.Tests.Commands
{
    [TestClass]
    public class RegisterCommand_Tests
    {

        private DealershipRepository dealershipRepository;
        private DealershipFactory dealershipFactory;
        private CommandFactory commandFactory;

        private void Prepare()
        {
            this.commandFactory = new CommandFactory();
            this.dealershipFactory = new DealershipFactory();
            this.dealershipRepository = new DealershipRepository();
        }
        [TestMethod]
        public void Execute_ShouldRegisterUser_WhenUserDoesNotExist()
        {
            Prepare();
            // Arrange
            ICommand registerUser = commandFactory.CreateCommand("registeruser", dealershipFactory, dealershipRepository);
            List<string> parameters = new List<string>();
            parameters.Add("pesho123");
            parameters.Add("petar");
            parameters.Add("petrov");
            parameters.Add("password");

            // Act
            registerUser.Execute(parameters);

            // Assert
            IUser pesho = dealershipRepository.Users[0];
            Assert.AreEqual("pesho123", pesho.Username);
        }

        [TestMethod]
        public void Execute_ShouldNotRegisterUser_WhenUserAlreadyExist()
        {
            Prepare();
            // Arrange
            ICommand registerUser = commandFactory.CreateCommand("registeruser", dealershipFactory, dealershipRepository);
            List<string> parameters = new List<string>();
            parameters.Add("pesho123");
            parameters.Add("petar");
            parameters.Add("petrov");
            parameters.Add("password");

            // Act
            registerUser.Execute(parameters);
            registerUser.Execute(parameters);

            // Assert
            Assert.AreEqual(1, dealershipRepository.Users.Count);
        }
    }
}
