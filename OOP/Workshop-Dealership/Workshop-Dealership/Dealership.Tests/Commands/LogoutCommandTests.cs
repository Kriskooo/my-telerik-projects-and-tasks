﻿using System.Collections.Generic;
using Dealership.Core.Factories;
using Dealership.Core;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using Dealership.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Commands.Contracts;
using Dealership.Commands;

namespace Dealership.Tests.Commands
{
    [TestClass]
    public class LogoutCommand_Tests
    {

        private DealershipRepository dealershipRepository;
        private DealershipFactory dealershipFactory;
        private CommandFactory commandFactory;

        private void Prepare()
        {
            this.commandFactory = new CommandFactory();
            this.dealershipFactory = new DealershipFactory();
            this.dealershipRepository = new DealershipRepository();
        }
        [TestMethod]
        public void Execute_ShouldLogoutUser()
        {
            Prepare();
            // Arrange
            IUser userToLogIn = new User("pesho123", "petar", "petrov", "password", Role.Normal);
            dealershipRepository.LoggedUser = userToLogIn;
            ICommand logout = new LogoutCommand(dealershipFactory, dealershipRepository);

            // Act
            logout.Execute(new List<string>());

            // Assert
            Assert.IsNull(dealershipRepository.LoggedUser);
        }
    }
}
