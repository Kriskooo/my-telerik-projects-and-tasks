﻿using System.Collections.Generic;
using Dealership.Core.Factories;
using Dealership.Core;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using Dealership.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dealership.Commands.Contracts;
using Dealership.Commands;

namespace Dealership.Tests.Commands
{
    [TestClass]
    public class Login_Tests
    {

        private DealershipRepository dealershipRepository;
        private DealershipFactory dealershipFactory;
        private CommandFactory commandFactory;

        private void Prepare()
        {
            this.commandFactory = new CommandFactory();
            this.dealershipFactory = new DealershipFactory();
            this.dealershipRepository = new DealershipRepository();
        }
        [TestMethod]
        public void Execute_ShouldLoginUser_WhenUserNotLoggedIn()
        {
            this.Prepare();

            // Arrange
            IUser userToLogIn = new User("pesho123", "petar", "petrov", "password", Role.Normal);
            dealershipRepository.AddUser(userToLogIn);
            ICommand login = new LoginCommand(dealershipFactory, dealershipRepository);
            List<string> parameters = new List<string>();
            parameters.Add("pesho123");
            parameters.Add("password");

            // Act
            login.Execute(parameters);

            // Assert
            Assert.AreEqual("pesho123", dealershipRepository.LoggedUser.Username);
        }

        [TestMethod]
        public void Execute_ShouldNotLoginUser_WhenPasswordIsWrong()
        {
            this.Prepare();

            // Arrange
            IUser userToLogIn = new User("pesho123", "petar", "petrov", "password", Role.Normal);
            dealershipRepository.AddUser(userToLogIn);
            ICommand login = commandFactory.CreateCommand("login", dealershipFactory, dealershipRepository);
            List<string> parameters = new List<string>();
            parameters.Add("pesho123");
            parameters.Add("password123");

            // Act
            login.Execute(parameters);

            // Assert
            Assert.IsNull(dealershipRepository.LoggedUser);
        }

    }
}
