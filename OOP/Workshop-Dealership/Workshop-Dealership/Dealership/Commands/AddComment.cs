﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Common;
using Dealership.Models.Contracts;

namespace Dealership.Commands
{
    public class AddCommentCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public AddCommentCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            String content = parameters[0];
            String author = parameters[1];
            int vehicleIndex = int.Parse(parameters[2]) - 1;

            return AddComment(content, vehicleIndex, author);
        }
        private string AddComment(String content, int vehicleIndex, String author)
        {
            IComment comment = dealershipFactory.CreateComment(content, dealershipRepository.LoggedUser.Username);
            IUser user = dealershipRepository.Users.FirstOrDefault(u => u.Username.ToLower().Equals(author.ToLower()));
            if (user == null)
            {
                return string.Format(CommandConstants.NO_SUCH_USER, author);
            }

            Validator.ValidateIntRange(vehicleIndex, 0, user.Vehicles.Count, CommandConstants.VEHICLE_DOES_NOT_EXIST);

            IVehicle vehicle = user.Vehicles[vehicleIndex];

            dealershipRepository.LoggedUser.AddComment(comment, vehicle);

            return string.Format(CommandConstants.COMMENT_ADDED_SUCCESSFULLY, dealershipRepository.LoggedUser.Username);
        }
    }
}
