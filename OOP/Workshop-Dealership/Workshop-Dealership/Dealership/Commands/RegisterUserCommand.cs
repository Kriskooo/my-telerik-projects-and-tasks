﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;

namespace Dealership.Commands
{
    public class RegisterUserCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public RegisterUserCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            string username = parameters[0];
            string firstName = parameters[1];
            string lastName = parameters[2];
            string password = parameters[3];

            Role role = Role.Normal;

            if (parameters.Count > CommandConstants.NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE)
            {
                role = Enum.Parse<Role>(parameters[4], true);
            }

            return registerUser(username, firstName, lastName, password, role);
        }

        private string registerUser(string username, string firstName, string lastName, string password, Role role)
        {
            if (dealershipRepository.LoggedUser != null)
            {
                return string.Format(CommandConstants.USER_LOGGED_IN_ALREADY, dealershipRepository.LoggedUser.Username);
            }

            if (dealershipRepository.Users.Any(u => u.Username.ToLower().Equals(username.ToLower())))
            {

                return string.Format(CommandConstants.USER_ALREADY_EXIST, username);
            }

            IUser user = dealershipFactory.CreateUser(username, firstName, lastName, password, role.ToString());
            dealershipRepository.LoggedUser = user;
            dealershipRepository.AddUser(user);

            return string.Format(CommandConstants.USER_REGISTERED, username);
        }
    }
}
