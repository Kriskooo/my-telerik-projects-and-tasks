﻿using System;
using System.Collections.Generic;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Contracts;
using Dealership.Models;
using Dealership.Models.Common;

namespace Dealership.Commands
{
    public class RemoveVehicleCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public RemoveVehicleCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            int vehicleIndex = int.Parse(parameters[0]) - 1;
            return RemoveVehicle(vehicleIndex);
        }

        private string RemoveVehicle(int vehicleIndex)
        {
            IUser loggedUser = dealershipRepository.LoggedUser;
            Validator.ValidateIntRange(vehicleIndex, 0, loggedUser.Vehicles.Count, CommandConstants.REMOVED_VEHICLE_DOES_NOT_EXIST);

            IVehicle vehicle = loggedUser.Vehicles[vehicleIndex];

            loggedUser.RemoveVehicle(vehicle);

            return string.Format(CommandConstants.VEHICLE_REMOVED_SUCCESSFULLY, loggedUser.Username);
        }
    }
}
