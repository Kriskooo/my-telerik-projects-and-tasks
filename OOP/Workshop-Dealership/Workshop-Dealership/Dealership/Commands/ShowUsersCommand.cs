﻿using System;
using System.Collections.Generic;
using System.Text;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Common.Enums;

namespace Dealership.Commands
{
    public class ShowUsersCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public ShowUsersCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            if (this.dealershipRepository.LoggedUser.Role != Role.Admin)
            {
                throw new ArgumentException("You are not an admin!");
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("--USERS--");
            int count = 1;
            foreach (var user in this.dealershipRepository.Users)
            {
                sb.AppendLine($"{count++}. {user.ToString()}");
            }

            return sb.ToString().Trim();
        }
    }
}
