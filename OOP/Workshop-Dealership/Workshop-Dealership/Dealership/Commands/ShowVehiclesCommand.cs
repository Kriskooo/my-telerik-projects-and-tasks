﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models;
using Dealership.Models.Contracts;

namespace Dealership.Commands
{
    public class ShowVehiclesCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public ShowVehiclesCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            string username = parameters[0];

            return ShowUserVehicles(username);
        }

        private string ShowUserVehicles(string username)
        {
            IUser user = dealershipRepository.Users.FirstOrDefault(u => u.Username.ToLower().Equals(username.ToLower()));

            if (user == null)
            {
                return string.Format(CommandConstants.NO_SUCH_USER, username);
            }

            return user.PrintVehicles();
        }
    }
}
