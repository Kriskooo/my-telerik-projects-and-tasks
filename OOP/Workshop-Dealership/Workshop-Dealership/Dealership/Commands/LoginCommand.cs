﻿using System.Collections.Generic;
using System.Linq;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Contracts;

namespace Dealership.Commands
{
    public class LoginCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public LoginCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            string username = parameters[0];
            string password = parameters[1];

            return Login(username, password);
        }
        private string Login(string username, string password)
        {
            if (dealershipRepository.LoggedUser != null)
            {
                return string.Format(CommandConstants.USER_LOGGED_IN_ALREADY, dealershipRepository.LoggedUser.Username);
            }

            IUser userFound = dealershipRepository.Users.FirstOrDefault(user => user.Username.ToLower().Equals(username.ToLower()));

            if (userFound != null && userFound.Password.Equals(password))
            {
                dealershipRepository.LoggedUser = userFound;
                return string.Format(CommandConstants.USER_LOGGED_IN, username);
            }

            return CommandConstants.WRONG_USERNAME_OR_PASSWORD;
        }
    }
}
