﻿namespace Dealership.Commands.Enums
{
    public enum CommandType
    {
        REGISTERUSER,
        LOGIN,
        LOGOUT,
        ADDVEHICLE,
        REMOVEVEHICLE,
        ADDCOMMENT,
        REMOVECOMMENT,
        SHOWUSERS,
        SHOWVEHICLES
    }
}
