﻿using System.Collections.Generic;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;

namespace Dealership.Commands
{
    public class LogoutCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public LogoutCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            dealershipRepository.LoggedUser = null;
            return CommandConstants.USER_LOGGED_OUT;
        }
    }
}
