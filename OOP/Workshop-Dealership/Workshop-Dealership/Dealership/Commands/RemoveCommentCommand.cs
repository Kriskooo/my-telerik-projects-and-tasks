﻿using System;
using System.Collections.Generic;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Contracts;
using Dealership.Models;
using System.Linq;
using Dealership.Models.Common;
using Dealership.Commands.Constants;

namespace Dealership.Commands
{
    public class RemoveCommentCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public RemoveCommentCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            int vehicleIndex = int.Parse(parameters[0]) - 1;
            int commentIndex = int.Parse(parameters[1]) - 1;
            string username = parameters[2];

            return RemoveComment(vehicleIndex, commentIndex, username);
        }

        private string RemoveComment(int vehicleIndex, int commentIndex, string username)
        {
            IUser user = dealershipRepository.Users.FirstOrDefault(u => u.Username.ToLower().Equals(username.ToLower()));

            if (user == null)
            {
                return string.Format(CommandConstants.NO_SUCH_USER, username);
            }

            Validator.ValidateIntRange(vehicleIndex, 0, user.Vehicles.Count, CommandConstants.REMOVED_VEHICLE_DOES_NOT_EXIST);
            Validator.ValidateIntRange(commentIndex, 0, user.Vehicles[vehicleIndex].Comments.Count, CommandConstants.REMOVED_COMMENT_DOES_NOT_EXIST);

            IVehicle vehicle = user.Vehicles[vehicleIndex];
            IComment comment = user.Vehicles[vehicleIndex].Comments[commentIndex];

            dealershipRepository.LoggedUser.RemoveComment(comment, vehicle);

            return string.Format(CommandConstants.COMMENT_REMOVED_SUCCESSFULLY, dealershipRepository.LoggedUser.Username);
        }
    }
}
