﻿namespace Dealership.Commands.Constants
{
    public class CommandConstants
    {
        public const string INVALID_COMMAND = "Invalid command! {0}";

        public const string USER_ALREADY_EXIST = "User {0} already exist. Choose a different username!";
        public const string USER_LOGGED_IN_ALREADY = "User {0} is logged in! Please log out first!";
        public const string USER_REGISTERED = "User {0} registered successfully!";
        public const string NO_SUCH_USER = "There is no user with username {0}!";
        public const string USER_LOGGED_OUT = "You logged out!";
        public const string USER_LOGGED_IN = "User {0} successfully logged in!";
        public const string WRONG_USERNAME_OR_PASSWORD = "Wrong username or password!";
        public const string YOU_ARE_NOT_AN_ADMIN = "You are not an admin!";

        public const string COMMENT_ADDED_SUCCESSFULLY = "{0} added comment successfully!";
        public const string COMMENT_REMOVED_SUCCESSFULLY = "{0} removed comment successfully!";

        public const string VEHICLE_REMOVED_SUCCESSFULLY = "{0} removed vehicle successfully!";
        public const string VEHICLE_ADDED_SUCCESSFULLY = "{0} added vehicle successfully!";

        public const string REMOVED_VEHICLE_DOES_NOT_EXIST = "Cannot remove comment! The vehicle does not exist!";
        public const string REMOVED_COMMENT_DOES_NOT_EXIST = "Cannot remove comment! The comment does not exist!";

        public const string COMMENT_DOES_NOT_EXIST = "The comment does not exist!";
        public const string VEHICLE_DOES_NOT_EXIST = "The vehicle does not exist!";
        public const string REPORT_SEPARATOR = "####################";
        public const int NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE = 4;
    }
}
