﻿using System.Collections.Generic;

namespace Dealership.Commands.Contracts
{
    public interface ICommand
    {
        string Execute(IList<string> parameters);
    }
}
