﻿using System;
using System.Collections.Generic;
using Dealership.Commands.Constants;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;

namespace Dealership.Commands
{
    public class AddVehicleCommand : ICommand
    {
        private readonly IDealershipFactory dealershipFactory;
        private readonly IDealershipRepository dealershipRepository;

        public AddVehicleCommand(IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            this.dealershipFactory = dealershipFactory;
            this.dealershipRepository = dealershipRepository;
        }

        public string Execute(IList<string> parameters)
        {
            string type = parameters[0];
            string make = parameters[1];
            string model = parameters[2];
            double price = Double.Parse((parameters[3]));
            string additionalParam = parameters[4];

            VehicleType typeEnum = Enum.Parse<VehicleType>(type, true);

            return АddVehicle(typeEnum, make, model, price, additionalParam);
        }

        private string АddVehicle(VehicleType type, string make, string model, double price, string additionalParam)
        {
            IVehicle vehicle = null;

            if (type == VehicleType.Car)
            {
                vehicle = dealershipFactory.CreateCar(make, model, price, int.Parse(additionalParam));
            }
            else if (type == VehicleType.Motorcycle)
            {
                vehicle = dealershipFactory.CreateMotorcycle(make, model, price, additionalParam);
            }
            else if (type == VehicleType.Truck)
            {
                vehicle = dealershipFactory.CreateTruck(make, model, price, int.Parse(additionalParam));
            }
            dealershipRepository.LoggedUser.AddVehicle(vehicle);

            return string.Format(CommandConstants.VEHICLE_ADDED_SUCCESSFULLY, dealershipRepository.LoggedUser.Username);
        }

    }
}