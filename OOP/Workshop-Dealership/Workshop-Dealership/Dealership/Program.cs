﻿using System;
using Dealership.Core;

namespace Dealership
{
    class Program
    {
        static void Main(string[] args)
        {
            var engine = new DealershipEngine();
            engine.Start();
        }
    }
}
