﻿using System.Collections.Generic;
using Dealership.Core.Contracts;
using Dealership.Models.Contracts;

namespace Dealership.Core
{
    public class DealershipRepository : IDealershipRepository
    {

        private readonly List<IUser> users;

        public DealershipRepository()
        {
            this.users = new List<IUser>();
        }

        public IList<IUser> Users
        {
            get
            {
                return new List<IUser>(this.users);
            }
        }

        public void AddUser(IUser userToAdd)
        {
            if (!this.users.Contains(userToAdd))
            {
                this.users.Add(userToAdd);
            }
        }

        public IUser LoggedUser { get; set; }
    }
}
