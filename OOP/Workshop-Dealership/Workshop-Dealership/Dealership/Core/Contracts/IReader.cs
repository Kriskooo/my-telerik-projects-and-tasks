﻿namespace Dealership.Core.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}
