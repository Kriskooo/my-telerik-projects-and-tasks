﻿using Dealership.Models.Contracts;

namespace Dealership.Core.Contracts
{
    public interface IDealershipFactory
    {
        IUser CreateUser(string username, string firstName, string lastName, string password, string role);

        IComment CreateComment(string content, string author);

        ICar CreateCar(string make, string model, double price, int seats);

        IMotorcycle CreateMotorcycle(string make, string model, double price, string category);

        ITruck CreateTruck(string make, string model, double price, int weightCapacity);
    }
}
