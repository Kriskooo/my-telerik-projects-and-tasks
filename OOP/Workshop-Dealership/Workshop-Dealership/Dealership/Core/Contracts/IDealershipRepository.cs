﻿using System.Collections.Generic;
using Dealership.Models.Contracts;

namespace Dealership.Core.Contracts
{
    public interface IDealershipRepository
    {
        IList<IUser> Users { get; }
        IUser LoggedUser { get; set; }
        void AddUser(IUser userToAdd);
    }
}
