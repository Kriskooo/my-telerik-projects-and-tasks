﻿using Dealership.Commands.Contracts;

namespace Dealership.Core.Contracts
{
    public interface ICommandFactory
    {
        ICommand CreateCommand(string commandTypeAsString, IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository);
    }
}
