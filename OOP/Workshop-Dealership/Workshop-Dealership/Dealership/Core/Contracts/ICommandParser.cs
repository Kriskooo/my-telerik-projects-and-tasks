﻿using System.Collections.Generic;

namespace Dealership.Core.Contracts
{
    public interface ICommandParser
    {
        string ParseCommand(string fullCommand);
        IList<string> ParseParameters(string fullCommand);
    }
}
