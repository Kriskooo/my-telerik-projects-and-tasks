﻿using System;
using Dealership.Core.Contracts;

namespace Dealership.Core.Providers
{
    public class ConsoleReader : IReader
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
