﻿using System;
using System.Collections.Generic;
using Dealership.Commands.Contracts;
using Dealership.Core.Contracts;
using Dealership.Core.Factories;
using Dealership.Core.Providers;

namespace Dealership.Core
{
    public class DealershipEngine : IDealershipEngine
    {

        private const string TERMINATION_COMMAND = "exit";
        private const string USER_NOT_LOGGED = "You are not logged! Please login first!";
        private const string REPORT_SEPARATOR = "####################";

        private readonly IDealershipFactory dealershipFactory;
        private readonly IWriter writer;
        private readonly IReader reader;
        private readonly CommandParser commandParser;
        private readonly DealershipRepository dealershipRepository;
        private readonly CommandFactory commandFactory;

        public DealershipEngine()
        {
            dealershipFactory = new DealershipFactory();
            writer = new ConsoleWriter();
            reader = new ConsoleReader();
            commandParser = new CommandParser();
            dealershipRepository = new DealershipRepository();
            commandFactory = new CommandFactory();
        }

        public void Start()
        {

            while (true)
            {
                try
                {
                    string commandAsString = reader.ReadLine();
                    if (commandAsString == TERMINATION_COMMAND)
                    {
                        break;
                    }
                    processCommand(commandAsString);
                }
                catch (Exception ex)
                {
                    writer.WriteLine(!string.IsNullOrEmpty(ex.Message) ? ex.Message : ex.ToString());
                    writer.WriteLine(REPORT_SEPARATOR);
                }
            }
        }

        private void processCommand(string commandAsString)
        {
            if (commandAsString == null || commandAsString.Trim().Equals(""))
            {
                throw new ArgumentException("Command cannot be null or empty.");
            }

            string commandTypeAsString = commandParser.ParseCommand(commandAsString);
            CheckPermissionToExecute(commandTypeAsString);
            ICommand command = commandFactory.CreateCommand(commandTypeAsString, dealershipFactory, dealershipRepository);
            IList<string> parameters = commandParser.ParseParameters(commandAsString);
            string executionResult = command.Execute(parameters);
            writer.WriteLine(executionResult);
            writer.WriteLine(REPORT_SEPARATOR);

        }

        private void CheckPermissionToExecute(string commandTypeAsString)
        {
            if (!(commandTypeAsString.ToUpper().Equals("REGISTERUSER")) && !commandTypeAsString.ToUpper().Equals("LOGIN"))
            {
                if (dealershipRepository.LoggedUser == null)
                {
                    throw new ArgumentException(USER_NOT_LOGGED);
                }
            }
        }

    }
}
