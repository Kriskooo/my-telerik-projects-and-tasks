﻿using System;
using Dealership.Commands;
using Dealership.Commands.Contracts;
using Dealership.Commands.Enums;
using Dealership.Core.Contracts;

namespace Dealership.Core.Factories
{
    public class CommandFactory : ICommandFactory
    {
        private const string INVALID_COMMAND = "Invalid command name: {0}!";

        public ICommand CreateCommand(string commandTypeAsString, IDealershipFactory dealershipFactory, IDealershipRepository dealershipRepository)
        {
            CommandType commandType = Enum.Parse<CommandType>(commandTypeAsString, true);
            switch (commandType)
            {
                case CommandType.LOGIN:
                    return new LoginCommand(dealershipFactory, dealershipRepository);
                case CommandType.LOGOUT:
                    return new LogoutCommand(dealershipFactory, dealershipRepository);
                case CommandType.SHOWUSERS:
                    return new ShowUsersCommand(dealershipFactory, dealershipRepository);
                case CommandType.ADDCOMMENT:
                    return new AddCommentCommand(dealershipFactory, dealershipRepository);
                case CommandType.ADDVEHICLE:
                    return new AddVehicleCommand(dealershipFactory, dealershipRepository);
                case CommandType.REGISTERUSER:
                    return new RegisterUserCommand(dealershipFactory, dealershipRepository);
                case CommandType.SHOWVEHICLES:
                    return new ShowVehiclesCommand(dealershipFactory, dealershipRepository);
                case CommandType.REMOVECOMMENT:
                    return new RemoveCommentCommand(dealershipFactory, dealershipRepository);
                case CommandType.REMOVEVEHICLE:
                    return new RemoveVehicleCommand(dealershipFactory, dealershipRepository);
            }
            throw new ArgumentException(string.Format(INVALID_COMMAND, commandTypeAsString));
        }
    }
}
