﻿using System;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using Dealership.Models;
using Dealership.Core.Contracts;

namespace Dealership.Core.Factories
{
    public class DealershipFactory : IDealershipFactory
    {
        public ICar CreateCar(string make, string model, double price, int seats)
        {
            ICar car = new Car(make, model, price, seats);

            return car;
        }

        public IMotorcycle CreateMotorcycle(string make, string model, double price, string category)
        {
            IMotorcycle motorcycle = new Motorcycle(make, model, price, category);

            return motorcycle;
        }

        public ITruck CreateTruck(string make, string model, double price, int weightCapacity)
        {
            ITruck truck = new Truck(make, model, price, weightCapacity);

            return truck;
        }

        public IUser CreateUser(string username, string firstName, string lastName, string password, string role)
        {
            IUser user = null;

            if (role == Role.Normal.ToString())
            {
                user = new User(username, firstName, lastName, password);
            }
            else
            {
                user = new User(username, firstName, lastName, password, Enum.Parse<Role>(role));
            }

            return user;
        }

        public IComment CreateComment(string content, string author)
        {
            IComment comment = new Comment(content, author);

            return comment;
        }
    }
}
