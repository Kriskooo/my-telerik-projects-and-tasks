﻿using System.Globalization;

namespace Dealership.Models.Common
{
    public class Utils
    {
        public static string RemoveTrailingZerosFromDouble(double value)
        {
            return value.ToString("G29", CultureInfo.InvariantCulture);
        }
    }
}
