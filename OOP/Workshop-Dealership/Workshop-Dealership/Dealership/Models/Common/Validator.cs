﻿using System;
using System.Text.RegularExpressions;

namespace Dealership.Models.Common
{
    public class Validator
    {
        public static void ValidateIntRange(int value, int min, int max, string message)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateDecimalRange(double value, double min, double max, string message)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidatePattern(string value, string pattern, string message)
        {
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = regex.Matches(value);
            if (matches.Count == 0)
            {

                throw new ArgumentException(message);
            }
        }

        public static void ValidateArgumentIsNotNull(object arg, string message)
        {
            if (arg == null)
            {
                throw new ArgumentException(message);
            }
        }
    }
}
