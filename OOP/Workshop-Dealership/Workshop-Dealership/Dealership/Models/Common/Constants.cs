﻿namespace Dealership.Models.Common
{
    public class Constants
    {
        public const string VEHICLE_NULL_ERR = "Vehicle cannot be null!";

        public const int MODEL_NAME_LEN_MIN = 1;
        public const int MODEL_NAME_LEN_MAX = 15;
        public const string MODEL_NULL_ERR = "Model must not be NULL!";
        public static string MODEL_NAME_LEN_ERR = $"Model must be between {MODEL_NAME_LEN_MIN} and {MODEL_NAME_LEN_MAX} characters long!";

        public const int MAKE_NAME_LEN_MIN = 2;
        public const int MAKE_NAME_LEN_MAX = 15;
        public const string MAKE_NULL_ERR = "Make must not be NULL!";
        public static string MAKE_NAME_LEN_ERR = $"Make must be between {MAKE_NAME_LEN_MIN} and {MAKE_NAME_LEN_MAX} characters long!";

        public const double PRICE_VAL_MIN = 0;
        public const double PRICE_VAL_MAX = 1000000;
        public static string PRICE_VAL_ERR = $"Price must be between {PRICE_VAL_MIN} and {PRICE_VAL_MAX}!";

        public const string MAKE_FIELD = "Make";
        public const string MODEL_FIELD = "Model";
        public const string PRICE_FIELD = "Price";
        public const string WHEELS_FIELD = "Wheels";
        public const string COMMENTS_HEADER = "    --COMMENTS--";
        public const string NO_COMMENTS_HEADER = "    --NO COMMENTS--";

        public const int CATEGORY_LEN_MIN = 3;
        public const int CATEGORY_LEN_MAX = 10;
        public const string CATEGORY_NULL_ERR = "Category must not be NULL!";
        public static string CATEGORY_LEN_ERR = $"Category must be between {CATEGORY_LEN_MIN} and {CATEGORY_LEN_MAX} characters long!";

        public const int WEIGHT_CAP_MIN = 1;
        public const int WEIGHT_CAP_MAX = 100;
        public static string WEIGHT_CAP_ERR = $"Weight capacity must be between {WEIGHT_CAP_MIN} and {WEIGHT_CAP_MAX}!";

        public const int CAR_SEATS_MIN = 1;
        public const int CAR_SEATS_MAX = 10;
        public static string CAR_SEATS_ERR = $"Seats must be between {CAR_SEATS_MIN} and {CAR_SEATS_MAX}!";

        public const int CONTENT_LEN_MIN = 3;
        public const int CONTENT_LEN_MAX = 200;
        public const string AUTHOR_NULL_ERR = "Author must not be NULL!";
        public const string CONTENT_NULL_ERR = "Content must not be NULL!";
        public const string COMMENT_NULL_ERR = "Comment must not be NULL!";
        public static string CONTENT_LEN_ERR = $"Content must be between {CONTENT_LEN_MIN} and {CONTENT_LEN_MAX} characters long!";

        public const string YOU_ARE_NOT_THE_AUTHOR = "You are not the author of the comment you are trying to remove!";

        public const string COMMENT_HEADER = "    ----------";
        public const string CONTENT_FIELD = "Content";
        public const string COMMENT_INDENTATION = "    ";
        public const string AUTHOR_HEADER = "      User: ";

        public const int USERNAME_LEN_MIN = 2;
        public const int USERNAME_LEN_MAX = 20;
        public const string USERNAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
        public const string USERNAME_NULL_ERR = "Username must not be NULL!";
        public const string USERNAME_PATTERN_ERR = "Username contains invalid symbols!";
        public static string USERNAME_LEN_ERR = $"Username must be between {USERNAME_LEN_MIN} and {USERNAME_LEN_MAX} characters long!";

        public const int PASSWORD_LEN_MIN = 5;
        public const int PASSWORD_LEN_MAX = 30;
        public const string PASSWORD_REGEX_PATTERN = "^[A-Za-z0-9@*_-]+$";
        public const string PASSWORD_NULL_ERR = "Password must not be NULL!";
        public const string PASSWORD_PATTERN_ERR = "Username contains invalid symbols!";
        public static string PASSWORD_LEN_ERR = $"Password must be between {PASSWORD_LEN_MIN} and {PASSWORD_LEN_MAX} characters long!";

        public const int LASTNAME_LEN_MIN = 2;
        public const int LASTNAME_LEN_MAX = 20;
        public static string LASTNAME_LEN_ERR = $"Lastname must be between {LASTNAME_LEN_MIN} and {LASTNAME_LEN_MAX} characters long!";
        public const string LASTNAME_NULL_ERR = "Lastname must not be NULL!";

        public const int FIRSTNAME_LEN_MIN = 2;
        public const int FIRSTNAME_LEN_MAX = 20;
        public static string FIRSTNAME_LEN_ERR = $"Firstname must be between {FIRSTNAME_LEN_MIN} and {FIRSTNAME_LEN_MAX} characters long!";
        public const string FIRSTNAME_NULL_ERR = "Firstname must not be NULL!";

        public const string NOT_AN_VIP_USER_VEHICLES_ADD = "You are not VIP and cannot add more than {0} vehicles!";
        public const string ADMIN_CANNOT_ADD_VEHICLES = "You are an admin and therefore cannot add vehicles!";
        public const int VIP_MAX_VEHICLES_TO_ADD = 5;

        public const string USER_TO_STRING = "Username: {0}, FullName: {1} {2}, Role: {3}";
        public const string USERNAME_FIELD = "Username";
        public const string FIRST_NAME_FIELD = "Firstname";
        public const string LAST_NAME_FIELD = "Lastname";
        public const string PASSWORD_FIELD = "Password";
        public const string NO_VEHICLES_HEADER = "--NO VEHICLES--";
        public const string USER_HEADER = "--USER {0}--";
    }
}
