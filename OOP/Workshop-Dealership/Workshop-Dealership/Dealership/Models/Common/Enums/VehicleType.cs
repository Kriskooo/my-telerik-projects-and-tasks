﻿namespace Dealership.Models.Common.Enums
{
    public enum VehicleType
    {
        Car,
        Motorcycle,
        Truck
    }
}
