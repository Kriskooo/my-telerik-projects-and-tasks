﻿namespace Dealership.Models.Common.Enums
{
    public enum Role
    {
        Admin,
        Normal,
        VIP
    }
}
