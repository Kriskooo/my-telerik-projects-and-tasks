﻿using Dealership.Models.Common;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using System;

namespace Dealership.Models
{
    public class Truck : Vehicle, ITruck
    {
        private const int wheelCount = 8;
        private int weightCapacity;

        public Truck(string make, string model, double price, int weightCapacity)
            : base(VehicleType.Truck, make, model, price, wheelCount)
        {
            this.WeightCapacity = weightCapacity;
        }

        public int WeightCapacity
        {
            get => this.weightCapacity;
            private set
            {
                if (value < Constants.WEIGHT_CAP_MIN || value > Constants.WEIGHT_CAP_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.WEIGHT_CAP_ERR, Constants.WEIGHT_CAP_MIN, Constants.WEIGHT_CAP_MAX));
                }
                this.weightCapacity = value;
            }
        }
        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Weight Capacity: {this.WeightCapacity}";
        }
    }
}

