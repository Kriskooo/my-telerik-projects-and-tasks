﻿using Dealership.Models.Common;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dealership.Models
{
    public abstract class Vehicle : IVehicle
    {
        private string make;
        private string model;
        private double price;
        private int wheels;
        private IList<IComment> comments;

        protected Vehicle(VehicleType type, string make, string model, double price, int wheels)
        {
            this.Type = type;
            this.Make = make;
            this.Model = model;
            this.Price = price;
            this.Wheels = wheels;
            this.comments = new List<IComment>();
        }

        public VehicleType Type { get; }


        public string Make
        {
            get => this.make;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.MAKE_NULL_ERR);

                if (value.Length < Constants.MAKE_NAME_LEN_MIN || value.Length > Constants.MAKE_NAME_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.MAKE_NAME_LEN_ERR, Constants.MAKE_NAME_LEN_MIN, Constants.MAKE_NAME_LEN_MAX));
                }
                this.make = value;
            }
        }

        public string Model
        {
            get => this.model;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.MODEL_NULL_ERR);

                if (value.Length < Constants.MODEL_NAME_LEN_MIN || value.Length > Constants.MODEL_NAME_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.MODEL_NAME_LEN_ERR, Constants.MODEL_NAME_LEN_MIN, Constants.MODEL_NAME_LEN_MAX));
                }
                this.model = value;
            }
        }

        public double Price
        {
            get => this.price;
            private set
            {
                Validator.ValidateDecimalRange(value, Constants.PRICE_VAL_MIN, Constants.PRICE_VAL_MAX, string.Format(Constants.PRICE_VAL_ERR, Constants.PRICE_VAL_MIN, Constants.PRICE_VAL_MAX));

                this.price = value;
            }
        }
        public int Wheels
        {
            get => this.wheels;
            private set
            {
                this.wheels = value;
            }
        }
        public IList<IComment> Comments => this.comments;

        public void AddComment(IComment comment)
        {
            this.comments.Add(comment);
        }

        public void RemoveComment(IComment comment)
        {
            this.comments.Remove(comment);
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Make: {this.Make}");
            sb.AppendLine($"Model: {this.Model}");
            sb.AppendLine($"Wheels: {this.Wheels}");
            sb.AppendLine($"Price: ${Utils.RemoveTrailingZerosFromDouble(this.Price)}");

            return sb.ToString().TrimEnd();
        }
    }
}
