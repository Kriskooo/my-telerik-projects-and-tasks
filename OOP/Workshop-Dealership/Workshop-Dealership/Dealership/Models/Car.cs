﻿using Dealership.Models.Common;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using System;

namespace Dealership.Models
{
    public class Car : Vehicle, ICar
    {
        private const int wheelCount = 4;
        private int seats;

        public Car(string make, string model, double price, int seats)
         : base(VehicleType.Car, make, model, price, wheelCount)
        {
            this.Seats = seats;
        }

        public int Seats
        {
            get => this.seats;
            private set
            {
                if (value < Constants.CAR_SEATS_MIN || value > Constants.CAR_SEATS_MAX)
                {
                    throw new ArgumentException("Seats must be between 1 and 10!");
                }
                this.seats = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Seats count: {this.Seats}";
        }
    }
}
