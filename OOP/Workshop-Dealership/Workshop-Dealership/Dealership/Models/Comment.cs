﻿using System;
using System.Text;
using Dealership.Models.Common;
using Dealership.Models.Contracts;

namespace Dealership.Models
{
    public class Comment : IComment
    {
        private string content;
        private string author;

        public Comment(string content, string author)
        {
            this.Content = content;
            this.Author = author;
        }

        public string Content
        {
            get => this.content;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.CONTENT_NULL_ERR);

                if (value.Length < Constants.CONTENT_LEN_MIN || value.Length > Constants.CONTENT_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.CONTENT_LEN_ERR, Constants.CONTENT_LEN_MIN, Constants.CONTENT_LEN_MAX));
                }
                this.content = value;
            }
        }

        public string Author
        {
            get => this.author;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.AUTHOR_NULL_ERR);

                this.author = value;
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Constants.COMMENT_HEADER);
            sb.AppendLine(this.Content);
            sb.AppendLine($"  User: {this.Author}");
            sb.AppendLine(Constants.COMMENT_HEADER);

            return sb.ToString().Trim();
        }
    }
}
