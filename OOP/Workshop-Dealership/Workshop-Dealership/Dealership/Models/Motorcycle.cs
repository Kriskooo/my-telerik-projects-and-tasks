﻿using Dealership.Models.Common;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using System;

namespace Dealership.Models
{
    public class Motorcycle : Vehicle, IMotorcycle
    {
        private const int wheelCount = 2;
        private string category;

        public Motorcycle(string make, string model, double price, string category)
         : base(VehicleType.Motorcycle, make, model, price, wheelCount)
        {
            this.Category = category;
        }

        public string Category
        {
            get => this.category;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.CATEGORY_NULL_ERR);
                if (value.Length < Constants.CATEGORY_LEN_MIN || value.Length > Constants.CATEGORY_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.CATEGORY_LEN_ERR, Constants.CATEGORY_LEN_MIN, Constants.CATEGORY_LEN_MAX));
                }
                this.category = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Category length: {this.Category}";
        }
    }
}
