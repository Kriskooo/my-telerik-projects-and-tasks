﻿using Dealership.Models.Common;
using Dealership.Models.Common.Enums;
using Dealership.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Dealership.Models
{
    public class User : IUser
    {
        private string username;
        private string firstName;
        private string lastName;
        private string password;
        private readonly IList<IVehicle> vehicles;

        public User(string username, string firstName, string lastName, string password)
        {
            this.Username = username;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Password = password;
            this.Role = Role.Normal;
            this.vehicles = new List<IVehicle>();
        }
        public User(string username, string firstName, string lastName, string password, Role role)
           : this(username, firstName, lastName, password)
        {
            this.Role = role;
        }

        public string Username
        {
            get => this.username;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.USERNAME_NULL_ERR);
                Validator.ValidatePattern(value, Constants.USERNAME_REGEX_PATTERN, Constants.USERNAME_PATTERN_ERR);
                if (value.Length < Constants.USERNAME_LEN_MIN || value.Length > Constants.USERNAME_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.USERNAME_LEN_ERR, Constants.USERNAME_LEN_MIN, Constants.USERNAME_LEN_MAX));
                }
                this.username = value;
            }
        }
        public string FirstName
        {
            get => this.firstName;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.FIRSTNAME_NULL_ERR);
                if (value.Length < Constants.FIRSTNAME_LEN_MIN || value.Length > Constants.FIRSTNAME_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.FIRSTNAME_LEN_ERR, Constants.FIRSTNAME_LEN_MIN, Constants.FIRSTNAME_LEN_MAX));
                }
                this.firstName = value;
            }
        }
        public string LastName
        {
            get => this.lastName;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.LASTNAME_NULL_ERR);
                if (value.Length < Constants.LASTNAME_LEN_MIN || value.Length > Constants.LASTNAME_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.LASTNAME_LEN_ERR, Constants.LASTNAME_LEN_MIN, Constants.LASTNAME_LEN_MAX));
                }
                this.lastName = value;
            }
        }
        public string Password
        {
            get => this.password;
            private set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.PASSWORD_NULL_ERR);
                Validator.ValidatePattern(value, Constants.PASSWORD_REGEX_PATTERN, Constants.PASSWORD_PATTERN_ERR);
                if (value.Length < Constants.PASSWORD_LEN_MIN || value.Length > Constants.PASSWORD_LEN_MAX)
                {
                    throw new ArgumentException(string.Format(Constants.PASSWORD_LEN_ERR, Constants.PASSWORD_LEN_MIN, Constants.PASSWORD_LEN_MAX));
                }
                this.password = value;
            }
        }
        public Role Role { get; }

        public IList<IVehicle> Vehicles => this.vehicles;

        public void AddComment(IComment commentToAdd, IVehicle vehicleToAddComment)
        {
            vehicleToAddComment.AddComment(commentToAdd);
        }

        public void AddVehicle(IVehicle vehicle)
        {
            if (this.Role == Role.Admin)
            {
                throw new ArgumentException(Constants.ADMIN_CANNOT_ADD_VEHICLES);
            }

            if (this.Role == Role.Normal && this.vehicles.Count == 5)
            {
                throw new ArgumentException(string.Format(Constants.NOT_AN_VIP_USER_VEHICLES_ADD, Constants.VIP_MAX_VEHICLES_TO_ADD));
            }

            this.vehicles.Add(vehicle);

        }

        public string PrintVehicles()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"--USER {this.Username}--");

            for (int i = 0; i < this.vehicles.Count; i++)
            {
                int counter = i + 1;
                IVehicle vechicle = this.vehicles[i];
                sb.AppendLine($"{counter}. {vechicle.Type}:");
                sb.AppendLine($"  {vechicle.ToString()}");
            }

            return sb.ToString().Trim();
        }

        public void RemoveComment(IComment commentToRemove, IVehicle vehicleToRemoveComment)
        {
            if (commentToRemove.Author == this.Username)
            {
                vehicleToRemoveComment.RemoveComment(commentToRemove);
            }

        }

        public void RemoveVehicle(IVehicle vehicle)
        {
            this.vehicles.Remove(vehicle);
        }
        public override string ToString()
        {
            return string.Format(Constants.USER_TO_STRING, this.Username, this.FirstName, this.LastName, this.Role.ToString());
        }
    }
}
