﻿namespace Dealership.Models.Contracts
{
    public interface IPriceable
    {
        double Price { get; }
    }
}
