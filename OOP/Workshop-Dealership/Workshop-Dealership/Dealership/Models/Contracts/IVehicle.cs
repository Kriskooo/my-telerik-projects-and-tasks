﻿using Dealership.Models.Common.Enums;

namespace Dealership.Models.Contracts
{
    public interface IVehicle : IPriceable, ICommentable
    {
        int Wheels { get; }
        VehicleType Type { get; }
        string Make { get; }
        string Model { get; }
        void AddComment(IComment comment);
        void RemoveComment(IComment comment);
    }
}
