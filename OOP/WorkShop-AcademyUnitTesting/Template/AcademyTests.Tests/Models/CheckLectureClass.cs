﻿using Academy.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AcademyTests.Tests.Models
{
    [TestClass]
    public class CheckLectureClass
    {
        [TestMethod]
        public void LectureNameIsCorrect()
        {
            var name = "name test";
            var date = Convert.ToDateTime("11-11-2031");

            var trainer = new Trainer("Kris", new List<string>());
            var lecture = new Lecture(name, date, trainer);

            Assert.AreEqual(name, lecture.Name);
            Assert.AreEqual(date, lecture.Date);
            Assert.AreEqual(trainer, lecture.Trainer);

            Assert.AreEqual(0, lecture.Resources.Count);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow(" ")]
        [DataRow("")]
        [DataRow("  ")]
        [DataRow("12")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaak")]
        public void Lecture_Should_ThrowArgumenException_LectureValueIsInvalid(string name)
        {
            var trainer = new Trainer("Deadpool", new List<string> { "kris" });
            
            var date = Convert.ToDateTime("11-11-2030");


            Assert.ThrowsException<ArgumentException>(() => new Lecture(name, date, trainer));
        }
    }
}