using Academy.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AcademyTests.Tests
{
    [TestClass]
    public class CheckCourse
    {
        [TestMethod]
        public void CourseNameIsCorrect()
        {
            //Arrange
            var name = "This is a test name";
            var lecturesPerWeek = 3;
            var StartDate = Convert.ToDateTime("11-12-2030");
            var EndingDate = Convert.ToDateTime("10-11-2030");
            
            //Act
            var sut = new Course(name, lecturesPerWeek, StartDate, EndingDate);

            //Assert
            Assert.AreEqual(name, sut.Name);
            Assert.AreEqual(lecturesPerWeek, sut.LecturesPerWeek);
            Assert.AreEqual(StartDate, sut.StartingDate);
            Assert.AreEqual(EndingDate, sut.EndingDate);
            Assert.AreEqual(0, sut.OnlineStudents.Count);
            Assert.AreEqual(0, sut.OnsiteStudents.Count);
            Assert.AreEqual(0, sut.Lectures.Count);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("  ")]
        [DataRow("aa")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void CourseName_Should_ThrowArgumenException_NameValueIsInvalid(string name)
        {           
            var lecturesPerWeek = 3;
            var startDate = Convert.ToDateTime("11-12-2030");
            var endingDate = Convert.ToDateTime("10-11-2030");

            Assert.ThrowsException<ArgumentException>(() => new Course(name, lecturesPerWeek, startDate, endingDate));
           
        }
    }
}