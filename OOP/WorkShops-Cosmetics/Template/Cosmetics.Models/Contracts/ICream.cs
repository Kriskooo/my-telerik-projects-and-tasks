﻿using Cosmetics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmetics.Models.Contracts
{
    public interface ICream : IProduct
    {
        public Scent Scent { get; }
    }
}
