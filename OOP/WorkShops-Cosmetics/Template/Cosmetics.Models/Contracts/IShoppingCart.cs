﻿namespace Cosmetics.Contracts
{
    public interface IShoppingCart
    {
        void AddProcut(IProduct product);
        void RemoveProduct(IProduct product);
        bool ContainsProduct(IProduct product);
        decimal TotalPrice();

    }
}