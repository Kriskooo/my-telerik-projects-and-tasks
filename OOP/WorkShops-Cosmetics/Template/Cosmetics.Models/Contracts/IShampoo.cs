﻿using Cosmetics.Common;
using Cosmetics.Products;

namespace Cosmetics.Contracts
{
    public interface IShampoo : IProduct
    {
        public int Milliliters { get;}

        public UsageType Usage { get;}
    }
}