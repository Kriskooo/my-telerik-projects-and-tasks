﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using Cosmetics.Models.Products;
using System;
using System.Text;

namespace Cosmetics.Products
{
    public class Shampoo : Product, IShampoo
    {
        private int milliliters;
        private UsageType usage;

        public Shampoo(string name, string brand, decimal price, GenderType gender, int milliliters, UsageType usage)
            : base(name, brand, price, gender)
        {
            this.Milliliters = milliliters;
            this.Usage = usage;

        }

        public int Milliliters
        {
            get => this.milliliters;

            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.milliliters = value;
            }

        }

        public UsageType Usage
        {
            get => this.usage;

            private set
            {
                if (!Enum.IsDefined(typeof(UsageType), value))
                {

                    throw new ArgumentException();
                }

                this.usage = value;
            }
        }

        public override string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.Print());
            sb.AppendLine($" #Milliliters: {this.Milliliters}");
            sb.AppendLine($" #Usage: {this.Usage.ToString()}");
            sb.Append($" ===");

            return sb.ToString().TrimEnd();

        }
    }
}
