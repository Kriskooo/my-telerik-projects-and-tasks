﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmetics.Models.Products
{
    public abstract class Product : IProduct
    {
        protected string name;
        protected string brand;
        protected decimal price;
        protected GenderType gender;


        public Product(string name, string brand, decimal price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.Gender = gender;
        }

        public virtual string Name
        {
            get => this.name;

            protected set
            {
                if (value.Length < 3 || value.Length > 10)
                {
                    throw new ArgumentNullException();
                }

                this.name = value;
            }
        }

        public virtual string Brand
        {
            get => this.brand;

            protected set
            {

                this.brand = value;
            }
        }

        public virtual decimal Price
        {
            get => this.price;

            protected set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.price = value;
            }
        }

        public GenderType Gender
        {
            get => this.gender;

            private set
            {

                if (!Enum.IsDefined(typeof(GenderType), value))
                {
                    throw new ArgumentException("Invalid gender.");
                }

                this.gender = value;
            }
        }

        public virtual string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($" #Price: ${this.Price:f2}");
            sb.AppendLine($" #Gender: {this.Gender}");        
            return sb.ToString().TrimEnd();
        }
    }
}
