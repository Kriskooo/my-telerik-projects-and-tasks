﻿using Cosmetics.Common;
using Cosmetics.Models.Contracts;
using Cosmetics.Models.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmetics
{
    public class Cream : Product, ICream
    {
        private Scent scent;

        public Cream(string name, string brand, decimal price, GenderType gender, Scent scent)
            : base(name, brand, price, gender)
        {
            this.Scent = scent;
        }


        public Scent Scent

        {
            get => this.scent;

            private set
            {
                if (value < Scent.Lavender || value > Scent.Rose)
                {
                    throw new ArgumentException("Invalid scent.");
                }

                this.scent = value;
            }
        }

        public override string Name
        {
            get => this.name;

            protected set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new ArgumentNullException();
                }

                this.name = value;
            }
        }

        public override string Brand
        {
            get => this.brand;

            protected set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new NotImplementedException();
                }

                this.brand = value;
            }
        }

        public override decimal Price
        {
            get => this.price;

            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.price = value;
            }
        }

        public override string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.Print());           
            sb.AppendLine($" # Scent:{this.Scent}");
            sb.AppendLine($"===");
            return sb.ToString().TrimEnd();
        }
    }
}

