﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using Cosmetics.Models.Products;
using System;
using System.Text;

namespace Cosmetics.Products
{
    public class Toothpaste : Product, IToothpaste
    {
        private string ingredients;

        public Toothpaste(string name, string brand, decimal price, GenderType gender, string ingredients)
            : base(name, brand, price, gender)
        {
            this.Ingredients = ingredients;
        }

        public string Ingredients
        {
            get => this.ingredients;
            private set
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }

                this.ingredients = value;
            }
        }
        public override string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(base.Print());
            sb.AppendLine($" #Ingredients: {this.Ingredients}");
            sb.Append($" ===");

            return sb.ToString().TrimEnd();

        }
    }
}
