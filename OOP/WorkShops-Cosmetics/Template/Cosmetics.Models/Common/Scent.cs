﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmetics
{
    public enum Scent
    {
        Lavender,
        Vanilla,
        Rose
    }
}
