﻿using Cosmetics.Core;
using Cosmetics.Common;
using System;
using System.Collections.Generic;

namespace Cosmetics
{
    public class Program
    {
        public static void Main()
        {
            var engine = new Engine();


            Console.WriteLine(engine.CreateShampoo("MyMan", "Nivea", 10.99m, GenderType.Men, 1000, UsageType.EveryDay));
            Console.WriteLine(engine.CreateToothpaste("White", "Colgate", 10.99m, GenderType.Men, new List<string>() { "calcium", "fluorid" }));
            Console.WriteLine(engine.CreateCream("Vichy", "Rose", 18.50m, GenderType.Men, Scent.Rose));

            Console.WriteLine(engine.CreateCategory("Shampoos"));
            Console.WriteLine(engine.CreateCategory("Toothpastes"));
            Console.WriteLine(engine.CreateCategory("Creams"));

            Console.WriteLine(engine.AddToCategory("Shampoos", "MyMan"));
            Console.WriteLine(engine.AddToCategory("Toothpastes", "White"));
            Console.WriteLine(engine.AddToCategory("Creams", "Vichy"));

            Console.WriteLine(engine.AddToShoppingCart("MyMan"));
            Console.WriteLine(engine.AddToShoppingCart("White"));
            Console.WriteLine(engine.AddToShoppingCart("Vichy"));

            Console.WriteLine(engine.ShowCategory("Shampoos"));
            Console.WriteLine(engine.ShowCategory("Toothpastes"));
            Console.WriteLine(engine.ShowCategory("Creams"));

            Console.WriteLine(engine.CalculateShoppingCartTotalPrice());

            Console.WriteLine(engine.RemoveFromCategory("Shampoos", "MyMan"));
            Console.WriteLine(engine.ShowCategory("Shampoos"));
            Console.WriteLine(engine.RemoveFromShoppingCart("MyMan"));
            Console.WriteLine(engine.CalculateShoppingCartTotalPrice());
        }
    }
}
