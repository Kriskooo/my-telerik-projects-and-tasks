﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cosmetics.Cart;
using Cosmetics.Common;
using Cosmetics.Contracts;
using Cosmetics.Products;

namespace Cosmetics.Core
{
    public class Services
    {
        private const string CategoryDoesNotExist = "Category {0} does not exist!";
        private const string CategoryExists = "Category with name {0} already exists!";
        private const string InvalidCommand = "Invalid command name: {0}!";
        private const string CategoryCreated = "Category with name {0} was created!";
        private const string ProductDoesNotExist = "Product {0} does not exist!";
        private const string ProductAddedToCategory = "Product {0} added to category {1}!";
        private const string ProductRemovedCategory = "Product {0} removed from category {1}!";
        private const string ShampooAlreadyExist = "Shampoo with name {0} already exists!";
        private const string ShampooCreated = "Shampoo with name {0} was created!";
        private const string CreamCreated = "Cream with name {0} was created!";
        private const string CreamAlredyExit = "Cream with name {0} alaredy exits !";
        private const string ToothpasteAlreadyExist = "Toothpaste with name {0} already exists!";
        private const string ToothpasteCreated = "Toothpaste with name {0} was created!";
        private const string ProductAddedToShoppingCart = "Product {0} was added to the shopping cart!";
        private const string ProductDoesNotExistInShoppingCart = "Shopping cart does not contain product with name {0}!";
        private const string ProductRemovedFromShoppingCart = "Product {0} was removed from the shopping cart!";
        private const string TotalPriceInShoppingCart = "${0} total price currently in the shopping cart!";
        private const string InvalidGenderType = "Invalid gender type!";
        private const string InvalidUsageType = "Invalid usage type!";
        private const string NoProductsInShoppingCart = "No products in shopping cart!";

        private readonly ShoppingCart shoppingCart;
        private readonly List<ICategory> categories;
        private readonly List<IProduct> products;


        public IFormatProvider CreamAlreadyExist { get; private set; }

        public Services()
        {
            this.categories = new List<ICategory>();
            this.products = new List<IProduct>();
            this.shoppingCart = new ShoppingCart();
        }
        public string CreateCategory(string categoryName)
        {
            var category = this.FindCategory(categoryName);
            if (category != null)
            {
                return string.Format(CategoryExists, categoryName);
            }

            category = new Category(categoryName);
            this.categories.Add(category);

            return string.Format(CategoryCreated, categoryName);
        }

        public string CreateShampoo(string name, string brand, decimal price, GenderType gender, int milliliters, UsageType usage)
        {
            var shampoo = this.FindProduct(name);
            if (shampoo != null)
            {
                return string.Format(ShampooAlreadyExist, name);
            }


            // TODO: If you have arranged your classes and interfaces correctly, then the following cast can be removed!
            shampoo = (IProduct)(new Shampoo(name, brand, price, gender, milliliters, usage));
            this.products.Add(shampoo);

            return string.Format(ShampooCreated, name);
        }

        public string CreateToothpaste(string name, string brand, decimal price, GenderType gender, IList<string> ingredients)
        {
            var toothpaste = this.FindProduct(name);
            if (toothpaste != null)
            {
                return string.Format(ToothpasteAlreadyExist, name);
            }

            // TODO: If you have arranged your classes and interfaces correctly, then the following cast can be removed!
            toothpaste = (IProduct)(new Toothpaste(name, brand, price, gender, string.Join(", ", ingredients)));
            this.products.Add(toothpaste);

            return string.Format(ToothpasteCreated, name);
        }

        public string CreateCream(string name, string brand, decimal price, GenderType gender, Scent scent)
        {
            var cream = this.FindProduct(name);
            if (cream != null)
            {
                return string.Format(CreamAlreadyExist, name);
            }


            // TODO: If you have arranged your classes and interfaces correctly, then the following cast can be removed!
            cream = new Cream(name, brand, price, gender, scent);
            this.products.Add(cream);

            return string.Format(CreamCreated, name);
        }


        public string AddProductToShoppingCart(string productName)
        {
            var product = this.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            this.shoppingCart.AddProduct(product);

            return string.Format(ProductAddedToShoppingCart, productName);
        }

        public string RemoveProductFromShoppingCart(string productName)
        {
            var product = this.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            if (!this.shoppingCart.ContainsProduct(product))
            {
                return string.Format(ProductDoesNotExistInShoppingCart, productName);
            }

            this.shoppingCart.RemoveProduct(product);

            return string.Format(ProductRemovedFromShoppingCart, productName);
        }



        public ICategory FindCategory(string categoryName)
        {
            foreach (var category in this.categories)
            {
                if (category.Name == categoryName)
                {
                    return category;
                }
            }

            return null;
        }

        public string ShowCategory(string categoryName)
        {
            var category = this.FindCategory(categoryName);
            if (category != null)
            {
                return category.Print();
            }
            else
            {
                return string.Format(CategoryDoesNotExist, categoryName);
            }
        }

        public string AddProductToCategory(string categoryName, string productName)
        {
            var category = this.FindCategory(categoryName);
            if (category == null)
            {
                return string.Format(CategoryDoesNotExist, categoryName);
            }

            var product = this.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            category.AddProduct(product);

            return string.Format(ProductAddedToCategory, productName, categoryName);
        }

        public string RemoveProductFromCategory(string categoryName, string productName)
        {
            var category = this.FindCategory(categoryName);
            if (category == null)
            {
                return string.Format(CategoryDoesNotExist, categoryName);
            }

            var product = this.FindProduct(productName);
            if (product == null)
            {
                return string.Format(ProductDoesNotExist, productName);
            }

            category.RemoveProduct(product);

            return string.Format(ProductRemovedCategory, productName, categoryName);
        }


        public IProduct FindProduct(string productName)
        {
            foreach (var product in this.products)
            {
                if (product.Name == productName)
                {
                    return product;
                }
            }

            return null;
        }

        public string CalculateShoppingCartTotalPrice()
        {
            if (!this.shoppingCart.ProductList.Any())
            {
                return NoProductsInShoppingCart;
            }

            decimal totalPrice = this.shoppingCart.TotalPrice();

            return string.Format(TotalPriceInShoppingCart, totalPrice);
        }

    }
}

