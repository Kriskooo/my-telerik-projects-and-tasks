﻿using Cosmetics.Common;
using System.Collections.Generic;

namespace Cosmetics.Core
{
    public class Engine
    {
        //private const string InvalidGenderType = "Invalid gender type!";
        //private const string InvalidUsageType = "Invalid usage type!";

        private readonly Services appServices;

        public Engine()
        {
            this.appServices = new Services();
        }

        public string CreateCategory(string categoryName)
        {
            return this.appServices.CreateCategory(categoryName);
        }

        public string AddToCategory(string categoryName, string productName)
        {
            return this.appServices.AddProductToCategory(categoryName, productName);
        }

        public string RemoveFromCategory(string categoryName, string productToRemove)
        {
            return this.appServices.RemoveProductFromCategory(categoryName, productToRemove);
        }

        public string ShowCategory(string categoryToShow)
        {
            return this.appServices.ShowCategory(categoryToShow);
        }

        public string CreateShampoo(string name, string brand, decimal price, GenderType gender, int milliliters, UsageType usage)
        {
            return this.appServices.CreateShampoo(name, brand, price, gender, milliliters, usage);
        }

        public string CreateToothpaste(string name, string brand, decimal price, GenderType gender, IList<string> ingredients)
        {
            return this.appServices.CreateToothpaste(name, brand, price, gender, ingredients);
        }

        public string CreateCream(string name, string brand, decimal price, GenderType gender, Scent scent)
        {
            return this.appServices.CreateCream(name, brand, price, gender, scent);
        }
        public string AddToShoppingCart(string productName)
        {
            return this.appServices.AddProductToShoppingCart(productName);
        }

        public string RemoveFromShoppingCart(string productName)
        {
            return this.appServices.RemoveProductFromShoppingCart(productName);
        }

        public string CalculateShoppingCartTotalPrice()
        {
            return this.appServices.CalculateShoppingCartTotalPrice();
        }
    }
}

