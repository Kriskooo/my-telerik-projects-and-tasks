<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## OOP Workshop - Cosmetics Shop

### Overview

You are given a software system for managing a cosmetics shop. The system already has a working Engine. You do not have to touch anything in it. Just use it.

### Task
Your **task** is to **design an object-oriented class hierarchy** to model the cosmetics shop, **using the best practices for object-oriented design** (OOD) and **object-oriented programming** (OOP). Encapsulate correctly all fields and use validation whenever needed.

There should be two types of products for now - shampoos and toothpastes. 

#### Shampoo

- The product has a **name**, a **brand**, a **price**, a **gender** using it, an amount in **milliliters** and a type of **usage**.
- The product's name should be between 3 and 10 symbols in length whereas the length of the brand should be kept between 2 and 10 symbols.
- The price cannot be negative.
- Genders using the product can be **"men"**, **"women"** or **"unisex"**.
- Milliliters cannot be a negative number.
- The product can be used for medical purposes but is also suitable for everyday usage.
- Implements IShampoo
- Try protecting your properties and methods against `null` values.

#### Toothpaste

- The product has a **name**, a **brand**, a **price**, a **gender** using it and a list of **ingredients**.
- The name of the product should be between 3 and 10 symbols.
- The brand of the product should be between 2 and 10 symbols.
- The price should not go below zero.
- Genders using the product can be **"men"**, **"women"** or **"unisex"**.
- Ingredients are comma separated values.
- Implements IToothpaste
- Try protecting your properties and methods against `null` values.

### Output Example

If everything is done correctly, after starting the application you should see the following output:

```
Shampoo with name MyMan was created!
Toothpaste with name White was created!
Category with name Shampoos was created!
Category with name Toothpastes was created!
Product MyMan added to category Shampoos!
Product White added to category Toothpastes!
Product MyMan was added to the shopping cart!
Product White was added to the shopping cart!
#Category: Shampoos
#MyMan Nivea
 #Price: $10.99
 #Gender: Men
 #Milliliters: 1000
 #Usage: EveryDay
 ===
#Category: Toothpastes
#White Colgate
 #Price: $10.99
 #Gender: Men
 #Ingredients: calcium, fluorid
 ===
$21.98 total price currently in the shopping cart!
Product MyMan removed from category Shampoos!
#Category: Shampoos
 #No product in this category
Product MyMan was removed from the shopping cart!
$10.99 total price currently in the shopping cart!
```
> **Note** - Don't worry if your program prints with a ','(comma) as a decimal separator instead a '.'(dot). That's a Windows setting and not an issue with your implementation.

### OPTIONAL ADVANCED TASK

#### Cream

- Implement new product and its creation in the engine class. 
- Cream (name, brand, price, gender, scent)
    - name minimum 3 symbols and maximum 15
    - brand minimum 3 symbols and maximum 15
    - price greater than zero
    - gender (men, women, unisex)
    - scent (lavender, vanilla, rose)
- Implement product creation in the Factory and the Engine
    - Just look at the other products
- Test it if it works correctly

