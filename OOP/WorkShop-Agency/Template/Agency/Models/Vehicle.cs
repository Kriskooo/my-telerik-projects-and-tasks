﻿using Agency.Exceptions;
using Agency.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models
{
    public abstract class Vehicle : IVehicle
    {
        protected int passangerCapacity;
        protected double pricePerKilometer;

        public Vehicle(int id, int passengerCapacity, double pricePerKilometer)
        {
            this.Id = id;
            this.PassangerCapacity = passengerCapacity;
            this.PricePerKilometer = pricePerKilometer;
        }

        public int Id { get; }

        public virtual int PassangerCapacity
        {
            get => this.passangerCapacity;
            set
            {
                if (value < 1 || value > 800)
                {
                    throw new EntryPointNotFoundException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
                }

                this.passangerCapacity = value;
            }
        }
        public virtual double PricePerKilometer
        {
            get => this.pricePerKilometer;
            set
            {
                if (value < 0.10 || value > 2.50)
                {
                    throw new InvalidUserInputException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
                }

                this.pricePerKilometer = value;
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{this.GetType().Name} ----");
            sb.AppendLine($"Passenger capacity: {this.PassangerCapacity}");
            sb.AppendLine($"Price per kilometer: {this.PricePerKilometer:f2}");

            return sb.ToString().TrimEnd();
        }

    }
}
