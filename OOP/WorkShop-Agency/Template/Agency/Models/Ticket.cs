﻿using Agency.Exceptions;
using Agency.Models.Contracts;

using System;
using System.Text;

namespace Agency.Models
{
    public class Ticket : ITicket
    {
        private double adminastrativecosts;

        public Ticket(int id, IJourney journey, double administrativeCosts)
        {
            this.Id = id;
            this.Journey = journey;
            this.AdministrativeCosts = administrativeCosts;

        }

        public int Id { get; set; }
        public IJourney Journey { get; set; }

        public double AdministrativeCosts
        {
            get => this.adminastrativecosts;

            private set
            {
                if (value < 0)
                {
                    throw new InvalidUserInputException($"Value of 'costs' must be a positive number. Actual value: {value:F2}.");
                }

                this.adminastrativecosts = value;
            }
        }
        public double CalculatePrice()
        {
            return this.AdministrativeCosts * this.Journey.CalculatePrice();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Ticket ----");
            sb.AppendLine($"Destination: {this.Journey.Destination}");
            sb.AppendLine($"Price: {this.CalculatePrice():f2}");

            return sb.ToString().TrimEnd();
        }
    }
}
