﻿using Agency.Exceptions;
using Agency.Models.Contracts;
using System;

namespace Agency.Models
{
    public class Bus : Vehicle, IBus
    {
        public const int PassengerCapacityMinValue = 10;
        public const int PassengerCapacityMaxValue = 50;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;


        public Bus(int id, int passengerCapacity, double pricePerKilometer, bool hasFreeTv)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.HasFreeTv = hasFreeTv;
        }

        public bool HasFreeTv { get; }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Has free TV: {this.HasFreeTv}";
        }

        public override int PassangerCapacity
        {
            get => this.passangerCapacity;

            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    throw new InvalidUserInputException("A bus cannot have less than 10 passengers or more than 50 passengers.");
                }
                this.passangerCapacity = value;
            }
        }

        public override double PricePerKilometer
        {
            get => this.pricePerKilometer;

            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    throw new InvalidUserInputException("A Bus with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
                }
                this.pricePerKilometer = value;
            }
        }
    }
}


