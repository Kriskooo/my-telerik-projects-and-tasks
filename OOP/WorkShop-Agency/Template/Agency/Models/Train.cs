﻿using Agency.Exceptions;
using Agency.Models.Contracts;
using System;

namespace Agency.Models
{
    public class Train : Vehicle, ITrain
    {
        public const int PassengerCapacityMinValue = 30;
        public const int PassengerCapacityMaxValue = 150;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;
        public const int CartsMinValue = 1;
        public const int CartsMaxValue = 15;

        private int carts;
        public Train(int id, int passengerCapacity, double pricePerKilometer, int carts)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.Carts = carts;
        }
        public int Carts
        {
            get => this.carts;

            private set
            {
                if (value < CartsMinValue || value > CartsMaxValue)
                {
                    throw new InvalidUserInputException("A train cannot have less than 1 cart or more than 15 carts.");
                }
                this.carts = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Carts amount: {this.Carts}";
        }


        public override int PassangerCapacity
        {
            get => this.passangerCapacity;

            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    throw new InvalidUserInputException("A train cannot have less than 30 passengers or more than 150 passengers.");
                }

                this.passangerCapacity = value;
            }
        }

        public override double PricePerKilometer
        {
            get => this.pricePerKilometer;
            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    throw new InvalidUserInputException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
                }
                this.pricePerKilometer = value;
            }
        }
    }
}