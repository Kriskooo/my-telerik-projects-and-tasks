﻿using Agency.Exceptions;
using Agency.Models.Contracts;
using System;

namespace Agency.Models
{
    public class Airplane : Vehicle, IAirplane
    {
        public const int PassengerCapacityMinValue = 1;
        public const int PassengerCapacityMaxValue = 800;
        public const double PricePerKilometerMinValue = 0.10;
        public const double PricePerKilometerMaxValue = 2.50;

        public Airplane(int id, int passengerCapacity, double pricePerKilometer, bool isLowCost)
            : base(id, passengerCapacity, pricePerKilometer)
        {
            this.IsLowCost = isLowCost;
        }
        public bool IsLowCost { get; }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + $"Is low-cost: {this.IsLowCost}";
        }

        public override int PassangerCapacity
        {
            get => this.passangerCapacity;

            set
            {
                if (value < PassengerCapacityMinValue || value > PassengerCapacityMaxValue)
                {
                    throw new InvalidUserInputException("A bus cannot have less than 1 passengers or more than 800 passengers.");
                }
                this.passangerCapacity = value;
            }
        }
        public override double PricePerKilometer
        {
            get => this.pricePerKilometer;

            set
            {
                if (value < PricePerKilometerMinValue || value > PricePerKilometerMaxValue)
                {
                    throw new InvalidUserInputException("A Bus with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
                }
                this.pricePerKilometer = value;
            }
        }
    }
}