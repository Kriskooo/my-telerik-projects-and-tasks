﻿using Agency.Exceptions;
using Agency.Models.Contracts;

using System;
using System.Text;

namespace Agency.Models
{
    public class Journey : IJourney
    {
        public const int StartLocationMinLength = 5;
        public const int StartLocationMaxLength = 25;
        public const int DestinationMinLength = 5;
        public const int DestinationMaxLength = 25;
        public const int DistanceMinValue = 5;
        public const int DistanceMaxValue = 5000;

        private string startLocation;
        private string destination;
        private int distance;

        public Journey(int id, string from, string to, int distance, IVehicle vehicle)
        {
            this.Id = id;
            this.StartLocation = from;
            this.Destination = to;
            this.Distance = distance;
            this.Vehicle = vehicle;
        }

        public string StartLocation
        {
            get => this.startLocation;

            private set
            {
                if (value.Length < StartLocationMinLength || value.Length > StartLocationMaxLength)
                {
                    throw new InvalidUserInputException("The length of StartLocation must be between 5 and 25 symbols.");
                }

                this.startLocation = value;
            }

        }
        public string Destination
        {
            get => this.destination;

            private set
            {
                if (value.Length < DestinationMinLength || value.Length > DestinationMaxLength)
                {
                    throw new InvalidUserInputException("The length of Destination must be between 5 and 25 symbols.");
                }
                this.destination = value;
            }
        }
        public int Distance
        {
            get => this.distance;

            private set
            {
                if (value < DistanceMinValue || value > DistanceMaxValue)
                {
                    throw new InvalidUserInputException("The Distance cannot be less than 5 or more than 5000 kilometers.");
                }
                this.distance = value;
            }

        }
        public IVehicle Vehicle { get; }

        public int Id { get; }

        public double CalculatePrice()
        {
            return this.Distance * this.Vehicle.PricePerKilometer;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Journey ----");
            sb.AppendLine($"Start location: {this.StartLocation}");
            sb.AppendLine($"Destination: {this.Destination}");
            sb.AppendLine($"Distance: {this.Distance}");
            sb.AppendLine($"Travel costs: {this.CalculatePrice():f2}");

            return sb.ToString().TrimEnd();
        }
    }
}
