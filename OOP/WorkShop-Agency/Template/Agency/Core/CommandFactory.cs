﻿using Agency.Commands;
using Agency.Commands.Contracts;
using Agency.Core.Contracts;

using System;
using System.Collections.Generic;

namespace Agency.Core
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IRepository repository;

        public CommandFactory(IRepository repository)
        {
            this.repository = repository;
        }

        public ICommand Create(string commandLine)
        {
            // RemoveEmptyEntries makes sure no empty strings are added to the result of the split operation.
            string[] arguments = commandLine.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            string commandName = this.ExtractCommandName(arguments);
            List<string> commandParameters = this.ExtractCommandParameters(arguments);

            ICommand command;
            switch (commandName.ToLower())
            {
                case "createairplane":
                    {
                        command = new CreateAirplaneCommand(commandParameters, this.repository);
                        break;
                    }
                case "createbus":
                    {
                        command = new CreateBusCommand(commandParameters, this.repository);
                        break;
                    }
                case "createtrain":
                    {
                        command = new CreateTrainCommand(commandParameters, this.repository);
                        break;
                    }
                case "createticket":
                    {
                        command = new CreateTicketCommand(commandParameters, this.repository);
                        break;
                    }
                case "createjourney":
                    {
                        command = new CreateJourneyCommand(commandParameters, this.repository);
                        break;
                    }
                case "listjourneys":
                    {
                        command = new ListJourneysCommand(this.repository);
                        break;
                    }
                case "listtickets":
                    {
                        command = new ListTicketsCommand(this.repository);
                        break;
                    }
                case "listvehicles":
                    {
                        command = new ListVehiclesCommand(this.repository);
                        break;
                    }
                default:
                    {
                        throw new InvalidOperationException($"Command with name: {commandName} doesn't exist!");
                    }
            }
            return command;
        }

        // Receives a full line and extracts the command to be executed from it.
        // For example, if the input line is "FilterBy Assignee John", the method will return "FilterBy".
        private string ExtractCommandName(string[] arguments)
        {
            string commandName = arguments[0];
            return commandName;
        }

        // Receives a full line and extracts the parameters that are needed for the command to execute.
        // For example, if the input line is "FilterBy Assignee John",
        // the method will return a list of ["Assignee", "John"].
        private List<String> ExtractCommandParameters(string[] arguments)
        {
            List<string> commandParameters = new List<string>();

            for (int i = 1; i < arguments.Length; i++)
            {
                commandParameters.Add(arguments[i]);
            }

            return commandParameters;

        }
    }
}
