﻿using Agency.Core;
using Agency.Core.Contracts;

namespace Agency
{
    class Startup
    {
        static void Main()
        {
            IRepository repository = new Repository();
            ICommandFactory commandManager = new CommandFactory(repository);
            IEngine engine = new Engine(commandManager);
            engine.Start();
        }
    }
}
