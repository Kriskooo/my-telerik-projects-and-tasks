﻿using Agency.Commands.Abstracts;
using Agency.Core.Contracts;
using Agency.Exceptions;
using Agency.Models.Contracts;
using System.Collections.Generic;

namespace Agency.Commands
{
    public class CreateTicketCommand : BaseCommand
    {
        public CreateTicketCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {

        }
        public override string Execute()
        {

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException($"Invalid number of arguments. Expected: 2, Received: {this.CommandParameters.Count}");
            }

            double administrativeCosts = this.ParseDoubleParameter(base.CommandParameters[1], "administrativeCosts");
            int journeyId = this.ParseIntParameter(base.CommandParameters[0], "journeyId");
            IJourney journey = this.Repository.FindJourneyById(journeyId);

            var ticket = base.Repository.CreateTicket(journey, administrativeCosts);
            return $"Ticket with ID {ticket.Id} was created.";

        }
    }
}

