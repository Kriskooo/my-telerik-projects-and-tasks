﻿using System.Collections.Generic;
using Agency.Commands;
using Agency.Core;
using Agency.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agency.Tests.Commands
{
    [TestClass]
    public class CreateJourneyCommandTests
    {
        [TestMethod]
        [DataRow(4)]
        public void Execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected(int testValue)
        {
            Assert.ThrowsException<InvalidUserInputException>(() =>
            {
                var paramsList = new List<string>();
                for (int i = 0; i < testValue - 1; i++)
                {
                    paramsList.Add("");
                }
                var repository = new Repository();
                var command = new CreateJourneyCommand(paramsList, repository);
                command.Execute();
            });
        }
    }
}
