﻿using Agency.Exceptions;
using Agency.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agency.Tests
{
    [TestClass]
    public class TicketTests
    {
        [TestMethod]
        [DataRow(double.MinValue)]
        public void Constructor_Should_ThrowException_When_CostsAreNegative(double testValue)
        {
            Assert.ThrowsException<InvalidUserInputException>(() =>
            {
                _ = new Ticket(
                    id: 1,
                    journey: Helpers.GetTestJourney(),
                    administrativeCosts: testValue);
            });
        }
    }
}
