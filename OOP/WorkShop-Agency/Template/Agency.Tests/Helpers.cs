﻿using Agency.Models.Contracts;
using Agency.Models;

namespace Agency.Tests
{
    public class Helpers
    {
        public static IVehicle GetTestVehicle()
        {
            return (IVehicle)new Bus(
                    id: 1,
                    passengerCapacity: Bus.PassengerCapacityMinValue,
                    pricePerKilometer: Bus.PricePerKilometerMinValue,
                    hasFreeTv: true);
        }

        public static IJourney GetTestJourney()
        {
            return (IJourney)new Journey(
                    id: 1,
                    from: new string('x', Journey.StartLocationMinLength),
                    to: new string('x', Journey.DestinationMinLength),
                    distance: Journey.DistanceMinValue,
                    vehicle: Helpers.GetTestVehicle());

        }
    }
}
