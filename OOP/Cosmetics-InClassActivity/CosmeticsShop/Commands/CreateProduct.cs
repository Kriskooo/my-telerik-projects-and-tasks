﻿using CosmeticsShop.Core;
using CosmeticsShop.Models;
using CosmeticsShop.MyExceptions;
using System;
using System.Collections.Generic;

namespace CosmeticsShop.Commands
{
    public class CreateProduct : ICommand
    {
        private readonly CosmeticsRepository cosmeticsRepository;

        public CreateProduct(CosmeticsRepository productRepository)
        {
            this.cosmeticsRepository = productRepository;
        }

        public string Execute(List<string> parameters)
        {
            if (parameters.Count != 4)
            {
                throw new ParameterCountExceptions("CreateProduct command expects 4 parameters.");
            }
            string name = parameters[0];
            string brand = parameters[1];
            double price;

            try
            {
                price = double.Parse(parameters[2]);

            }
            catch (Exception)
            {

                throw new PriceFormatExceptions("Third parameter should be price (real number).");
            }

            GenderType gender;
            try
            {
                gender = Enum.Parse<GenderType>(parameters[3], true);

            }
            catch (Exception)
            {
                throw new GenderExceptions("Forth parameter should be one of Men, Women or Unisex.");
            }

            if (this.cosmeticsRepository.ProductExist(name))
            {
                throw new ArgumentException($"Product {name} already exist.");
            }

            this.cosmeticsRepository.CreateProduct(name, brand, price, gender);

            return $"Product with name {name} was created!";
        }
    }
}
