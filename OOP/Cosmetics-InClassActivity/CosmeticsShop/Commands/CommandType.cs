﻿namespace CosmeticsShop.Commands
{
    public enum CommandType
    {
        CREATECATEGORY,
        CREATEPRODUCT,
        ADDPRODUCTTOCATEGORY,
        SHOWCATEGORY,
    }
}
