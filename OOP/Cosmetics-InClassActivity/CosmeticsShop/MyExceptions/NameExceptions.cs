﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class NameExceptions : Exception
    {
        public NameExceptions(string message)
            : base(message)
        {

        }
    }
}
