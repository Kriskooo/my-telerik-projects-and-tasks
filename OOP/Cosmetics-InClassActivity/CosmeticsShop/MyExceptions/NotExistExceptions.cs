﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class NotExistExceptions : Exception
    {
        public NotExistExceptions(string message)
            : base(message)
        {

        }
    }
}
