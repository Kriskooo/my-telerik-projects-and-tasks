﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class GenderExceptions : Exception
    {
        public GenderExceptions(string message)
            : base(message)
        {

        }
    }
}
