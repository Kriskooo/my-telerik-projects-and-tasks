﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class PriceFormatExceptions : Exception
    {
        public PriceFormatExceptions(string message)
            : base(message)
        {

        }
    }
}
