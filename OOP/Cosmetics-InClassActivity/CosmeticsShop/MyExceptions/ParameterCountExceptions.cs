﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class ParameterCountExceptions : Exception
    {
        public ParameterCountExceptions(string message)
            : base(message)
        {

        }
    }
}
