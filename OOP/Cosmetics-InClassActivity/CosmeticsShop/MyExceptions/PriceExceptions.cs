﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class PriceExceptions : Exception
    {
        public PriceExceptions(string message)
            : base(message)
        {

        }
    }
}
