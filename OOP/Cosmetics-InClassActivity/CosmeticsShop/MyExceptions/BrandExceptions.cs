﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class BrandExceptions : Exception
    {
        public BrandExceptions(string message)
            : base(message)
        {

        }
    }
}
