﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmeticsShop.MyExceptions
{
    public class CommandExceptions : Exception
    {
        public CommandExceptions(string message)
            :base(message)
        {

        }
    }
}
