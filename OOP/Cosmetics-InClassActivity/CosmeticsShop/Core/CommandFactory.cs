﻿using CosmeticsShop.Commands;
using CosmeticsShop.MyExceptions;
using System;

namespace CosmeticsShop.Core
{
    public class CommandFactory
    {
        public ICommand CreateCommand(string commandTypeValue, CosmeticsRepository productRepository)
        {
            CommandType commandType;
            try
            {
                commandType = Enum.Parse<CommandType>(commandTypeValue, true);

            }
            catch (Exception)
            {

                throw new CommandExceptions($"Command {commandTypeValue} is not supported.");
            }

            switch (commandType)
            {
                case CommandType.CREATECATEGORY:
                    return new CreateCategory(productRepository);
                case CommandType.CREATEPRODUCT:
                    return new CreateProduct(productRepository);
                case CommandType.ADDPRODUCTTOCATEGORY:
                    return new AddProductToCategory(productRepository);
                case CommandType.SHOWCATEGORY:
                    return new ShowCategory(productRepository);
                default:
                    throw new CommandExceptions($"Command {commandType} is not supported.");
            }
        }
    }
}
