﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.test.Models
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void Board_ShouldThrow_WhenItemExists()
        {
            var title = "This is a test title";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is a test description";

            //Act
            var issue = new Issue(title, description, dueDate);
            Board.AddItem(issue);

            Assert.ThrowsException<InvalidOperationException>(() => Board.AddItem(issue));
        }

        [TestMethod]
        public void Board_ShouldAdd_WhenItemUnique()
        {
            var title = "This is a test title";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is a test description";
            //var title2 = "Test Title 2";

            //Act
            var issue = new Issue(title, description, dueDate);
            //var issue2 = new Issue(title2, description, dueDate);

            Board.AddItem(issue);

            Assert.AreEqual(2, Board.TotalItems);
        }

        [TestMethod]
        public void Board_Shoul_ShowCorrectHistory()
        {
            var title = "This is a test title";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is a test description";

            //Act
            var issue = new Issue(title, description, dueDate);
            // var issue2 = new Issue(title, description, dueDate);

            Board.AddItem(issue);


            Assert.AreEqual(1, Board.TotalItems);
        }
    }
}
