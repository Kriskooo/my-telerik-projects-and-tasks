using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Boarder.test
{
    [TestClass]
    public class TaskTest
    {
        [TestMethod]
        public void AssignCorrectValues()
        {
            //Arrange
            var title = "This is a test title";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var status = Status.Todo;
            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
            Assert.AreEqual(assignee, sut.Assignee);
            Assert.AreEqual(dueDate, sut.DueDate);
            Assert.AreEqual(status, sut.Status);
        }

        public void TaskConstructor_ShouldThrow_WhenTitleIsNull()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.Title = null);
        }

        [TestMethod]
        public void TaskConstructor_ShouldThrow_WhenNullAssignee()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => task.Assignee = null);
        }

        [TestMethod]
        public void TaskConstructor_ShouldThrow_WhenTitleShort()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.Title = "Thi");
        }

        [TestMethod]
        public void TaskConstructor_ShouldThrow_WhenTitleLong()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.Title = "This is test title test test test test");
        }

        [TestMethod]
        public void TaskConstructor_ShouldThrow_WhenAssigneeShort()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.Assignee = "Tes");
        }

        [TestMethod]
        public void TaskConstructor_ShouldThrow_WhenAssigneeTooLong()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.Assignee = "Test user too long user name test test test");
        }

        [TestMethod]
        public void WhenAssigneeChanged_ShouldChangeCorrectly()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var newAssignee = "NewTestUser";

            //Act
            var task = new Task(title, assignee, dueDate);
            task.Assignee = newAssignee;

            Assert.AreEqual(newAssignee, task.Assignee);
        }

        [TestMethod]
        public void TaskConstructor_Should_ThrowWhenDateInPast()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);

            //Assert
            var exception = Assert.ThrowsException<ArgumentException>(() => task.DueDate = DateTime.Now.AddDays(-1));
        }

        [TestMethod]
        public void TaskDueDate_ShoudChange_WhenDateChangeIsValid()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);
            var newDueDate = Convert.ToDateTime("01-01-2031");
            //Assert
            task.DueDate = newDueDate;
            Assert.AreEqual(task.DueDate, newDueDate);
        }

        [TestMethod]
        public void TaskTitle_ShoudChange_WhenTitleChangeIsValid()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var task = new Task(title, assignee, dueDate);
            var newTitle = "New Test Title0;";
            //Assert
            task.Title = newTitle;
            Assert.AreEqual(task.Title, newTitle);
        }

        [TestMethod]
        public void Status_Should_Advance()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.InProgress;

            //Act
            var task = new Task(title, assignee, dueDate);
            task.AdvanceStatus();

            //Assert
            Assert.AreEqual(changedStatus, task.Status);
        }

        [TestMethod]
        public void Status_Should_NotAdvance()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.Verified;

            //Act
            var task = new Task(title, assignee, dueDate);
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();

            //Assert
            Assert.AreEqual(changedStatus, task.Status);
        }

        [TestMethod]
        public void Status_Should_Revert()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.Todo;
            //Act
            var task = new Task(title, assignee, dueDate);
            task.AdvanceStatus();
            task.AdvanceStatus();

            task.RevertStatus();
            task.RevertStatus();
            task.RevertStatus();
            task.RevertStatus();

            //Assert
            Assert.AreEqual(changedStatus, task.Status);
        }

        [TestMethod]
        public void ViewInfo_ShouldDisplayCorrectInfo()
        {
            var title = "This is a Test title";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var task = new Task(title, assignee, dueDate);
            string expected = $"Task: '{title}', [{task.Status}|{task.DueDate.ToString("dd-MM-yyyy")}] Assignee: {assignee}";
            Assert.AreEqual(expected, task.ViewInfo());
        }

    }
}
