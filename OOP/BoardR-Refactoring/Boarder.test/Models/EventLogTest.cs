﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.test.Models
{
    [TestClass]
    public class EventLogTest
    {
        public void EventLog_ShouldThrow_IfDescriptionNull()
        {
            string description = null;

            Assert.ThrowsException<ArgumentNullException>(() => new EventLog(description));
        }

        [TestMethod]
        public void EventLog_ShouldCreate_IfDescriptionCorrect()
        {
            var description = "This is test description";
            EventLog eventLog = new EventLog(description);
            var expected = $"[{DateTime.Now.ToString("yyyyMMdd|HH:mm:ss.ffff")}]{eventLog.Description}";
            Assert.AreEqual(expected, eventLog.ViewInfo());
        }

        [TestMethod]
        public void WhenCorrectInformationIsSet_EventLog_ShouldDisplayCorrectInformation()
        {
            var title = "This is a test title";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var sut = new Task(title, assignee, dueDate);

            string expected = $"[{DateTime.Now.ToString("yyyyMMdd|HH:mm:ss.ffff")}]Created Task: " + sut.ViewInfo();
            Assert.AreEqual(expected, sut.ViewHistory());
        }
    }
}
