﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.test.Models
{
    [TestClass]
    public class IssueTest
    {
        [TestMethod]
        public void Issue_Constructor_ShouldCreate()
        {
            //Arrange
            var title = "This is a test title";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is a test description";

            //Act
            var issue = new Issue(title, description, dueDate);

            //Assert
            Assert.IsInstanceOfType(issue, typeof(Issue));

        }

        [TestMethod]
        public void Issue_Constructor_ShouldNotCreate_WhenTitleIsNull()
        {
            //Arrange
            string title = null;
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is test description";

            //Act


            //Assert
            Assert.ThrowsException<ArgumentException>(() => new Issue(title, description, dueDate), "Please provide a non-empty name");
        }

        [TestMethod]
        public void Issue_Constructor_ShouldNotCreate_WhenDescriptionIsNull()
        {
            //Arrange
            var title = "This is a test title";
            var dueDate = Convert.ToDateTime("01-01-2030");
            string description = null;
            Issue issue = new Issue(title, description, dueDate);
            var expected = "No desciption";
            //Act,Assert

            Assert.AreEqual(issue.Description, expected);
        }

        [TestMethod]
        public void Issue_Constructor_ShouldNotCreate_WhenTitleTooShort()
        {
            //Arrange
            var title = "Thi";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var description = "This is test description";

            //Act, //Assert

            Assert.ThrowsException<ArgumentException>(() => new Issue(title, description, dueDate), "Please provide a name with length between 5 and 30 chars");
        }

        [TestMethod]
        public void Status_Should_Advance()
        {
            //Arrange
            string title = "This is test title";
            var assignee = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.Verified;

            //Act
            var task = new Issue(title, assignee, dueDate);
            task.AdvanceStatus();

            //Assert
            Assert.AreEqual(changedStatus, task.Status);
        }

        [TestMethod]
        public void Status_Should_NotAdvance()
        {
            //Arrange
            string title = "This is test title";
            var description = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.Verified;

            //Act
            var task = new Issue(title, description, dueDate);
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();
            task.AdvanceStatus();

            //Assert
            Assert.AreEqual(changedStatus, task.Status);
        }

        [TestMethod]
        public void Status_Should_Revert()
        {
            //Arrange
            string title = "This is test title";
            var description = "Test User";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var changedStatus = Status.Open;
            //Act
            var issue = new Issue(title, description, dueDate);
            issue.AdvanceStatus();
            issue.AdvanceStatus();

            issue.RevertStatus();
            issue.RevertStatus();
            issue.RevertStatus();
            issue.RevertStatus();

            //Assert
            Assert.AreEqual(changedStatus, issue.Status);
        }

        [TestMethod]
        public void ViewInfo_ShouldDisplayCorrectInfo()
        {
            var title = "This is a Test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var issue = new Issue(title, description, dueDate);
            string expected = $"Issue: '{title}', [{issue.Status}|{issue.DueDate.ToString("dd-MM-yyyy")}] Description: {description}";
            Assert.AreEqual(expected, issue.ViewInfo());
        }

        [TestMethod]
        public void ViewHistory_ShouldDisplayCorrectInfo()
        {
            var title = "This is a Test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var issue = new Issue(title, description, dueDate);
            string expected = $"Issue: '{title}', [{issue.Status}|{issue.DueDate.ToString("dd-MM-yyyy")}] Description: {description}";
            Assert.AreEqual(expected, issue.ViewInfo());
        }
    }
}