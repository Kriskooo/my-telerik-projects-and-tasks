﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.Contracts
{
    public interface ILogger
    {
        public void Log(string value);
    }
}
