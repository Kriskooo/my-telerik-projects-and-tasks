﻿using System;

namespace Boarder.Models
{
    public class Issue : BoardItem
    {
        public Issue(string title, string description, DateTime dueDate)
            : base(title, dueDate, Status.Open)
        {
            this.Description = description ?? "No desciption";

            this.AddEventLog($"Created Issue: {this.ViewInfo()}. Description: {this.Description}");
        }
        public string Description { get; }

        public override void RevertStatus()
        {
            string message = string.Empty;

            if (base.Status > Status.Open)
            {
                base.Status = Status.Open;

                message = $"Issue status set to Open";
                this.AddEventLog(message);
            }
            else
            {
                message = "Issue status already at Open";
                this.AddEventLog(message);
            }
        }

        public override void AdvanceStatus()
        {
            string message = string.Empty;

            if (base.Status < Status.Verified)
            {
                base.Status = Status.Verified;

                message = $"Issue status set to Verified";
                this.AddEventLog(message);
            }
            else
            {
                message = "Issue status already Verified";
                this.AddEventLog(message);
            }

        }

        public override string ViewInfo()
        {
            var baseInfo = base.ViewInfo();
            return $"Issue: '{this.Title}', [{this.Status}|{this.DueDate.ToString(DateFormat)}] Description: {this.Description}";
        }


    }
}
