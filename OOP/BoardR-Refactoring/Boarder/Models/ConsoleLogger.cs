﻿using Boarder.Contracts;
using System;

namespace Boarder.Models
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string value)
        {
            Console.WriteLine(value);
        }
    }
}
