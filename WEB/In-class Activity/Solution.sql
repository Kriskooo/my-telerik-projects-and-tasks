-- If you don't have the Corporation database, 
--    use the Corporation.sql script to create it first.
-- Then complete the tasks below. Good luck!
-----------------------------------------------------------

-- 1. Write a SQL query to find all information about all departments.
SELECT *
FROM Departments

-- 2. Write a SQL query to find all department names.
SELECT [Departments].[Name]
FROM [Departments]

-- 3. Write a SQL query to find the salary of each employee.
SELECT Salary
FROM Employees

-- 4. Write a SQL to find the full name of each employee. 
--    Full name is constructed by joining first, middle and last name.
SELECT e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName
FROM Employees As e

SELECT CONCAT(e.FirstName, ' ', e.MiddleName, ' ', e.LastName)
FROM Employees AS e

SELECT CONCAT(e.FirstName, ' ', e.MiddleName, IIF(e.MiddleName IS NOT NULL, ' ', ''), e.LastName)
FROM Employees AS e

SELECT CONCAT_WS(' ', e.FirstName, e.MiddleName, e.LastName) AS FullName
FROM Employees AS e

-- 5. Write a SQL query to find the email addresses of each employee (by his first and last name). 
--    Consider that the mail domain is telerik.com. Emails should look like "John.Doe@telerik.com" / john.doe@telerik.com. 
--    The produced column should be named "Full Email Address".
SELECT CONCAT(LOWER(e.FirstName), '.', LOWER(e.LastName), '@telerik.com') as [Full Email Address]
FROM Employees AS e

-- 6. Write a SQL query to find all different employee salaries.
SELECT DISTINCT Salary
FROM Employees

-- 7. Write a SQL query to find all information about the employees whose job title is "Sales Representative" or "Sales Manager".
SELECT * 
FROM Employees
WHERE JobTitle = 'Sales Representative' OR JobTitle LIKE '% Sales Manager';

-- 8. Write a SQL query to find the names of all employees whose first name starts with "SA".
SELECT * 
FROM Employees
WHERE FirstName LIKE 'SA%'

-- 9. Write a SQL query to find the names of all employees whose last name contains "ei".
SELECT * 
FROM Employees
WHERE LastName LIKE '%ei%'

-- 10. Write a SQL query to find the salary of all employees whose salary is in the range [20000…30000].
SELECT *
FROM Employees
WHERE Salary <= 30000 AND Salary >= 20000

SELECT *
FROM Employees
WHERE Salary BETWEEN 20000 AND 30000
ORDER BY Salary

-- 11. Write a SQL query to find the names of all employees whose salary is 25000, 14000, 12500 or 23600.
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName, e.Salary
FROM Employees AS e
WHERE e.Salary IN (25000, 14000, 12500, 23600);
-- WHERE Salary = 25000 OR Salary = 14000 OR Salary = 12500 OR Salary = 23600

-- 12. Write a SQL query to find all employees that do not have manager.
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName
FROM Employees AS e
WHERE e.ManagerID IS NULL

-- 13. Write a SQL query to find all employees that have salary more than 50000. Order them in decreasing order by salary.
SELECT *
FROM Employees
WHERE Salary > 50000
ORDER BY Salary DESC

-- 14. Write a SQL query to find the top 5 best paid employees.
SELECT TOP 5 *
FROM Employees
ORDER BY Salary DESC

-- 15. Write a SQL query to find all employees along with their address. Use inner join with ON clause.
-- SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName, a.AddressText AS Address
SELECT *
FROM Employees AS e
JOIN Addresses AS a ON e.AddressID = a.AddressID

-- 16. Write a SQL query to find all employees and their address. Use equijoins (conditions in the WHERE clause).
SELECT *
FROM Employees AS e, Addresses AS a
WHERE e.AddressID = a.AddressID

-- 17. Write a SQL query to find all employees along with their manager.
SELECT employee.FirstName, employee.LastName, employee.ManagerId, manager.EmployeeID , manager.FirstName, manager.LastName
FROM Employees AS employee
INNER JOIN Employees AS manager ON employee.ManagerID = manager.EmployeeID

-- 18. Write a SQL query to find all employees, along with their manager and their address. 
--     Hint: Join Employees e, Employees m and Addresses a.
SELECT e.FirstName, e.LastName, m.FirstName, m.LastName, a.AddressText
FROM Employees AS e
JOIN Employees AS m ON e.ManagerID = m.EmployeeID
JOIN Addresses AS a ON e.AddressID = a.AddressID

SELECT e.FirstName, e.LastName, m.FirstName, m.LastName, a.AddressText
FROM Employees AS e, Employees AS m, Addresses AS a
WHERE e.AddressID = a.AddressID AND e.ManagerID	= m.EmployeeID

-- 19. Write a SQL query to find all departments and all town names as a single list. Use UNION.
SELECT Name
FROM Departments
UNION 
SELECT Name
FROM Towns

-- 20. Write a SQL query to find all the employees and the manager for each of them along with the employees that do not have manager. 
--     Use right outer join. Rewrite the query to use left outer join.

SELECT e.FirstName + ' ' + e.LastName AS E, m.FirstName + ' ' + m.LastName AS M
FROM Employees AS e
RIGHT JOIN Employees AS m ON e.EmployeeID = m.ManagerID

SELECT e.FirstName + ' ' + e.LastName, m.FirstName + ' ' + m.LastName
FROM Employees AS e
LEFT JOIN Employees AS m
ON e.ManagerID = m.EmployeeID

-- 21. Write a SQL query to find the names of all employees from the departments "Sales" and "Finance" whose hire year is between 1995 and 2005.
SELECT e.FirstName + ' ' + e.LastName, d.Name
FROM Employees AS e
JOIN Departments AS d
ON e.DepartmentID = d.DepartmentID
WHERE (e.HireDate BETWEEN '1995' AND '2006') AND (d.Name = 'Sales' OR d.Name = 'Finance')


-- 22. Write a SQL query to find the names and salaries of the employees that take the minimal salary in the company.
--     Hint: Use a nested SELECT statement.
SELECT e.FirstName + ' ' + e.LastName, e.Salary
FROM Employees AS e
WHERE e.Salary = (SELECT MIN(Salary) FROM Employees)

-- 23. Write a SQL query to find the names and salaries of the employees that have a salary that is up to 10% higher than the minimal salary for the company.
SELECT e.FirstName + ' ' + e.LastName, e.Salary
FROM Employees AS e
WHERE e.Salary <= (SELECT MIN(Salary) FROM Employees) * 1.1
ORDER BY e.Salary


-- 24. Write a SQL query to find the full name, salary and department of the employees that take the minimal salary in their department.
--     Hint: Use a nested SELECT statement.
SELECT e.FirstName + ' ' + e.LastName, e.Salary, d.Name
FROM Employees AS e
JOIN Departments AS d 
ON e.DepartmentID = d.DepartmentID
WHERE e.Salary = (
					SELECT MIN(e1.Salary)
					FROM Employees AS e1
					WHERE e1.DepartmentID = d.DepartmentID
				 )
ORDER BY e.Salary

-- 25. Write a SQL query to find the average salary in the department with id = 1.
SELECT ROUND(AVG(e.Salary), 0), d.Name
FROM Employees AS e
JOIN Departments AS d
ON e.DepartmentID = d.DepartmentID
WHERE e.DepartmentID = 1
GROUP BY d.Name

-- 26. Write a SQL query to find the average salary  in the "Sales" department.
SELECT AVG(e.Salary), d.Name
FROM Employees AS e
JOIN Departments as d ON e.DepartmentID = d.DepartmentID
WHERE d.Name = 'Sales'
GROUP BY d.Name

-- 27. Write a SQL query to find the number of employees in the "Sales" department.
SELECT COUNT(*)
FROM Employees AS e
JOIN Departments AS d ON e.DepartmentID = d.DepartmentID
WHERE d.Name = 'Sales'

-- 28. Write a SQL query to find the number of all employees that have manager.
SELECT COUNT(*)
FROM Employees AS e
WHERE e.ManagerID IS NOT NULL

-- 29. Write a SQL query to find the number of all employees that have no manager.
SELECT COUNT(*)
FROM Employees AS e
WHERE e.ManagerID IS NULL

-- 30. Write a SQL query to find all departments and the average salary for each of them.
SELECT d.Name, ROUND(AVG(e.Salary), 0) AS Salary
FROM Employees AS e
JOIN Departments AS d
ON e.DepartmentID = d.DepartmentID
GROUP BY d.Name
ORDER BY Salary DESC

-- 31. Write a SQL query to find the count of all employees in each department and for each town.
SELECT COUNT(*), d.Name, t.Name
FROM Employees AS e
JOIN Departments AS d
 ON e.DepartmentID = d.DepartmentID
JOIN Addresses AS a
 ON e.AddressID = a.AddressID
JOIN Towns AS t
 ON t.TownID = a.TownID
GROUP BY t.Name, d.Name


-- 32. Write a SQL query to find all managers that have exactly 5 employees. Display their first name and last name.
SELECT m.ManagerID
FROM Employees AS m
GROUP BY m.ManagerID
HAVING COUNT(*) = 5;

SELECT m.FirstName+' '+ m.LastName AS 'Name', m.EmployeeID
FROM Employees AS e
JOIN Employees AS m
ON e.ManagerID = m.EmployeeID
GROUP BY m.FirstName, m.LastName, m.EmployeeID
HAVING COUNT(e.ManagerID) = 5

SELECT m.FirstName+' '+ m.LastName AS 'Name'
FROM Employees AS e 
INNER JOIN Employees AS m
ON e.ManagerID=m.EmployeeID
GROUP BY  m.FirstName,m.LastName
HAVING COUNT(e.ManagerID)=5

-- 33. Write a SQL query to find all employees along with their managers. 
--     For employees that do not have manager display the value "(no manager)".
SELECT	e.FirstName + ' ' + e.LastName,
		CASE
			WHEN e.ManagerID IS NULL
			THEN '(no manager)'
			ELSE m.FirstName + ' ' + m.LastName
		END
FROM Employees AS e
LEFT JOIN Employees AS m
ON e.ManagerID = m.EmployeeID


SELECT	e.FirstName + ' ' + ISNULL(e.MiddleName + ' ' + e.LastName, e.LastName) as 'Employee',
		ISNULL(m.FirstName + ' ' + m.LastName, 'no manager') as 'Manager'
FROM Employees as e
LEFT JOIN Employees as m
ON e.ManagerID = m.EmployeeID



-- 34. Write a SQL query to find the names of all employees whose last name is exactly 5 characters long. 
--     Hint: Use the built-in LEN(str) function.
SELECT e.FirstName, e.LastName
FROM Employees AS e
WHERE LEN(e.LastName) = 5


-- 35. Write a SQL query to display the current date and time in the following format "day.month.year hour:minutes:seconds:milliseconds".
--     Hint: Search in Google to find how to format dates in SQL Server.
SELECT FORMAT(GETDATE(), 'dd.MM.yyyy HH:mm:ss:fff')


-- 36. Write a SQL statement to create a table Users. Users should have username, password, full name and last login time.
--     - Choose appropriate data types for the table fields. Define a primary key column with a primary key constraint.
--     - Define the primary key column as identity to facilitate inserting records.
--     - Define unique constraint to avoid repeating usernames.
--     - Define a check constraint to ensure the password is at least 5 characters long.
CREATE TABLE Users (
		UserID INT IDENTITY NOT NULL,
		UserName NVARCHAR(255) UNIQUE NOT NULL,
		Password NVARCHAR(255) NOT NULL,
		CHECK (LEN(Password) >= 5),
		FullName NVARCHAR(255) NOT NULL,
		LastLoginTime DATETIME ,
		CONSTRAINT PK_Users PRIMARY KEY CLUSTERED (UserID ASC))

-- 37. Write SQL statements to insert in the Users table the names of all employees from the Employees table.
--     - Combine the first and last names as a full name.
--     - For username use the first letter of the first name + the last name (in lowercase).
--     - Use the same for the password, and NULL for last login time.
INSERT INTO Users (Users.UserName, Users.Password, Users.FullName, Users.LastLoginTime)
SELECT	LOWER(CONCAT(SUBSTRING(e.FirstName, 1, 1), e.LastName, e.EmployeeID)),
		LOWER(CONCAT(SUBSTRING(e.FirstName, 1, 1), e.LastName, 'xxxxx')),
		e.FirstName + ' ' + e.LastName,
		NULL
FROM Employees AS e

SELECT * FROM Users ORDER BY UserName

-- 38. Write a SQL statement that changes the password to NULL for all users that have not been in the system since 10.03.2010.
UPDATE Users
SET Users.Password = NULL
WHERE Users.LastLoginTime < '10.03.2010'

-- 39. Write a SQL statement that deletes all users without passwords (NULL password).

-- 40. Write a SQL query to display the average employee salary by department and job title.
SELECT d.Name, e.JobTitle, AVG(e.Salary) AS Salary
FROM Employees AS e
JOIN Departments AS d
ON e.DepartmentID = d.DepartmentID
GROUP BY d.Name, e.JobTitle
ORDER BY d.Name, e.JobTitle
-- 41. Write a SQL query to display the minimal employee salary by department and job title along with the name of some of the employees that take it.
SELECT Temp.Name, Temp.JobTitle, Temp.Salary, e.FirstName, e.LastName
FROM 
		(
			SELECT d.Name, e.JobTitle, MIN(e.Salary) as 'Salary'
			FROM Employees AS e
			JOIN Departments AS d
			ON e.DepartmentID = d.DepartmentID
			GROUP BY d.Name, e.JobTitle
		) as Temp, 
	Employees as e
WHERE e.Salary = Temp.Salary AND e.JobTitle = Temp.JobTitle
ORDER BY Temp.Name


-- 42. Write a SQL query to display the town where maximal number of employees work.
SELECT TOP(1) t.Name, COUNT(*) AS [Number of employees]
FROM Employees AS e
JOIN Addresses AS a
 ON e.AddressID = a.AddressID
JOIN Towns AS t
 ON a.TownID = t.TownID
GROUP BY t.Name
ORDER BY COUNT(*) DESC

-- 43. Write a SQL query to display the number of managers from each town.
SELECT t.Name AS 'Town', COUNT(DISTINCT(m.EmployeeID)) AS '# of managers'
FROM Employees AS e
JOIN Employees AS m
 ON e.ManagerID = m.EmployeeID
JOIN Addresses AS a
 ON m.AddressID = a.AddressID
JOIN Towns AS t
 ON a.TownID = t.TownID
GROUP BY t.Name
