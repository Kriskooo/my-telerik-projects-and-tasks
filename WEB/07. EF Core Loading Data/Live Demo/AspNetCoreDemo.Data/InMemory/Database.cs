﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Data
{
    public class Database : IDatabase
    {
        // New Database instance is created for each new request
        // Make collection static so that it persists between requests
        private static readonly List<Beer> beers;

        static Database()
        {
            beers = new List<Beer>();

            SeedData();
        }

        public ICollection<Beer> Beers
        {
            get { return beers; }
        }

        public ICollection<Brewery> Breweries { get; }

        private static void SeedData()
        {
            beers.AddRange(new List<Beer>
            {
                new Beer
                {
                    BeerId = 1,
                    Name = "London Pride",
                    Abv = 4.7
                },
                new Beer
                {
                    BeerId = 2,
                    Name = "Frontier",
                    Abv = 4.2
                },
                new Beer
                {
                    BeerId = 3,
                    Name = "Honey Dew",
                    Abv = 4.8
                }
            });
        }
    }
}
