﻿using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AspNetCoreDemo.Data
{
    public interface IDatabase
    {
        DbSet<Beer> Beers { get; }

        DbSet<Brewery> Breweries { get; }
    }
}
