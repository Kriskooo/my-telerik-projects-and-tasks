﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreDemo.Data
{
    public static class ModelBuilderExtensios
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var brewery = new Brewery();
            brewery.BreweryId = 1;
            brewery.Name = "Carlsberg";

            var beers = new List<Beer>();
            beers.Add(new Beer()
            {
                BeerId = 1,
                Name = "Pirinsko",
                Abv = 4.5,
                BreweryId = 1
            });
            beers.Add(new Beer()
            {
                BeerId = 2,
                Name = "Shumensko",
                Abv = 5.0,
                BreweryId = 1
            });
            beers.Add(new Beer()
            {
                BeerId = 3,
                Name = "Burgasko",
                Abv = 5.5,
                BreweryId = 1
            });

            var users = new List<User>()
            {
                new User()
                {
                    UserId = 1,
                    Name = "Alice"
                },
                new User()
                {
                    UserId = 2,
                    Name = "Bob"
                }
            };

            var ratings = new List<Rating>()
            {
                new Rating()
                {
                    UserId = 1,
                    BeerId = 1,
                    Value = 5
                },
                new Rating()
                {
                    UserId = 2,
                    BeerId = 3,
                    Value = 4
                }
            };

            modelBuilder.Entity<Brewery>().HasData(brewery);
            modelBuilder.Entity<Beer>().HasData(beers);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Rating>().HasData(ratings);
        }
    }
}
