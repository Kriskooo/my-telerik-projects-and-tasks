﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreDemo.Data
{
    public class BeersDbContext : DbContext, IDatabase
    {
        public BeersDbContext(DbContextOptions<BeersDbContext> options)
            : base(options)
        {

        }

        public DbSet<Beer> Beers { get; set; }

        public DbSet<Brewery> Breweries { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Rating>().HasKey(r => new { r.BeerId, r.UserId });

            // FLUENT API
            // // 1 brewery has many beers
            // modelBuilder
            //     .Entity<Beer>()
            //     .HasOne(beer => beer.Brewery)
            //     .WithMany(brewery => brewery.Beers);
            //     
            // modelBuilder.Entity<Beer>().HasKey(beer => beer.BeerId);
            // modelBuilder.Entity<Beer>()
            //     .Property(beer => beer.Name)
            //     .IsRequired()
            //     .HasMaxLength(64);

            // TODO: Use extension method for seeding
            modelBuilder.Seed();
        }
    }
}
