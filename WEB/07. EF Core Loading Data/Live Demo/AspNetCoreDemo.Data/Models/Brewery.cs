﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Data.Models
{
    public class Brewery
    {
        [Key]
        public int BreweryId { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
