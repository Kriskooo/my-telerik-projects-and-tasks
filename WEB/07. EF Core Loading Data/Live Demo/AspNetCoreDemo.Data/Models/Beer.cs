﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Data.Models
{
    public class Beer
    {
        [Key]
        public int BeerId { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }


        public double Abv { get; set; } // alcohol by volume

        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }

        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
    }
}
