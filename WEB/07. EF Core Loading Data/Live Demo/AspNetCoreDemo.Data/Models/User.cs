﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreDemo.Data.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
    }
}
