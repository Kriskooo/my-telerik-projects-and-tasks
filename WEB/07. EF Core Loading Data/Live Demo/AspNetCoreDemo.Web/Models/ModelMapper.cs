﻿using AspNetCoreDemo.Data.Models;

namespace AspNetCoreDemo.Web.Models
{
    public class ModelMapper
    {
        public Beer ToModel(BeerWebModel beerWebModel)
        {
            return new Beer
            {
                BeerId = beerWebModel.Id,
                Name = beerWebModel.Name,
                Abv = beerWebModel.Abv
            };
        }
    }
}
