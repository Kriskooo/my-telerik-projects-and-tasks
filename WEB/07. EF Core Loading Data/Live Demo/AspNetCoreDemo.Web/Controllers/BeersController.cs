﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Services;
using AspNetCoreDemo.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;
        private readonly ModelMapper modelMapper;

        public BeersController(IBeerService beerService, ModelMapper modelMapper)
        {
            this.beerService = beerService;
            this.modelMapper = modelMapper;
        }

        // GET api/beers
        [HttpGet("")]
        public IActionResult Get()
        {
            var beers = this.beerService.GetAll();

            return this.Ok(beers);
        }

        // GET api/beers/:id
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var beer = this.beerService.Get(id);

                return this.Ok(beer);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        // POST api/beers
        [HttpPost("")]
        public IActionResult Post([FromBody] BeerWebModel beerWebModel)
        {
            if (beerWebModel == null)
            {
                return this.BadRequest();
            }

            var beer = this.modelMapper.ToModel(beerWebModel);
            var result = this.beerService.Create(beer);

            return this.Created("post", result);
        }

        // PUT api/beers/:id
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] BeerWebModel beerWebModel)
        {
            try
            {
                if (beerWebModel == null)
                {
                    return this.BadRequest();
                }

                var beer = this.modelMapper.ToModel(beerWebModel);
                var result = this.beerService.Update(id, beer);

                return this.Ok(result);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        // DELETE api/beers/:id
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.beerService.Delete(id);
                return this.NoContent();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
