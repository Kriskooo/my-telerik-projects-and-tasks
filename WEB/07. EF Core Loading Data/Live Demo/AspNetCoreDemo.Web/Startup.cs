using AspNetCoreDemo.Data;
using AspNetCoreDemo.Services.Services;
using AspNetCoreDemo.Web.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AspNetCoreDemo.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // *** DATABASE ***
            // TODO: Register BeersDbContext as a service
            // services.AddDbContext<BeersDbContext>();
            var connectionString = this.Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<IDatabase, BeersDbContext>(options => options.UseSqlServer(connectionString));

            // *** SERVICES ***
            // services.AddScoped<IDatabase, Database>();
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<IBeerService, BeerService>();
            services.AddScoped<ModelMapper>();

            // *** CONTROLLERS ***
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
