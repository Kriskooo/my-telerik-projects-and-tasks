﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services.Services
{
    public class BeerService : IBeerService
    {
        private readonly IDatabase dbContext;

        public BeerService(IDatabase database)
        {
            this.dbContext = database;
        }

        public Beer Get(int id)
        {
            var beer = this.dbContext.Beers.FirstOrDefault(x => x.BeerId == id);

            return beer ?? throw new EntityNotFoundException();
        }

        public IEnumerable<BeerDTO> GetAll()
        {
            var beers = this.dbContext
                            .Beers
                              .Include(beer => beer.Brewery)
                              .Include(beer => beer.Ratings)
                                .ThenInclude(rating => rating.User);

            var beerDTOs = new List<BeerDTO>();

            foreach (var beer in beers)
            {
                var dto = new BeerDTO();

                dto.Name = beer.Name;
                dto.ABV = beer.Abv;
                dto.Brewery = beer.Brewery.Name;

                foreach (var rating in beer.Ratings)
                {
                    dto.Ratings.Add(rating.User.Name, rating.Value);
                }

                beerDTOs.Add(dto);
            }


            return beerDTOs;
        }

        public Beer Create(Beer beer)
        {
            this.dbContext.Beers.Add(beer);

            return beer;
        }

        public Beer Update(int id, Beer beer)
        {
            var beerToUpdate = this.Get(id);

            beerToUpdate.Name = beer.Name;
            beerToUpdate.Abv = beer.Abv;

            return beerToUpdate;
        }

        public void Delete(int id)
        {
            var beer = this.Get(id);

            this.dbContext.Beers.Remove(beer);
        }
    }
}
