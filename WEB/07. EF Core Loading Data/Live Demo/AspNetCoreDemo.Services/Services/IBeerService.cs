﻿using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Services
{
    public interface IBeerService
    {
        Beer Get(int id);
        
        IEnumerable<BeerDTO> GetAll();
        
        Beer Create(Beer beer);
        
        Beer Update(int id, Beer beer);
        
        void Delete(int id);
    }
}
