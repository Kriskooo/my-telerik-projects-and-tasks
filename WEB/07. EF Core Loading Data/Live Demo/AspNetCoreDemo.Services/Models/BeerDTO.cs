﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreDemo.Services.Models
{
    public class BeerDTO
    {
        public string Name { get; set; }
        public string Brewery { get; set; }
        public double ABV { get; set; }
        public Dictionary<string, int> Ratings { get; set; } = new Dictionary<string, int>();
    }
}
