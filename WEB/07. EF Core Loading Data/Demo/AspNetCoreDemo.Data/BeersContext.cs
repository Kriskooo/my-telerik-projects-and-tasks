﻿using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreDemo.Data
{
    public class BeersContext : DbContext
    {
        public BeersContext(DbContextOptions<BeersContext> options)
            : base(options)
        {
        }

        public DbSet<Beer> Beers { get; set; }

        public DbSet<Brewery> Breweries { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Seed();
        }
    }
}
