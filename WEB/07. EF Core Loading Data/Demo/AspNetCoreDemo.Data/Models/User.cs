﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Data.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
    }
}
