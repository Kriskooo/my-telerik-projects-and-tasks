﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportProject.Models.Models
{
    public class Sport
    {
        [Key]
        public int Id { get; set; }
        public List<Event> Events { get; set; }
    }
}
