﻿using SportProject.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportProject.Models.Models
{
    public class Event : IConfigurations
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<Match> Matches { get; set; }
        public bool IsDeleted { get; set; }
    }
}
