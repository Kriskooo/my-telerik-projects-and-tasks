﻿using SportProject.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportProject.Models.Models
{
    public class Bet : IConfigurations
    {
        [Key]
        public int Id { get; set; }
        public float Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
