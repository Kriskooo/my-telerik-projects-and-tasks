﻿using Microsoft.EntityFrameworkCore;
using SportProject.Models.Contracts;
using SportProject.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SportsProject.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Sport> Sports { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Bet> Bets { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            this.ApplyBaseRules();
            return base.SaveChangesAsync();
        }

        private void ApplyBaseRules()
        {
            foreach (var entityToAdd in this.ChangeTracker.Entries()
                .Where(x => x.Entity is IConfigurations && (x.State == EntityState.Added)))
            {
                var currentEntity = (IConfigurations)entityToAdd.Entity;
                if (entityToAdd.State == EntityState.Added && currentEntity.CreatedOn == default(DateTime))
                {
                    currentEntity.CreatedOn = DateTime.UtcNow;
                }
            }
        }
    }
}
