﻿using SportProject.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportProject.Services.Contracts
{
    public interface IEventService
    {
        Task<EventDto> GetEvent(int id);
        Task<IEnumerable<EventDto>> GetAllEvents();
    }
}
