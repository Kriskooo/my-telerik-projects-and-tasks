﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportProject.Services.Dtos
{
    public class BetDto
    {
        public float Value { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
