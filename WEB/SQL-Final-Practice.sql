-- 1. Write a SQL query to find the top 5 most recently shipped orders. Display their ship name and shipped date.
USE Northwind

SELECT TOP (5)n.ShipperName, d.OrderDate
FROM Shippers n
JOIN Orders d
ON n.ShipperID = d.ShipperID
ORDER BY d.OrderDate DESC

-- Expected Result:

-- Shipper Name         Order Date
-- -------------------------------
-- Speedy Express       1997-02-12
-- United Package       1997-02-11
-- United Package       1997-02-10
-- United Package       1997-02-10
-- Federal Shipping     1997-02-07
--------------------------------------------------------------------------------------------------
-- 2. Write a SQL query to find the top 3 (ordered alphabetically by product name) products along with their category name.

SELECT TOP (3)n.ProductName AS Product, d.CategoryName AS Category
FROM Products n
JOIN Categories d
ON n.CategoryID = d.CategoryID
ORDER BY n.ProductName


-- Expected Result:

-- Product              Category
-- ---------------------------------
-- Alice Mutton         Meat/Poultry
-- Aniseed Syrup        Condiments
-- Boston Crab Meat     Seafood
--------------------------------------------------------------------------------------------------
-- 3. Write a SQL query to find all suppliers that have more than 3 products (ordered by SupplierID). 
--    Display product's supplier company name.

SELECT Suppliers.SupplierName AS 'Company'
FROM  Products
JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
GROUP BY
Products.SupplierID, Suppliers.SupplierName
HAVING COUNT(Products.SupplierID) > 3;

-- Expected Result:

-- Company
-- --------------------------
-- New Orleans Cajun Delights
-- Pavlova, Ltd.
-- Specialty Biscuits, Ltd.
-- Plutzer AG
--------------------------------------------------------------------------------------------------
-- 4. Write a SQL query to find top 5 customer companies arranged by their order total. 
--    Order total is the sum of the product's quantity multiplied by its price. 
--    Display the order id, company name order total.

SELECT TOP(5) o.OrderID, c.CustomerName AS Company, SUM(p.Price*d.Quantity) AS OrderTotal
FROM Customers c
JOIN Orders o
ON o.CustomerID = c.CustomerID
JOIN OrderDetails d
ON o.OrderID = d.OrderID
JOIN Products p
ON d.ProductID = p.ProductID
GROUP BY  o.OrderID, c.CustomerName
ORDER BY OrderTotal DESC

-- Expected Result:

-- OrderID  Company               OrderTotal
-- -------------------------------------------
-- 10372    Queen Cozinha         15353.60
-- 10424    Mere Paillarde        14366.50
-- 10417    Simons bistro         14104.00
-- 10353    Piccolo und mehr      13427.00
-- 10360    Blondel               9244.25
--------------------------------------------------------------------------------------------------
-- 5. Write a SQL query to display cities that have both customers and suppliers along with how many customers and how many suppliers are there.




-- Expected Result:

-- City			Customers	Suppliers
-- ----------------------------------
-- Berlin		1			1
-- Montreal		1			1
-- Paris		2			1
-- Sao Paulo	4			1