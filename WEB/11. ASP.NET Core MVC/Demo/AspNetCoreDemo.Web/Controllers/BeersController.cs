﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService beerService;

        public BeersController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        //GET: /beers
        public IActionResult Index()
        {
            var beers = this.beerService.GetAll();

            return this.View(beers);
        }

        //GET: /beers/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var beer = this.beerService.Get(id);

                return this.View(beer);
            }
            catch (EntityNotFoundException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Beer with id {id} does not exist.";
                return this.View("Error");
                
                // Use default page
                //return this.NotFound();
            }
        }
    }
}
