﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Services
{
    public interface IUserService
    {
        User Get(int id);

        User Get(string username);

        IEnumerable<User> GetAll();
    }
}
