﻿using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AspNetCoreDemo.Data
{
    public static class ModelBuilderExtensions
    {
        public static IEnumerable<Brewery> Breweries { get; }
        public static IEnumerable<Beer> Beers { get; }
        public static IEnumerable<Rating> Ratings { get; }
        public static IEnumerable<User> Users { get; }
        public static IEnumerable<UserRole> UserRoles { get; }
        public static IEnumerable<Role> Roles { get; }

        static ModelBuilderExtensions()
        {
            Breweries = new List<Brewery>
            {
                new Brewery
                {
                    Id = 1,
                    Name = "Carlsberg"
                },
                new Brewery
                {
                    Id = 2,
                    Name = "Heineken"
                }
            };

            Beers = new List<Beer>
            {
                new Beer
                {
                    Id = 1,
                    Name = "Carlsberg Smooth Lager",
                    Abv = 4.5,
                    BreweryId = 1
                },
                new Beer
                {
                    Id = 2,
                    Name = "Carlsberg Elephant",
                    Abv = 7.2,
                    BreweryId = 1
                },
                new Beer
                {
                    Id = 3,
                    Name = "Edelweiss",
                    Abv = 4.9,
                    BreweryId = 2
                },
                new Beer
                {
                    Id = 4,
                    Name = "Tiger",
                    Abv = 5.0,
                    BreweryId = 2
                }
            };

            Roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Admin"
                },
                new Role
                {
                    Id = 2,
                    Name = "User"
                }
            };

            Users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Name = "Alice"
                },
                new User
                {
                    Id = 2,
                    Name = "Bob"
                }
            };

            UserRoles = new List<UserRole>
            {
                new UserRole
                {
                    Id = 1,
                    UserId = 1, // Alice
                    RoleId = 1  // Admin
                },
                new UserRole
                {
                    Id = 2,
                    UserId = 2, // Bob
                    RoleId = 2, // User
                }
            };

            Ratings = new List<Rating>
            {
                // Alice's ratings
                new Rating
                {
                    Id = 1,
                    UserId = 1,
                    BeerId = 1,
                    Value = 3
                },
                new Rating
                {
                    Id = 2,
                    UserId = 1,
                    BeerId = 2,
                    Value = 5
                },
                new Rating
                {
                    Id = 3,
                    UserId = 1,
                    BeerId = 3,
                    Value = 4
                },
                new Rating
                {
                    Id = 4,
                    UserId = 1,
                    BeerId = 4,
                    Value = 2
                },
                // Bob's ratings
                new Rating
                {
                    Id = 5,
                    UserId = 2,
                    BeerId = 1,
                    Value = 3
                },
                new Rating
                {
                    Id = 6,
                    UserId = 2,
                    BeerId = 2,
                    Value = 1
                },
                new Rating
                {
                    Id = 7,
                    UserId = 2,
                    BeerId = 3,
                    Value = 4
                },
                new Rating
                {
                    Id = 8,
                    UserId = 2,
                    BeerId = 4,
                    Value = 5
                }
            };
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brewery>().HasData(Breweries);
            modelBuilder.Entity<Beer>().HasData(Beers);
            modelBuilder.Entity<Rating>().HasData(Ratings);
            modelBuilder.Entity<User>().HasData(Users);
            modelBuilder.Entity<UserRole>().HasData(UserRoles);
            modelBuilder.Entity<Role>().HasData(Roles);
        }
    }
}
