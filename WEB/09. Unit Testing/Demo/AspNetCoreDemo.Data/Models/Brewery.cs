﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AspNetCoreDemo.Data.Models
{
    public class Brewery
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        // The easiest way to configure a one-to-many relationship is by convention.
        // EF Core will create a relationship if an entity contains a navigation property.
        // Therefore, the minimum required for a relationship is the presence of a navigation property in the principal entity:
        [JsonIgnore]
        public ICollection<Beer> Beers { get; set; }
    }
}
