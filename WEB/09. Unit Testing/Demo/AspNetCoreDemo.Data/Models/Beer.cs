﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AspNetCoreDemo.Data.Models
{
    public class Beer
    {
        // If a property is named Id or <entity name>Id (not case-sensitive), it will be configured as the primary key.
        // In this case there is no need to set a [Key] attribute nor use the Fluent API.
        // Entity Framework Core will prefer Id over <entity name>Id in the event that a class contains both.
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        [Range(0, 35)]
        public double Abv { get; set; }

        // In order to define a one-to-many relationship by convention you have to include an inverse navigation property in the dependent entity.
        // In this case it is the Brewery property below since the Beer entity the dependent entity whereas Brewery is the principal entity.
        // In addition to the navigation property in the principal entity (see Brewery.cs), this approach will result in both ends of the relationship being specified.
        public int BreweryId { get; set; }
        // Finally, the relationship is considered to be fully defined when both ends of the navigation are present together with a foreign key property:
        public Brewery Brewery { get; set; }

        [JsonIgnore]
        public ICollection<Rating> Ratings { get; set; }
    }
}
