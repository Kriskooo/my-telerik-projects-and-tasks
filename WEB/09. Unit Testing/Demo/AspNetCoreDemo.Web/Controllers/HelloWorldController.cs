﻿using AspNetCoreDemo.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloWorldController : ControllerBase
    {
        private readonly IDateTimeProvider dateTimeProvider;

        public HelloWorldController(IDateTimeProvider dateTimeProvider)
        {
            this.dateTimeProvider = dateTimeProvider;
        }

        public string SayHello()
        {
            return $"Hello, World! Date is: {this.dateTimeProvider.GetDateTime()}";
        }
    }
}
