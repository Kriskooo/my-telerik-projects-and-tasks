﻿using AspNetCoreDemo.Services.Contracts;
using AspNetCoreDemo.Services.Models;
using AspNetCoreDemo.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AspNetCoreDemo.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;
        private readonly IAuthHelper authHelper;

        public BeersController(IBeerService beerService, IAuthHelper authHelper)
        {
            this.beerService = beerService;
            this.authHelper = authHelper;
        }

        //GET api/beers
        [HttpGet("")]
        public IActionResult Get()
        {
            var beers = this.beerService.GetAll();

            return this.Ok(beers);
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var beer = this.beerService.Get(id);

            return this.Ok(beer);
        }

        //POST api/beers
        [HttpPost("")]
        public IActionResult Post([FromHeader] string authorization, [FromBody] BeerDto model)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);
                var beer = this.beerService.Create(model);
                return this.Created("post", beer);
            }
            catch (Exception e)
            {
                return this.BadRequest(e.Message);
            }



        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string authorization, int id, string name)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);
                var updatedBeer = this.beerService.Update(id, name);
                return this.Ok(updatedBeer);
            }
            catch (Exception e)
            {
                return this.BadRequest(e.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string authorization, int id)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);
                //this.beerService.Delete(id);
                return this.NoContent();
            }
            catch (Exception e)
            {
                return this.BadRequest(e.Message);
            }
        }
    }
}
