﻿using AspNetCoreDemo.Data.Models;

namespace AspNetCoreDemo.Web.Helpers
{
    public interface IAuthHelper
    {
        User TryGetUser(string authorizationHeader);
    }
}
