﻿using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Contracts;
using System;

namespace AspNetCoreDemo.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public User TryGetUser(string authorizationHeader)
        {
            try
            {
                return this.userService.GetByUsername(authorizationHeader);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid username");
            }
        }
    }
}
