﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Contracts;
using AspNetCoreDemo.Web.Helpers;
using AspNetCoreDemo.Services;

namespace AspNetCoreDemo.Tests.Web
{
    [TestClass]
    public class TryGetUser_Should
    {
        [TestMethod]
        public void ReturnCorrectUser()
        {
            //Arrange
            var expectedUser = new User { Id = 1, Name = "TestUser" };
            
            var mockService = new Mock<IUserService>();
            mockService.SetupSequence(userService => userService.GetByUsername(It.IsAny<string>())).Returns(expectedUser);

            var sut = new AuthHelper(mockService.Object);

            //Act
            var actualUser = sut.TryGetUser("testHeaderValue");

            //Assert
            mockService.Verify(x => x.GetByUsername(It.IsAny<string>()), Times.Once());
            Assert.AreSame(expectedUser, actualUser);
        }
    }
}
