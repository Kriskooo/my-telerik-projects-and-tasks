﻿using Microsoft.EntityFrameworkCore;
using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Tests.Services
{
    public class Utils
    {
        public static DbContextOptions<BeersContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeersContext>()
                       .UseInMemoryDatabase(databaseName)
                       .Options;
        }

        public static IEnumerable<Beer> GetBeers()
        {
            return ModelBuilderExtensions.Beers;
        }

        public static IEnumerable<Brewery> GetBreweries()
        {
            return ModelBuilderExtensions.Breweries;
        }

        public static IEnumerable<Rating> GetRatings()
        {
            return ModelBuilderExtensions.Ratings;
        }

        public static IEnumerable<User> GetUsers()
        {
            return ModelBuilderExtensions.Users;
        }
    }

}
