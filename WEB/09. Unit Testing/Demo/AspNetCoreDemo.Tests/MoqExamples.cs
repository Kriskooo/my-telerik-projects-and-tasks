﻿using Moq;
using System;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Contracts;

namespace Tests
{
    class MoqExamples
    {
        //A list of some of the features Moq provides.
        //Check the quickstart guide for more: https://github.com/Moq/moq4/wiki/Quickstart
        public void Examples()
        {
            //Create a mock object
            var mockService = new Mock<IUserService>();


            //Setup Behavior
            mockService.Setup(x => x.GetByUsername("TestUser"))
                .Returns(new User { Id = 1, Name = "TestUser" });


            //Matching Arguments
            mockService.Setup(x => x.GetByUsername(It.IsAny<string>()))
                .Returns(new User { Id = 1, Name = "TestUser" });


            //Throw when invoking with specific arguments
            mockService.Setup(x => x.GetByUsername("Peter"))
               .Throws<InvalidOperationException>();


            //Setup sequential calls
            mockService.SetupSequence(x => x.GetByUsername(It.IsAny<string>()))
               .Returns(new User { Id = 1, Name = "TestUser1" })
               .Returns(new User { Id = 2, Name = "TestUser2" })
               .Returns(new User { Id = 3, Name = "TestUser3" })
               .Returns(new User { Id = 4, Name = "TestUser4" });


            //Verify a method was never called
            mockService.Verify(x => x.GetByUsername(It.IsAny<string>()), Times.Never());


            //Verify a method was called once
            mockService.Verify(x => x.GetByUsername(It.IsAny<string>()), Times.Once());
        }
    }
}
