﻿using AspNetCoreDemo.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AspNetCoreDemo.Tests.Services
{
    public abstract class BaseTest
    {
        [TestCleanup]
        public void Cleanup()
        {
            var options = Utils.GetOptions(nameof(TestContext.TestName));
            var context = new BeersContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
