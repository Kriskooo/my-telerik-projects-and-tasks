﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Services;
using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AspNetCoreDemo.Tests.Services
{
    [TestClass]
    public class GetBeer_Should : BaseTest
    {
        private DbContextOptions<BeersContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new BeersContext(this.options))
            {
                arrangeContext.Breweries.AddRange(Utils.GetBreweries());
                arrangeContext.Beers.AddRange(Utils.GetBeers());
                arrangeContext.Ratings.AddRange(Utils.GetRatings());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectBeer_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = new BeerDto
            {
                Id = 1,
                Name = "Carlsberg Smooth Lager",
                Abv = 4.5,
                BreweryId = 1,
                Brewery = "Carlsberg",
                AverageRating = 3, // 
                RatedBy = new string[] { "Alice", "Bob" }
            };

            // Act & Assert
            using (var assertContext = new BeersContext(this.options))
            {
                var sut = new BeerService(assertContext);
                var actualDto = sut.Get(1);

                Assert.AreEqual(expectedDto.Id, actualDto.Id);
                Assert.AreEqual(expectedDto.Name, actualDto.Name);
                Assert.AreEqual(expectedDto.Abv, actualDto.Abv);
                Assert.AreEqual(expectedDto.BreweryId, actualDto.BreweryId);
                Assert.AreEqual(expectedDto.Brewery, actualDto.Brewery);
                CollectionAssert.AreEquivalent(expectedDto.RatedBy, actualDto.RatedBy);
            }
        }

        [TestMethod]
        public void ThrowException_When_BeerNotFound()
        {
            // Act & Assert
            using (var assertContext = new BeersContext(this.options))
            {
                var sut = new BeerService(assertContext);
                const int nonExistentBeerId = int.MaxValue;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(nonExistentBeerId));
            }
        }
    }
}
