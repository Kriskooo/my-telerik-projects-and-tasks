﻿using System;
using System.Linq;
using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AspNetCoreDemo.Tests.Services
{
    [TestClass]
    public class DeleteBeer_Should : BaseTest
    {
        private DbContextOptions<BeersContext> options;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new BeersContext(this.options))
            {
                arrangeContext.Beers.AddRange(Utils.GetBeers());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void DeleteBeer_When_ParamsAreValid()
        {
            // Act & Assert
            using (var assertContext = new BeersContext(this.options))
            {
                var sut = new BeerService(assertContext);
                var isDeleted = sut.Delete(id: 1);
                Assert.IsTrue(isDeleted);
                var beer = assertContext.Find<Beer>(1);
                Assert.IsNull(beer);
            }
        }

        [TestMethod]
        public void ReturnsFalse_When_BeerNotFound()
        {
            // Act & Assert
            using (var assertContext = new BeersContext(this.options))
            {
                var sut = new BeerService(assertContext);
                var isDeleted = sut.Delete(id: int.MaxValue);
                Assert.IsFalse(isDeleted);
            }
        }
    }
}
