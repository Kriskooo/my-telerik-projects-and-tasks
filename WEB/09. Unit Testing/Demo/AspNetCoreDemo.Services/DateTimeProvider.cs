﻿using AspNetCoreDemo.Services.Contracts;
using System;

namespace AspNetCoreDemo.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.Now;
    }

}
