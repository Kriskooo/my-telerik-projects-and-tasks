﻿using System.Linq;
using System.Text.Json.Serialization;
using AspNetCoreDemo.Data.Models;

namespace AspNetCoreDemo.Services.Models
{
    public class BeerDto
    {
        public BeerDto()
        {

        }
        public BeerDto(Beer model)
        {
            this.Id = model.Id;

            this.Name = model.Name;
            
            this.Abv = model.Abv;

            this.BreweryId = model.BreweryId;

            this.Brewery = model.Brewery.Name;

            this.AverageRating = model.Ratings
                                    .Select(rating => rating.Value)
                                    .DefaultIfEmpty(0)
                                    .Average();

            this.RatedBy = model.Ratings.Select(rating => rating.User.Name).ToArray();
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
        public double Abv { get; set; }
        [JsonIgnore]
        public int BreweryId { get; set; }
        public string Brewery { get; set; }
        public double AverageRating { get; set; }
        public string[] RatedBy { get; set; }

    }
}
