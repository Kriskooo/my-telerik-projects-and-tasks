﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services
{
    public class UserService : IUserService
    {
        private readonly BeersContext context;
        public UserService(BeersContext context)
        {
            this.context = context;
        }

        public User Get(int id) => this.context.Users.FirstOrDefault(x => x.Id == id) ?? throw new ArgumentNullException();

        public IEnumerable<User> GetAll() => this.context.Users;

        public User GetByUsername(string username) => this.context.Users.FirstOrDefault(x => x.Name == username) ?? throw new ArgumentNullException();
    }
}
