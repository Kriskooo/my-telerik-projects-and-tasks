﻿using System;

namespace AspNetCoreDemo.Services.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }

}
