﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Contracts
{
    public interface IUserService
    {
        User Get(int id);
        IEnumerable<User> GetAll();
        User GetByUsername(string username);
    }
}
