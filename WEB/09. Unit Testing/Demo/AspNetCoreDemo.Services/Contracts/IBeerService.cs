﻿using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Contracts
{
    public interface IBeerService
    {
        BeerDto Get(int id);
        IEnumerable<BeerDto> GetAll();
        BeerDto Create(BeerDto beer);
        Beer Update(int id, string name);
        bool Delete(int id);
    }
}
