﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Models;
using AspNetCoreDemo.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services
{
    public class BeerService : IBeerService
    {
        private readonly BeersContext context;

        public BeerService(BeersContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        private IQueryable<Beer> BeersQuery
        {
            get
            {
                return this.context.Beers
                    .Include(beer => beer.Brewery)
                    .Include(beer => beer.Ratings)
                    .ThenInclude(rating => rating.User);
            }
        }

        public BeerDto Get(int id)
        {
            var model = this.BeersQuery.FirstOrDefault(b => b.Id == id);
            if (model == null)
            {
                throw new EntityNotFoundException();
            }

            var beer = new BeerDto(model);
            return beer;
        }

        public IEnumerable<BeerDto> GetAll()
        {
            var beers = this.BeersQuery.Select(beerModel => new BeerDto(beerModel));
            return beers;
        }

        public BeerDto Create(BeerDto beer)
        {
            if (beer == null)
            {
                throw new ArgumentNullException(nameof(beer));
            }

            var model = new Beer
            {
                Name = beer.Name,
                Abv = beer.Abv,
                BreweryId = beer.BreweryId
            };


            this.context.Beers.Add(model);
            this.context.SaveChanges();

            return new BeerDto(model);
        }

        public Beer Update(int id, string name)
        {
            var beer = this.context.Beers
                .FirstOrDefault(x => x.Id == id)
                ?? throw new ArgumentNullException();

            beer.Name = name;
            this.context.SaveChanges();

            return beer;
        }

        public bool Delete(int id)
        {
            var beer = this.context.Beers
                .FirstOrDefault(beer => beer.Id == id);

            if (beer == null)
            {
                return false;
            }

            this.context.Beers.Remove(beer);
            this.context.SaveChanges();
            return true;
        }
    }
}
