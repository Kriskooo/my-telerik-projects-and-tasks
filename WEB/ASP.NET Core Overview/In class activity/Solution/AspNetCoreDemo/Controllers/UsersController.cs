﻿using AspNetCoreDemo.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private static readonly List<User> users = new List<User>
        {
            new User
            {
                Id = 1,
                Name = "Bruce Banner",
                Email = "bruce@avengers.com"
            },
            new User
            {
                Id = 2,
                Name = "Tony Stark",
                Email = "tony@avengers.com"
            },
        };

        [HttpGet("")]
        public IActionResult Get()
        {
            return this.Ok(users);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = users.FirstOrDefault(user => user.Id == id);

            if (user == null)
            {
                return this.NotFound();
            }

            return this.Ok(user);
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] User user)
        {
            if (user == null)
            {
                return this.BadRequest();
            }

            users.Add(user);

            return this.Created("post", user);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] User user)
        {
            if (id < 1 || user == null)
            {
                return this.BadRequest();
            }

            var userToUpdate = users.FirstOrDefault(user => user.Id == id);

            if (userToUpdate == null)
            {
                return this.NotFound();
            }

            userToUpdate.Name = user.Name;
            userToUpdate.Email = user.Email;

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = users.FirstOrDefault(user => user.Id == id);

            if (user == null)
            {
                return this.NotFound();
            }

            users.Remove(user);

            return this.NoContent();
        }
    }
}
