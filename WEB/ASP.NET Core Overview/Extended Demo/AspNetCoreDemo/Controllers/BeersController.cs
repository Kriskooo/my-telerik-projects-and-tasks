﻿using AspNetCoreDemo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        // New BeerController instance is created for each new request
        // Make collection static so that it persists between requests
        private static readonly List<Beer> beers = new List<Beer>
        {
            new Beer
            {
                Id = 1,
                Name = "Glarus English Ale",
                Abv = 4.6
            },
            new Beer
            {
                Id = 2,
                Name = "Rhombus Porter",
                Abv = 5.0
            },
        };

        [HttpGet("")]
        public IActionResult Get(string name, double? minAbv, double? maxAbv, string sortBy, string order)
        {
            var result = beers;
            if (!string.IsNullOrEmpty(name))
            {
                result = result.FindAll(b => b.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase));
            }
            if (minAbv.HasValue)
            {
                result = result.FindAll(b => b.Abv >= minAbv);
            }
            if (maxAbv.HasValue)
            {
                result = result.FindAll(b => b.Abv <= maxAbv);
            }
            if (!string.IsNullOrEmpty(sortBy))
            {
                if (sortBy.Equals("name", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Name).ToList();
                }
                else if (sortBy.Equals("abv", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(b => b.Abv).ToList();
                }
                if (!string.IsNullOrEmpty(order) && order.Equals("desc", StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Reverse();
                }
            }
            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var beer = beers.FirstOrDefault(beer => beer.Id == id);

            if (beer == null)
            {
                return this.NotFound();
            }

            return this.Ok(beer);
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] Beer beer)
        {
            if (beer == null)
            {
                return this.BadRequest();
            }

            beers.Add(beer);

            return this.Created("post", beer);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Beer beer)
        {
            if (id < 1 || beer == null)
            {
                return this.BadRequest();
            }

            var beerToUpdate = beers.FirstOrDefault(beer => beer.Id == id);

            if (beerToUpdate == null)
            {
                return this.NotFound();
            }

            beerToUpdate.Name = beer.Name;
            beerToUpdate.Abv = beer.Abv;

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var beer = beers.FirstOrDefault(beer => beer.Id == id);

            if (beer == null)
            {
                return this.NotFound();
            }

            beers.Remove(beer);

            return this.NoContent();
        }
    }
}
