﻿using DeliverIT.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface ICityService
    {
        Task<GetCityResponseModel> GetCities();
    }
}
