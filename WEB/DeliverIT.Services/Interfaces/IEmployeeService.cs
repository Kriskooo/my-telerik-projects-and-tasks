﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IEmployeeService
    {       
        Task<RegisterEmployeeResponseModel> RegisterEmployeeAsync(RegisterEmployeeRequestModel requestModel);
        Task<DeleteEmployeeResponseModel> DeleteEmployeeAsync(DeleteEmployeeRequestModel requestModel);
        Task<UpdateEmployeeResponseModel> UpdateEmployeeAsync(UpdateEmployeeRequestModel requestModel);
    }
}
