﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IAccountService
    {
        Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel);
        Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestModel requestModel);
    }
}
