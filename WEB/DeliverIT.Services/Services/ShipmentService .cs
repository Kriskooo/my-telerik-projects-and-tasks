﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Helpers;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class ShipmentService : IShipmentService
    {
        private readonly DeliverITContext _context;

        public ShipmentService(DeliverITContext context)
        {
            _context = context;
        }

        public async Task<CreateShipmentResponseModel> CreateShipmentAsync(CreateShipmentRequestModel requestModel)
        {
            var result = new CreateShipmentResponseModel();

            var now = DateTime.UtcNow;
            if (requestModel.OriginWarehouse < 1 ||
                requestModel.DestinationWarehouse < 1 ||
                requestModel.DepartureTime < now ||
                requestModel.ArrivalTime < now ||
                requestModel.ArrivalTime < requestModel.DepartureTime)
            {
                result.IsSuccess = false;
                result.Message = Constants.SHIPMENT_WRONG_PARAMETERS;
                return result;
            }

            var statusPreparingId = await _context.ShipmentStatuses
                                                  .Where(p => p.Name == "Preparing")
                                                  .Select(p => p.Id)
                                                  .SingleAsync();

            var shipment = new Shipment()
            {
                OriginWarehouseId = requestModel.OriginWarehouse,
                DestinationWarehouseId = requestModel.DestinationWarehouse,
                DepartureDate = requestModel.DepartureTime,
                ArrivalTime = requestModel.ArrivalTime,
                StatusId = statusPreparingId
            };

            _context.Shipments.Add(shipment);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.SHIPMENT_CREATED;

            return result;
        }

        public async Task<UpdateShipmentResponseModel> UpdateShipmentAsync(UpdateShipmentRequestModel requestModel)
        {
            var now = DateTime.UtcNow;
            var result = new UpdateShipmentResponseModel();

            if (requestModel.OriginWarehouseId < 1 ||
                requestModel.DestinationWarehouseId < 1 ||
                requestModel.DepartureTime < now ||
                requestModel.ArrivalTime < now ||
                requestModel.ArrivalTime < requestModel.DepartureTime)
            {
                result.IsSuccess = false;
                result.Message = Constants.SHIPMENT_WRONG_PARAMETERS;
                return result;
            }

            var shipment = await _context.Shipments.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (requestModel.OriginWarehouseId != shipment.OriginWarehouseId)
            {
                shipment.OriginWarehouseId = requestModel.OriginWarehouseId;
            }
            if (requestModel.DestinationWarehouseId != shipment.DestinationWarehouseId)
            {
                shipment.DestinationWarehouseId = requestModel.DestinationWarehouseId;
            }
            if (requestModel.ArrivalTime != shipment.ArrivalTime)
            {
                shipment.ArrivalTime = requestModel.ArrivalTime;
            }
            if (requestModel.DepartureTime != shipment.DepartureDate)
            {
                shipment.DepartureDate = requestModel.DepartureTime;
            }
            if (requestModel.StatusId != shipment.StatusId)
            {
                shipment.StatusId = requestModel.StatusId;
            }
            await _context.SaveChangesAsync();
            result.IsSuccess = true;
            result.Message = $"Shipment with id {requestModel.Id} was updated.";

            return result;
        }

        public async Task<DeleteShipmentResponseModel> DeleteShipmentAsync(DeleteShipmentRequestModel requestModel)
        {
            var result = new DeleteShipmentResponseModel();

            var shipment = await _context.Shipments.FirstOrDefaultAsync(p => p.Id == requestModel.Id);
            if (shipment != null)
            {
                _context.Shipments.Remove(shipment);
                _context.SaveChanges();
                result.IsSuccess = true;
                result.Message = $"Shipment with ID: {requestModel.Id} was deleted.";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"Shipment with ID: {requestModel.Id} does not exist.";
            }

            return result;
        }

        public async Task<GetShipmentsByWarehouseResponseModel> GetShipmentByOriginWarehouseAsync(GetShipmentsByWarehouseRequestModel requestModel)
        {

            var result = new GetShipmentsByWarehouseResponseModel();
            if (requestModel.WarehouseId < 1)
            {
                result.IsSuccess = false;
                result.Message = Constants.WAREHOUSE_ID_NOT_EXISTS;
                return result;
            }
            var shipmentsCount = await (from w in _context.Warehouses
                                        join s in _context.Shipments on w.Id equals s.OriginWarehouseId into grp1
                                        from g1 in grp1.DefaultIfEmpty()
                                        where w.Id == requestModel.WarehouseId
                                        select g1.Id)
                                        .CountAsync();

            result.TotalCount = shipmentsCount;
            result.IsSuccess = true;

            if (shipmentsCount > 0)
            {
                result.Shipments = await (from w in _context.Warehouses
                                          join s in _context.Shipments on w.Id equals s.OriginWarehouseId into grp1
                                          from g1 in grp1.DefaultIfEmpty()
                                          where w.Id == requestModel.WarehouseId
                                          select new GetShipmentsResponseModel
                                          {
                                              ArrivalTime = g1.ArrivalTime,
                                              DepartureTime = g1.DepartureDate,
                                              OriginWarehouse = g1.OriginWarehouseId,
                                              DestinationWarehouse = g1.DestinationWarehouseId,
                                              StatusName = _context.ShipmentStatuses.Where(p => p.Id == g1.Id)
                                                                                    .Select(p => p.Name)
                                                                                    .FirstOrDefault()
                                          })
                                          .Skip(requestModel.Skip)
                                          .Take(requestModel.Take)
                                          .ToListAsync();
            }
            else
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_SHIPMENT;
            }
            return result;
        }

        public async Task<GetShipmentsByWarehouseResponseModel> GetShipmentByDestinationWarehouseAsync(GetShipmentsByWarehouseRequestModel requestModel)
        {
            var result = new GetShipmentsByWarehouseResponseModel();
            if (requestModel.WarehouseId < 1)
            {
                result.IsSuccess = false;
                result.Message = Constants.WAREHOUSE_ID_NOT_EXISTS;
                return result;
            }
            var shipmentsCount = await (from w in _context.Warehouses
                                        join s in _context.Shipments on w.Id equals s.DestinationWarehouseId into grp1
                                        from g1 in grp1.DefaultIfEmpty()
                                        where w.Id == requestModel.WarehouseId
                                        select g1.Id)
                                        .CountAsync();

            result.TotalCount = shipmentsCount;
            result.IsSuccess = true;

            if (shipmentsCount > 0)
            {


                result.Shipments = await (from w in _context.Warehouses
                                          join s in _context.Shipments on w.Id equals s.DestinationWarehouseId into grp1
                                          from g1 in grp1.DefaultIfEmpty()
                                          where w.Id == requestModel.WarehouseId
                                          select new GetShipmentsResponseModel
                                          {
                                              ArrivalTime = g1.ArrivalTime,
                                              DepartureTime = g1.DepartureDate,
                                              OriginWarehouse = g1.OriginWarehouseId,
                                              DestinationWarehouse = g1.DestinationWarehouseId,
                                              StatusName = _context.ShipmentStatuses.Where(p => p.Id == g1.Id)
                                                                                    .Select(p => p.Name)
                                                                                    .FirstOrDefault()
                                          })
                                          .Skip(requestModel.Skip)
                                          .Take(requestModel.Take)
                                          .ToListAsync();
            }
            else
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_SHIPMENT;
            }
            return result;
        }

        /// <summary>
        /// //////
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<GetShipmentsByCustomerResponseModel> GetShipmentByCustomerAsync(GetShipmentsByCustomerRequestModel requestModel)
        {
            var result = new GetShipmentsByCustomerResponseModel();
            if (requestModel.CustomerId < 1)
            {
                result.IsSuccess = false;
                result.Message = $"Customer with Id: {requestModel.CustomerId} does not exist";
                return result;
            }
            var shipmentsCount = await (from w in _context.Parcels
                                        join s in _context.Shipments on w.CustomerId equals s.Id into grp1
                                        from g1 in grp1.DefaultIfEmpty()
                                        where w.Id == requestModel.CustomerId
                                        select g1.Id)
                                        .CountAsync();
            if (shipmentsCount > 0)
            {

                result.TotalCount = shipmentsCount;
                result.IsSuccess = true;
                result.Shipments = await (from p in _context.Parcels
                                          join s in _context.Shipments on p.ShipmentId equals s.Id
                                          where p.CustomerId == requestModel.CustomerId
                                          select new GetShipmentsResponseModel
                                          {
                                              ArrivalTime = s.ArrivalTime,
                                              DepartureTime = s.DepartureDate,
                                              OriginWarehouse = s.OriginWarehouseId,
                                              DestinationWarehouse = s.DestinationWarehouseId,
                                              StatusName = _context.ShipmentStatuses.Where(x => x.Id == p.Id)
                                                                                        .Select(p => p.Name)
                                                                                        .FirstOrDefault()

                                          })
                                          .Skip(requestModel.Skip)
                                          .Take(requestModel.Take)
                                          .ToListAsync();
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"No shipment for customer with ID: {requestModel.CustomerId}";
            }

            return result;
        }
        /// <summary>
        /// ///////
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<GetShipmentsResponse> GetShipmentAsync(GetPaginatedRequestModel requestModel)
        {
            var result = new GetShipmentsResponse();
          
            var shipmentsCount = await _context.Shipments.CountAsync();
            if (shipmentsCount > 0)
            {
                result.TotalCount = shipmentsCount;
                result.IsSuccess = true;
                result.Data = await (from s in _context.Shipments
                                     select new GetShipmentsResponseModel
                                     {
                                         Id = s.Id, 
                                         ArrivalTime = s.ArrivalTime,
                                         DepartureTime = s.DepartureDate,
                                         OriginWarehouse = s.OriginWarehouseId,
                                         DestinationWarehouse = s.DestinationWarehouseId,
                                         StatusName = _context.ShipmentStatuses.Where(x => x.Id == s.StatusId)
                                                                                   .Select(p => p.Name)
                                                                                   .FirstOrDefault()
                                     })
                                     .Skip(requestModel.Skip)
                                     .Take(requestModel.Take)
                                     .ToListAsync();

                var paginationResult = PaginationHelper.CalculatePages(shipmentsCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;                
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"No shipments";
            }

            return result;
        }

        public async Task<GetShipmentResponseModel> GetShipmentByIdAsync(EditShipmentRequestModel requestModel)
        {
            var result = new GetShipmentResponseModel();

            result = await _context.Shipments
                               .Where(p => p.Id == requestModel.Id)
                               .Select(
                                s => new GetShipmentResponseModel
                                {
                                    Id = s.Id,
                                    ArrivalTime = s.ArrivalTime,
                                    DepartureTime = s.DepartureDate,
                                    OriginWarehouseId = s.OriginWarehouseId,
                                    DestinationWarehouseId = s.DestinationWarehouseId,
                                    StatusId = s.StatusId
                                })
                            .SingleOrDefaultAsync();

            var allShipmentStatuses = await _context.ShipmentStatuses.Select(p => new StatusResponseModel
                                                                     {
                                                                         Id = p.Id,
                                                                         Name = p.Name
                                                                     }).ToListAsync();

            result.PossibleStatuses = allShipmentStatuses;

            return result;
        }
    }
}
