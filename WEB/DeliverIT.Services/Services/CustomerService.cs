﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly DeliverITContext db;

        public CustomerService(DeliverITContext db)
        {
            this.db = db;
        }

        public async Task<DeleteCustomerResponseModel> DeleteCustomerAsync(DeleteCustomerRequestModel requestModel)
        {
            var responseModel = new DeleteCustomerResponseModel();
            var customer = await this.db.Customers.FirstOrDefaultAsync(c => c.Id == requestModel.Id);

            if (customer == null)
            {
                responseModel.Message = Constants.CUSTOMER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }

            else
            {
                responseModel.Message = Constants.CUSTOMER_DELETED;
                responseModel.IsSuccess = true;
                this.db.Customers.Remove(customer);
                this.db.SaveChanges();

            }
            return responseModel;
        }

        public async Task<UpdateCustomerResponseModel> UpdateCustomerAsync(UpdateCustomerRequestModel requestModel)
        {
            var responseModel = new UpdateCustomerResponseModel();
            var customer = await this.db.Customers.FirstOrDefaultAsync(c => c.Id == requestModel.CustomerId);

            if (customer == null)
            {
                responseModel.Message = Constants.CUSTOMER_NOT_FOUND;
                responseModel.IsSuccess = false;
                return responseModel;
            }

            else
            {
                if (!string.IsNullOrEmpty(requestModel.FirstName) && customer.FirstName != requestModel.FirstName)
                {
                    customer.FirstName = requestModel.FirstName;
                    responseModel.IsSuccess = true;
                }

                if (!string.IsNullOrEmpty(requestModel.LastName) && customer.LastName != requestModel.LastName)
                {
                    customer.LastName = requestModel.LastName;
                    responseModel.IsSuccess = true;
                }

                if (!string.IsNullOrEmpty(requestModel.Email) && customer.Email != requestModel.Email)
                {
                    customer.Email = requestModel.Email;
                    responseModel.IsSuccess = true;
                }

                if (requestModel.AddressId > 0 && customer.AddressId != requestModel.AddressId)
                {
                    customer.AddressId = requestModel.AddressId;
                    responseModel.IsSuccess = true;
                }

                await this.db.SaveChangesAsync();

                if (responseModel.IsSuccess)
                {
                    responseModel.Message = Constants.CUSTOMER_CHANGED;
                }
                else
                {
                    responseModel.Message = Constants.CUSTOMER_FOUND;
                    responseModel.IsSuccess = false;
                }
            }
            return responseModel;
        }
        
        public async Task<TotalCustomersResponseModel> TotalCustomersAsync()
        {
            var result = new TotalCustomersResponseModel();
            result.Customers = await db.Customers.CountAsync();
            return result;
        }
    }
}