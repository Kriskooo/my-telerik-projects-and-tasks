﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public class AccountService : IAccountService
    {
        public Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel)
        {
            throw new NotImplementedException();
        }

        public Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestModel requestModel)
        {
            throw new NotImplementedException();
        }
    }
}
