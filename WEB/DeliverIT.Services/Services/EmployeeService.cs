﻿using DeliverIT.Data;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using DeliverIT.Models.EntityModels;
using DeliverIT.Services.Utilities;
using DeliverIT.Models.Models;

namespace DeliverIT.Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly DeliverITContext context;
        public EmployeeService(DeliverITContext context)

        {
            this.context = context;
        }

        public async Task<RegisterEmployeeResponseModel> RegisterEmployeeAsync(RegisterEmployeeRequestModel requestModel)
        {
            var userExists = await _context.Users.AnyAsync(p => p.Username == requestModel.Username);
            var employeeExists = await _context.Employees.AnyAsync(p => p.Email == requestModel.Email);

            var result = new RegisterEmployeeResponseModel();

            if (userExists || employeeExists)
            {
                result.IsSuccess = false;
                result.Message = Constants.USER_CUSTOMER_ALREADY_EXISTS;
                return result;
            }

            var customerRoleId = await _context.Roles
                                               .Where(p => p.Name == "Employee")
                                               .Select(p => p.Id)
                                               .SingleAsync();
            var user = new User
            {
                Username = requestModel.Username,
                Password = requestModel.Password,
                RoleId = customerRoleId
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            var address = new Address();

            if (!string.IsNullOrEmpty(requestModel.CityName) &&
                !string.IsNullOrEmpty(requestModel.CountryName) &&
                !string.IsNullOrEmpty(requestModel.Address))
            {
                var countryIdResponse = await _context.Countries
                    .Where(p => p.Name == requestModel.CountryName)
                    .Select(p => new
                    {
                        ContryId = p.Id
                    })
                    .SingleOrDefaultAsync();

                var cityIdResponse = await _context.Cities
                   .Where(p => p.Name == requestModel.CityName)
                   .Select(p => new
                   {
                       CityId = p.Id
                   })
                   .SingleOrDefaultAsync();

               
                address.CityId = cityIdResponse.CityId;
                address.CountryId = countryIdResponse.ContryId;
                address.Street = requestModel.Address;                

                await _context.Addresses.AddAsync(address);
            }

            var userId = await _context.Users.Where(p => p.Username == requestModel.Username)
                                             .Select(p => p.Id)
                                             .FirstOrDefaultAsync();
            var employee = new Employee
            {
                Email = requestModel.Email,
                FirstName = requestModel.FirstName,
                LastName = requestModel.LastName,
                Address = address,
                UserId = userId
            };

            _context.Employees.Add(employee);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.CUSTOMER_REGISTERED;

            return result;
        }

        public async Task<DeleteEmployeeResponseModel> DeleteEmployeeAsync(DeleteEmployeeRequestModel requestModel)
        {
            var result = new DeleteEmployeeResponseModel();

            var employee = await _context.Employees.FirstOrDefaultAsync(c => c.Id == requestModel.Id);

            if (employee == null)
            {
                result.Message = Constants.CUSTOMER_NOT_FOUND;
                result.IsSuccess = false;
            }

            else
            {
                result.Message = Constants.CUSTOMER_DELETED;
                result.IsSuccess = true;
                _context.Employees.Remove(employee);
                _context.SaveChanges();

            }

            return result;
        }

        public async Task<UpdateEmployeeResponseModel> UpdateEmployeeAsync(UpdateEmployeeRequestModel requestModel)
        {
            var responseModel = new UpdateEmployeeResponseModel();
            var employee = await _context.Employees.FirstOrDefaultAsync(c => c.Id == requestModel.EmployeeId);

            if (employee == null)
            {
                responseModel.Message = Constants.CUSTOMER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(requestModel.FirstName) && requestModel.FirstName != employee.FirstName)
                {
                    employee.FirstName = requestModel.FirstName;
                    responseModel.IsSuccess = true;
                }

                if (!string.IsNullOrEmpty(requestModel.LastName) && employee.LastName != requestModel.LastName)
                {
                    employee.LastName = requestModel.LastName;
                    responseModel.IsSuccess = true;
                }

                if (!string.IsNullOrEmpty(requestModel.Email) && employee.Email != requestModel.Email)
                {
                    employee.Email = requestModel.Email;
                    responseModel.IsSuccess = true;
                }

                if (requestModel.AddressId > 0 && employee.AddressId != requestModel.AddressId)
                {
                    employee.AddressId = requestModel.AddressId;
                    responseModel.IsSuccess = true;
                }

                await _context.SaveChangesAsync();

                if (responseModel.IsSuccess)
                {
                    responseModel.Message = Constants.CUSTOMER_CHANGED;
                }
                else
                {
                    responseModel.Message = Constants.CUSTOMER_FOUND;
                    responseModel.IsSuccess = false;
                }
            }
            return responseModel;
        }
    }
}
