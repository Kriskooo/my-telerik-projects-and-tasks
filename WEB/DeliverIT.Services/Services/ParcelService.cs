﻿using DeliverIT.Data;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using DeliverIT.Models.EntityModels;
using System;
using DeliverIT.Services.Utilities;
using DeliverIT.Services.Helpers;

namespace DeliverIT.Services.Implementations
{
    public class ParcelService : IParcelService

    {
        private readonly DeliverITContext _context;
        public ParcelService(DeliverITContext context)

        {
            _context = context;
        }

        public async Task<CreateParcelResponseModel> CreateParcelAsync(CreateParcelRequestModel requestModel)
        {
            var result = new CreateParcelResponseModel();

            var now = DateTime.UtcNow;
            if (requestModel.WarehouseId < 1 ||
                requestModel.Weight < 1 ||
                requestModel.ShipmentId < 1 ||
                requestModel.CategoryId < 1 ||
                requestModel.CustomerId < 1)
            {
                result.IsSuccess = false;
                result.Message = Constants.PARCEL_WRONG_PARAMETERS;
                return result;
            }

            var parcel = new Parcel()
            {
                CustomerId = requestModel.CustomerId,
                CategoryId = requestModel.CategoryId,
                ShipmentId = requestModel.ShipmentId,
                WarehouseId = requestModel.WarehouseId,
                Weight = requestModel.Weight
            };

            _context.Parcels.Add(parcel);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.PARCEL_CREATED;

            return result;
        }

        public async Task<DeleteParcelResponseModel> DeleteParcelAsync(DeleteParcelRequestModel requestModel)
        {
            var result = new DeleteParcelResponseModel();

            var shipment = await _context.Parcels.FirstOrDefaultAsync(p => p.Id == requestModel.Id);
            if (shipment != null)
            {
                _context.Parcels.Remove(shipment);
                _context.SaveChanges();
                result.IsSuccess = true;
                result.Message = $"Parcel with ID: {requestModel.Id} was deleted.";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"Parcel with ID: {requestModel.Id} does not exist.";
            }

            return result;
        }

        public async Task<UpdateParcelResponseModel> UpdateParcelAsync(UpdateParcelRequestModel requestModel)
        {
            var now = DateTime.UtcNow;
            var result = new UpdateParcelResponseModel();
            if ((requestModel.WarehouseId.HasValue && requestModel.WarehouseId < 1) ||
                requestModel.Weight < 1 ||
                (requestModel.ShipmentId.HasValue && requestModel.ShipmentId < 1) ||
                requestModel.CategoryId.HasValue && requestModel.CategoryId < 1 ||
                requestModel.CustomerId.HasValue && requestModel.CustomerId < 1)
            {
                result.IsSuccess = false;
                result.Message = Constants.PARCEL_WRONG_PARAMETERS;
                return result;
            }
            var parcel = await _context.Parcels.FirstOrDefaultAsync(p => p.Id == requestModel.Id);
            if (parcel != null)
            {
                if (requestModel.CustomerId != parcel.CustomerId)
                {
                    parcel.CustomerId = requestModel.CustomerId;
                }
                if (requestModel.CategoryId.HasValue && requestModel.CategoryId != parcel.CategoryId)
                {
                    parcel.CategoryId = requestModel.CategoryId;
                }
                if (requestModel.WarehouseId != parcel.WarehouseId)
                {
                    parcel.WarehouseId = requestModel.WarehouseId;
                }
                if (requestModel.ShipmentId != parcel.ShipmentId)
                {
                    parcel.ShipmentId = requestModel.ShipmentId;
                }
                if (requestModel.Weight != parcel.Weight)
                {
                    parcel.Weight = requestModel.Weight;
                }
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"Shipment with id {requestModel.Id} was updated.";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"Shipment with id {requestModel.Id} does not exist.";
            }

            return result;
        }

        public async Task<FilterParcelResponseModel> FilterParcelAsync(FilterParcelRequesteModel requestModel)
        {
            var result = new FilterParcelResponseModel();
            if ((requestModel.CustomerId.HasValue && requestModel.CustomerId < 1) ||
               (requestModel.Weight.HasValue && requestModel.Weight < 1) ||
               requestModel.CustomerId < 1)
            {
                result.IsSuccess = false;
                result.Message = "Wrong parameters";
                return result;
            }

            var parcels = (from p in _context.Parcels
                           join c in _context.Categories on p.CategoryId equals c.Id
                           join s in _context.Shipments on p.ShipmentId equals s.Id
                           select new GetParcelResponseModel
                           {
                               Weight = p.Weight,
                               DeliveryToAddress = p.DeliveryToAddress,
                               CustomerId = p.CustomerId,
                               WarehouseId = p.WarehouseId,
                               CategoryName = c.Name,
                               ShipmentId = p.ShipmentId,
                               ArrivalDate = s.ArrivalTime
                           })
                           .AsQueryable();

            if (requestModel.Weight.HasValue)
            {
                parcels = parcels.Where(p => p.Weight == requestModel.Weight);
            }
            if (requestModel.WarehouseId.HasValue)
            {
                parcels = parcels.Where(p => p.WarehouseId == requestModel.WarehouseId);
            }
            if (requestModel.CustomerId.HasValue)
            {
                parcels = parcels.Where(p => p.CustomerId == requestModel.CustomerId);
            }
            if (!string.IsNullOrEmpty(requestModel.CategoryName))
            {
                parcels = parcels.Where(p => p.CategoryName == requestModel.CategoryName);
            }
            if (requestModel.OrderByWeight == true && requestModel.OrderByArrivalDate == true)
            {
                if (requestModel.OrderDescending == true)
                {
                    parcels = parcels.OrderByDescending(p => p.Weight).ThenByDescending(p => p.ArrivalDate);
                }
                else
                {
                    parcels = parcels.OrderBy(p => p.Weight).ThenBy(p => p.ArrivalDate);
                }
            }
            if (requestModel.OrderByArrivalDate == true)
            {
                if (requestModel.OrderDescending == true)
                {
                    parcels = parcels.OrderByDescending(p => p.ArrivalDate);
                }
                else
                {
                    parcels = parcels.OrderBy(p => p.ArrivalDate);
                }
            }
            if (requestModel.OrderByWeight == true)
            {
                if (requestModel.OrderDescending == true)
                {
                    parcels = parcels.OrderByDescending(p => p.Weight);
                }
                else
                {
                    parcels = parcels.OrderBy(p => p.Weight).ThenBy(p => p.ArrivalDate);
                }
            }

            result.IsSuccess = true;
            result.Parcels = await parcels.ToListAsync();

            return result;
        }

        public async Task<GetParcelsResponse> GetParcelAsync(GetParcelsRequestModel requestModel)
        {
            var result = new GetParcelsResponse();

            var parcelsCountQueryable = _context.Parcels.AsQueryable();

            result.IsSuccess = true;

            var resultQueryable = (from s in _context.Parcels
                                   join c in _context.Shipments on s.ShipmentId equals c.Id
                                   select new GetParcelResponseModel
                                   {
                                       Id = s.Id,
                                       Weight = s.Weight,
                                       DeliveryToAddress = s.DeliveryToAddress,
                                       CustomerId = s.CustomerId,
                                       WarehouseId = s.WarehouseId,
                                       CategoryName = s.Category.Name,
                                       ShipmentId = s.ShipmentId,
                                       ArrivalDate = c.ArrivalTime
                                   }).AsQueryable();


            if (requestModel.UserRole == Constants.CustomerRoleName)
            {
                var customerId = await (from c in _context.Customers
                                        join u in _context.Users on c.UserId equals u.Id
                                        where u.Id == requestModel.UserId
                                        select u.Id)
                                        .SingleOrDefaultAsync();

                parcelsCountQueryable = parcelsCountQueryable.Where(p => p.CustomerId == customerId);

                resultQueryable = resultQueryable.Where(p => p.CustomerId == customerId);

            }

            result.TotalCount = await parcelsCountQueryable.CountAsync();

            if (result.TotalCount > 0)
            {
                result.Data = await resultQueryable.Skip(requestModel.Skip)
                                                   .Take(requestModel.Take)
                                                   .ToListAsync();

                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;
            }

            else
            {
                result.IsSuccess = false;
                result.Message = $"No parcels";
            }

            return result;
        }

        public async Task<GetParcelResponseModel> GetParcelByIdAsync(EditParcelRequestModel requestModel)
        {
            var result = new GetParcelResponseModel();

            result = await (from s in _context.Parcels
                            join c in _context.Shipments on s.ShipmentId equals c.Id
                            select new GetParcelResponseModel
                            {
                                Id = s.Id,
                                Weight = s.Weight,
                                DeliveryToAddress = s.DeliveryToAddress,
                                CustomerId = s.CustomerId,
                                WarehouseId = s.WarehouseId,
                                CategoryName = s.Category.Name,
                                ShipmentId = s.ShipmentId,
                                ArrivalDate = c.ArrivalTime
                            })
                            .Where(s => s.Id == requestModel.Id)
                            .SingleOrDefaultAsync();

            var allCategories = await _context.Categories.Select(p => new CategoryResponseModel
            {
                Id = p.Id,
                Name = p.Name
            }).ToListAsync();
            result.PossibleStatuses = allCategories;
           
            return result;
        }
        public async Task<GetParcelsForCustomerResponseModel> GetParcelsAsync()
        {
            var result = new GetParcelsForCustomerResponseModel();

            var parcels = (from p in _context.Parcels
                           join c in _context.Customers on p.CustomerId equals c.Id
                           join a in _context.Users on c. equals a.user)
        }
    }
}
