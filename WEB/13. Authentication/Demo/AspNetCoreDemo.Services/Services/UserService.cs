﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services.Services
{
    public class UserService : IUserService
    {
        private readonly BeersContext context;

        public UserService(BeersContext context)
        {
            this.context = context;
        }

        public User Get(int id)
        {
            var user = this.context.Users
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .FirstOrDefault(x => x.Id == id);

            return user ?? throw new EntityNotFoundException();
        }

        public User Get(string username)
        {
            var user = this.context.Users
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .FirstOrDefault(x => x.Username.Equals(username));

            return user ?? throw new EntityNotFoundException();
        }

        public bool Exist(string username)
        {
            return this.context.Users.Any(x => x.Username.Equals(username));
        }

        public IEnumerable<User> GetAll()
        {
            return this.context.Users;
        }

        public User Create(User user)
        {
            this.context.Users.Add(user);
            this.context.SaveChanges();

            var userRole = new UserRole
            {
                UserId = user.Id,
                RoleId = 1
            };

            user.Roles.Add(userRole);
            this.context.Update<User>(user);

            return user;
        }
    }
}
