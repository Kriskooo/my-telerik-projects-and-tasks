﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Services
{
    public interface IUserService
    {
        User Get(int id);

        User Get(string username);

        bool Exist(string username);

        IEnumerable<User> GetAll();

        User Create(User user);
    }
}
