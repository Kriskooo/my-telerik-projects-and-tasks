﻿using AspNetCoreDemo.Services.Services;
using AspNetCoreDemo.Web.Exceptions;
using AspNetCoreDemo.Web.Helpers;
using AspNetCoreDemo.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthHelper authHelper;
        private readonly IUserService userService;
        private readonly ModelMapper modelMapper;

        public AuthController(IAuthHelper authHelper, IUserService userService, ModelMapper modelMapper)
        {
            this.authHelper = authHelper;
            this.userService = userService;
            this.modelMapper = modelMapper;
        }

        //GET: /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public IActionResult Login([Bind("Username, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = this.authHelper.TryGetUser(loginViewModel.Username, loginViewModel.Password);
                this.HttpContext.Session.SetString("CurrentUser", user.Username);

                return this.RedirectToAction("index", "home");
            }
            catch (AuthenticationException e)
            {
                this.ModelState.AddModelError("Username", e.Message);
                return this.View(loginViewModel);
            }
        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");

            return this.RedirectToAction("index", "home");
        }

        //GET: /auth/register
        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();

            return this.View(registerViewModel);
        }

        [HttpPost]
        public IActionResult Register([Bind("Username, Password, ConfirmPassword")] RegisterViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }

            if (this.userService.Exist(registerViewModel.Username))
            {
                this.ModelState.AddModelError("Username", "User with same username already exists.");
                return this.View(registerViewModel);
            }

            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(registerViewModel);
            }


            var user = this.modelMapper.ToModel(registerViewModel);
            this.userService.Create(user);

            return this.RedirectToAction(nameof(this.Login));
        }
    }
}
