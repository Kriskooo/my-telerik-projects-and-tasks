﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Services;
using AspNetCoreDemo.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace AspNetCoreDemo.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService beerService;
        private readonly ModelMapper modelMapper;

        public BeersController(IBeerService beerService, ModelMapper modelMapper)
        {
            this.beerService = beerService;
            this.modelMapper = modelMapper;
        }

        //GET: /beers
        public IActionResult Index()
        {
            var beers = this.beerService.GetAll();

            return this.View(beers);
        }

        //GET: /beers/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var beer = this.beerService.Get(id);

                return this.View(beer);
            }
            catch (EntityNotFoundException)
            {
                return this.BeerNotFound(id);

                // Use default page
                //return this.NotFound();
            }
        }

        //GET: /beers/create
        public IActionResult Create()
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var newBeerViewModel = new BeerViewModel();

            return this.BeerView(newBeerViewModel);
        }

        //POST: /beers/create
        [HttpPost]
        public IActionResult Create([Bind("Name,Abv,BreweryId")] BeerViewModel beerViewModel)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            if (!this.ModelState.IsValid)
            {
                return this.BeerView(beerViewModel);
            }

            var beer = this.modelMapper.ToModel(beerViewModel);
            this.beerService.Create(beer);

            return this.RedirectToAction(nameof(this.Index));
        }


        //GET: /beers/edit/:id
        public IActionResult Edit(int id)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            try
            {
                var beer = this.beerService.Get(id);
                var beerViewModel = this.modelMapper.ToViewModel(beer);

                return this.BeerView(beerViewModel);
            }
            catch (EntityNotFoundException)
            {
                return this.BeerNotFound(id);
            }
        }

        //POST: /beers/edit/:id
        [HttpPost]
        public IActionResult Edit(int id, [Bind("Name,Abv,BreweryId")] BeerViewModel beerViewModel)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            if (!ModelState.IsValid)
            {
                return this.BeerView(beerViewModel);
            }

            try
            {
                var beer = this.modelMapper.ToModel(beerViewModel);
                this.beerService.Update(id, beer);
            }
            catch (EntityNotFoundException)
            {
                return this.BeerNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        //GET: /beers/delete/:id
        public IActionResult Delete(int id)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            try
            {
                var beer = this.beerService.Get(id);

                return this.View(beer);
            }
            catch (EntityNotFoundException)
            {
                return this.BeerNotFound(id);
            }
        }

        //POST: /beers/delete/:id
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            try
            {
                this.beerService.Delete(id);
            }
            catch (EntityNotFoundException)
            {
                return this.BeerNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult BeerNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Beer with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult BeerView(BeerViewModel beerViewModel)
        {
            beerViewModel.Breweries = this.GetBreweries();
            return this.View(beerViewModel);
        }

        private SelectList GetBreweries()
        {
            var breweries = this.beerService.GetBreweries();
            return new SelectList(breweries, "Id", "Name");
        }
    }
}
