﻿using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Services.Services;
using AspNetCoreDemo.Web.Exceptions;

namespace AspNetCoreDemo.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private static readonly string ErrorMessage = "Invalid authentication info.";

        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public User TryGetUser(string username)
        {
            try
            {
                return this.userService.Get(username);
            }
            catch (EntityNotFoundException)
            {
                throw new AuthenticationException(ErrorMessage);
            }
        }

        public User TryGetUser(string username, string password)
        {
            var user = this.TryGetUser(username);
            if (user.Password != password)
            {
                throw new AuthenticationException(ErrorMessage);
            }
            return user;
        }
    }
}
