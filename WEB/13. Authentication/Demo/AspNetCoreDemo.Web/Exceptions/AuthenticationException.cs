﻿using System;

namespace AspNetCoreDemo.Web.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException()
        {
        }

        public AuthenticationException(string message)
            : base(message)
        {
        }
    }
}
