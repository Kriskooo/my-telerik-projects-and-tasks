﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace AspNetCoreDemo.Web.Models
{
    public class BeerViewModel : BeerWebModel
    {
        public SelectList Breweries { get; set; }
    }
}
