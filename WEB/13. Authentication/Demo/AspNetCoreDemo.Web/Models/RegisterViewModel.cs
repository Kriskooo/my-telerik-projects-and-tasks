﻿using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Web.Models
{
    public class RegisterViewModel : LoginViewModel
    {
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
