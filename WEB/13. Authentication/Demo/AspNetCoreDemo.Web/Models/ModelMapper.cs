﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Web.Models
{
    public class ModelMapper
    {
        public Beer ToModel(BeerWebModel beerWebModel)
        {
            return new Beer
            {
                Name = beerWebModel.Name,
                Abv = beerWebModel.Abv,
                BreweryId = beerWebModel.BreweryId
            };
        }

        public BeerViewModel ToViewModel(Beer beer)
        {
            return new BeerViewModel
            {
                Name = beer.Name,
                Abv = beer.Abv,
                BreweryId = beer.BreweryId
            };
        }

        public User ToModel(RegisterViewModel registerViewModel)
        {
            return new User
            {
                Username = registerViewModel.Username,
                Password = registerViewModel.Password
            };
        }
    }
}
