﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Data.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public ICollection<UserRole> Roles { get; set; } = new List<UserRole>();

        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
    }
}
