﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Data.Models
{
    public class Role
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        public ICollection<UserRole> Users { get; set; } = new List<UserRole>();
    }
}
