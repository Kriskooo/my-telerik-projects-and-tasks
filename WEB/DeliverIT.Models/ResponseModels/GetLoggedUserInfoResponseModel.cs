﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class GetLoggedUserInfoResponseModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserRoleName { get; set; }
    }
}
