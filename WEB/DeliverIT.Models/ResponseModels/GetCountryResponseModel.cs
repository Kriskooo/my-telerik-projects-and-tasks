﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class GetCountryResponseModel
    {
        public List<string> Countries { get; set; }
    }
}
