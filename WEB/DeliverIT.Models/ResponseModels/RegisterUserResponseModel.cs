﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class RegisterUserResponseModel
    {
        public string Username { get; set; }
        public string UserRoleName { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
