﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class GetCityResponseModel
    {
        public List<string> Cities { get; set; }
    }
}
