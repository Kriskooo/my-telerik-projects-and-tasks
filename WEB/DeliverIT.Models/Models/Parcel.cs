﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class Parcel
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public int Weight { get; set; }
        [DefaultValue(false)]
        public bool DeliveryToAddress { get; set; }
        [Required]
        [ForeignKey("Customer")]
        public long? CustomerId { get; set; }
        public Customer Customer { get; set; }
        [Required]
        [ForeignKey("Warehouse")]
        public int? WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
        [Required]
        [ForeignKey("Category")]
        public long? CategoryId { get; set; }
        public Category Category { get; set; }
        [Required]
        [ForeignKey("Shipment")]
        public long? ShipmentId { get; set; }
        public Shipment Shipment { get; set; }
    }
}
