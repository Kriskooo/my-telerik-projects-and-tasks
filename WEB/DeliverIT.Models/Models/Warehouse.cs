﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class Warehouse
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("Address")]
        public long AddressId { get; set; }
        public Address Address { get; set; }

        public virtual ICollection<Parcel> Parcels { get; set; }
        public virtual ICollection<Shipment> OriginShipments { get; set; }
        public virtual ICollection<Shipment> DestinationShipments { get; set; }
    }
}
