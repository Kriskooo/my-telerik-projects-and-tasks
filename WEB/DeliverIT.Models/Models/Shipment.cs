﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class Shipment
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [ForeignKey("Warehouse")]
        public int OriginWarehouseId { get; set; }
        public Warehouse OriginWarehouse { get; set; }
        [Required]
        [ForeignKey("Warehouse")]
        public int DestinationWarehouseId { get; set; }
        public Warehouse DestinationWarehouse { get; set; }
        [Required]
        public DateTime DepartureDate { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }
        [Required]
        [ForeignKey("ShipmentStatus")]
        public byte StatusId { get; set; }
        public ShipmentStatus Status { get; set; }
        public virtual ICollection<Parcel> Parcels { get; set; }
    }
}
