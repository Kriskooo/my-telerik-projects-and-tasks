﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class Address
    {
        [Key]
        public long Id { get; set; }
        public string Street { get; set; }
        [Required]
        [ForeignKey("City")]
        public long CityId { get; set; }
        public City City { get; set; }
        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Warehouse> Warehouses { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
