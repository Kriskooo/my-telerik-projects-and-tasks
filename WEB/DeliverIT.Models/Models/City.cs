﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class City
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }

    }
}
