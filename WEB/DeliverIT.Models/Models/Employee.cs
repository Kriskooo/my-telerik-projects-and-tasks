﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class Employee
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [ForeignKey("Address")]
        public long? AddressId { get; set; }
        public Address Address { get; set; }
        [Required]
        [ForeignKey("User")]
        public long UserId { get; set; }
    }
}

