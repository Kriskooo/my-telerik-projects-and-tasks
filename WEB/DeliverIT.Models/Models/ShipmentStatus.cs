﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Models.Models
{
    public class ShipmentStatus
    {
        [Key]
        public byte Id { get; set; }
        [Required]
        [RegularExpression("preparing|on the way|completed", ErrorMessage = "Invalid Status")]
        public string Name { get; set; }
        public virtual ICollection<Shipment> Shipments { get; set; }
    }
}
