﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.RequestModels
{
    public class RegisterUserRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string Address { get; set; }
    }
}
