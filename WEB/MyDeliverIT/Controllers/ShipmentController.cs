﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("/api/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Employee, Admin")]
    public class ShipmentController : Controller
    {
        private readonly IShipmentService _shipmentServices;

        public ShipmentController(IShipmentService shipmentServices)
        {
            _shipmentServices = shipmentServices;
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ShowShipments()
        {
            return View();
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ShowShipmentsMvc()
        {
            return View();
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> CreateShipment()
        {
            return View(new CreateShipmentRequestModel());
        }       

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] CreateShipmentRequestModel requestModel)
        {
            var result = await _shipmentServices.CreateShipmentAsync(requestModel);
           
            if (result.IsSuccess)
            {
                //return View("CreateShipment");
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> EditShipment([FromQuery] EditShipmentRequestModel requestModel)
        {
            var result = await _shipmentServices.GetShipmentByIdAsync(requestModel);
            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateShipment([FromForm] UpdateShipmentRequestModel requestModel)
        {
            var result = await _shipmentServices.UpdateShipmentAsync(requestModel);
            if (result.IsSuccess)
            {
                return RedirectToAction("ShowShipments");                
            }
            return BadRequest(result.Message);
        }
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateShipmentRequestModel requestModel)
        {
            var result = await _shipmentServices.UpdateShipmentAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        public async Task<IActionResult> GetShipmentsByWarehouseOrigin([FromQuery] GetShipmentsByWarehouseRequestModel requestModel)
        {
            var result = await _shipmentServices.GetShipmentByOriginWarehouseAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        // TODO : Filtration by Next arriving Shipment - Next arriving Bool -> Show all shipments if false / Show only next arriving if ture
        public async Task<IActionResult> GetShipmentsByWarehouseDestination([FromQuery] GetShipmentsByWarehouseRequestModel requestModel)
        {
            var result = await _shipmentServices.GetShipmentByDestinationWarehouseAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        public async Task<IActionResult> GetShipmentsByCustomer([FromQuery] GetShipmentsByCustomerRequestModel requestModel)
        {
            var result = await _shipmentServices.GetShipmentByCustomerAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpPost]
        public async Task<IActionResult> GetShipments(GetPaginatedRequestModel requestModel)
        {
            var result = await _shipmentServices.GetShipmentAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }       

        [HttpDelete]
        public async Task<IActionResult> Delete(DeleteShipmentRequestModel requestModel)
        {
            var result = await _shipmentServices.DeleteShipmentAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        //[HttpPost]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public async Task<IActionResult> GetShipmentsMvc(GetPaginatedRequestModel requestModel)
        //{
        //    var result = await _shipmentServices.GetShipmentAsync(requestModel);
        //    return PartialView("_ShipmentsTablePartial", result);
        //}

        //[HttpPost]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public async Task<IActionResult> DeleteMvc([FromForm] DeleteShipmentRequestModel requestModel)
        //{
        //    var result = await _shipmentServices.DeleteShipmentAsync(requestModel);
        //    if (result.IsSuccess)
        //    {
        //        return RedirectToAction("ShowShipmentsMvc");
        //    }
        //    return BadRequest(result.Message);
        //}
    }
}
