﻿using EFCoreDemo.Data.Models;
using System.Collections.Generic;

namespace EFCoreDemo.Services.Services
{
    public interface IBeerService
    {
        Beer Get(int id);
        IEnumerable<Beer> GetAll();
        Beer Create(Beer beer);
        Beer Update(int id, string name);
        void Delete(int id);
    }
}
