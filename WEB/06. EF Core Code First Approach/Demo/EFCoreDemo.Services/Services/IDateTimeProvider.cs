﻿using System;

namespace EFCoreDemo.Services.Services
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }

}
