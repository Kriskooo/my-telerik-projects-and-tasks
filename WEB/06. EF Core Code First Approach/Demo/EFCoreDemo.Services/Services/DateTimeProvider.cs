﻿using System;

namespace EFCoreDemo.Services.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.Now;
    }

}
