﻿using EFCoreDemo.Data.Context;
using EFCoreDemo.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace EFCoreDemo.Services.Services
{
	public class BeerService : IBeerService
	{
		private readonly BeersDbContext context;
		public BeerService(BeersDbContext context)
		{
			this.context = context;
		}
		public Beer Get(int id)
		{
			var beer = this.context.Beers
				.FirstOrDefault(x => x.Id == id)
				?? throw new ArgumentNullException();

			return beer;
		}

		public IEnumerable<Beer> GetAll()
		{
			var beers = this.context.Beers;

			return beers;
		}

		public Beer Create(Beer beer)
		{
			this.context.Beers.Add(beer);
			this.context.SaveChanges();

			return beer;
		}

		public Beer Update(int id, string name)
		{
			var beer = this.context.Beers
				.FirstOrDefault(x => x.Id == id)
				?? throw new ArgumentNullException();

			beer.Name = name;
			this.context.SaveChanges();

			return beer;
		}

		public void Delete(int id)
		{
			var beer = this.context.Beers
				.FirstOrDefault(beer => beer.Id == id)
				?? throw new ArgumentNullException();

			var outcome = this.context.Beers.Remove(beer);
			this.context.SaveChanges();
		}
	}
}
