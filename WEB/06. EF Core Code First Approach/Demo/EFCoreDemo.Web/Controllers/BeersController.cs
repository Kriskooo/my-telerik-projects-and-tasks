﻿using System;

using EFCoreDemo.Data.Models;
using EFCoreDemo.Services.Services;

using Microsoft.AspNetCore.Mvc;

namespace EFCoreDemo.Web.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class BeersController : ControllerBase
	{
		private readonly IBeerService beerService;

		public BeersController(IBeerService beerService)
		{
			this.beerService = beerService;
		}

		//GET api/beers
		[HttpGet("")]
		public IActionResult Get()
		{
			var beers = this.beerService.GetAll();

			return Ok(beers);
		}

		//GET api/beers/:id
		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			var beer = this.beerService.Get(id);

			return Ok(beer);
		}

		//POST api/beers
		[HttpPost("")]
		public IActionResult Post([FromBody] Beer model)
		{
			if (model == null)
			{
				return BadRequest();
			}

			var beer = this.beerService.Create(model);

			return Created("post", beer);
		}

		//PUT api/beers/:id
		[HttpPut("{id}")]
		public IActionResult Put(int id, string name)
		{
			var updatedBeer = this.beerService.Update(id, name);

			return Ok(updatedBeer);
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				this.beerService.Delete(id);
				return NoContent();
			}
			catch (Exception)
			{
				return BadRequest();
			}

		}

	}
}
