﻿using System.Collections.Generic;

using EFCoreDemo.Data.Models;

using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Data
{
	public static class ModelBuilderExtension
	{
		public static void Seed(this ModelBuilder modelBuilder)
		{
			var brewery = new Brewery()
			{
				Id = 1,
				Name = "Fuller's Brewery"
			};

			var beers = new List<Beer>
			{
				new Beer
				{
					Id = 1,
					Name = "London Pride",
					Abv = 4.7,
					BreweryId = 1
				},
				new Beer
				{
					Id = 2,
					Name = "Frontier",
					Abv = 4.2,
					BreweryId = 1
				},
				new Beer
				{
					Id = 3,
					Name = "Honey Dew",
					Abv = 4.8,
					BreweryId = 1
				}
			};

			modelBuilder.Entity<Brewery>().HasData(brewery);
			modelBuilder.Entity<Beer>().HasData(beers);
		}
	}
}
