﻿using EFCoreDemo.Data.Models;

using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Data.Context
{
	public class BeersDbContext : DbContext
	{
		public BeersDbContext(DbContextOptions<BeersDbContext> options)
			: base(options) { }

		public DbSet<Beer> Beers { get; set; }
		public DbSet<Brewery> Breweries { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<Rating> Ratings { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//EF Core Fluent API
			modelBuilder.Entity<Beer>().HasKey(beer => beer.Id);
			modelBuilder.Entity<Beer>().Property(beer => beer.Name)
				.IsRequired()
				.HasMaxLength(50);

			modelBuilder.Entity<Beer>().HasOne(beer => beer.Brewery)
				.WithMany(brewery => brewery.Beers)
				.OnDelete(DeleteBehavior.Cascade);

			modelBuilder.Entity<Rating>().HasKey(e => new { e.BeerId, e.UserId });

			modelBuilder.Seed();

			base.OnModelCreating(modelBuilder);
		}
	}
}
