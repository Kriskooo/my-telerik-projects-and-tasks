﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Data.Models
{
	public class User
	{
		[Key]
		public int Id { get; set; }
		[Required]
		[MinLength(3), MaxLength(128)]
		public string Name { get; set; }
		public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
	}
}
