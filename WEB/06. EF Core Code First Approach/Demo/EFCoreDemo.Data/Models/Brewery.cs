﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Data.Models
{
	public class Brewery
    {
        public int Id { get; set; }
        [Required]
        [MinLength(3), MaxLength(128)]
        public string Name { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
