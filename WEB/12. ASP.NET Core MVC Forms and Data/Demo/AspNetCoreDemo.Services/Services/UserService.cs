﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services.Services
{
    public class UserService : IUserService
    {
        private readonly BeersContext context;

        public UserService(BeersContext context)
        {
            this.context = context;
        }

        public User Get(int id)
        {
            var user = this.context.Users
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .FirstOrDefault(x => x.Id == id);

            return user ?? throw new EntityNotFoundException();
        }

        public User Get(string username)
        {
            var user = this.context.Users
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .FirstOrDefault(x => x.Name.Equals(username));

            return user ?? throw new EntityNotFoundException();
        }

        public IEnumerable<User> GetAll()
        {
            return this.context.Users;
        }
    }
}
