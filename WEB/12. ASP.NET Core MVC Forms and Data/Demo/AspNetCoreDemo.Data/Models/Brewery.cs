﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AspNetCoreDemo.Data.Models
{
    public class Brewery
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
