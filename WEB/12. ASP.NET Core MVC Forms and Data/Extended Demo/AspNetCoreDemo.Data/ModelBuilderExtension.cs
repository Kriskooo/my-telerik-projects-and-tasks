﻿using System.Collections.Generic;
using AspNetCoreDemo.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreDemo.Data
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var breweries = new List<Brewery>
            {
                new Brewery
                {
                    Id = 1,
                    Name = "Fuller's Brewery"
                },
                new Brewery
                {
                    Id = 2,
                    Name = "Guinness"
                },
                new Brewery
                {
                    Id = 3,
                    Name = "Murphy's"
                }
            };

            var beers = new List<Beer>
            {
                new Beer
                {
                    Id = 1,
                    Name = "London Pride",
                    Abv = 4.7,
                    BreweryId = 1
                },
                new Beer
                {
                    Id = 2,
                    Name = "Frontier",
                    Abv = 4.2,
                    BreweryId = 1
                },
                new Beer
                {
                    Id = 3,
                    Name = "Honey Dew",
                    Abv = 4.8,
                    BreweryId = 1
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "User"
                },
                new Role
                {
                    Id = 2,
                    Name = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Name = "Alice"
                },
                new User
                {
                    Id = 2,
                    Name = "Bob"
                }
            };

            var userRoles = new List<UserRole>
            {
                new UserRole
                {
                    Id = 1,
                    RoleId = 1,
                    UserId = 1
                },
                new UserRole
                {
                    Id = 2,
                    RoleId = 2,
                    UserId = 2
                }
            };

            var ratings = new List<Rating>
            {
                new Rating
                {
                    Id = 1,
                    UserId = 1,
                    BeerId = 1,
                    Value = 5
                },
                new Rating
                {
                    Id = 2,
                    UserId = 2,
                    BeerId = 1,
                    Value = 2
                }
            };

            modelBuilder.Entity<Role>().HasData(roles);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<UserRole>().HasData(userRoles);
            modelBuilder.Entity<Rating>().HasData(ratings);
            modelBuilder.Entity<Brewery>().HasData(breweries);
            modelBuilder.Entity<Beer>().HasData(beers);
        }
    }
}
