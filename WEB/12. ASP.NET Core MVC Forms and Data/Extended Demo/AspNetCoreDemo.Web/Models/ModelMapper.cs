﻿using AspNetCoreDemo.Data.Models;

namespace AspNetCoreDemo.Web.Models
{
    public class ModelMapper
    {
        public Beer ToModel(BeerWebModel beerWebModel)
        {
            return new Beer
            {
                Name = beerWebModel.Name,
                Abv = beerWebModel.Abv,
                BreweryId = beerWebModel.BreweryId
            };
        }

        public BeerViewModel ToViewModel(Beer beer)
        {
            return new BeerViewModel
            {
                Name = beer.Name,
                Abv = beer.Abv,
                BreweryId = beer.BreweryId
            };
        }
    }
}
