﻿using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{
    public class HomeController : Controller
    {
        //GET: /home/index or /home or /
        public IActionResult Index()
        {
            return View();
        }

        //GET: /home/about
        public IActionResult About()
        {
            return View();
        }
    }
}
