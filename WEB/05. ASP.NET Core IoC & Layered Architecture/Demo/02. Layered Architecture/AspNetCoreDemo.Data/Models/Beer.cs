﻿namespace AspNetCoreDemo.Data.Models
{
    public class Beer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Abv { get; set; }
    }
}
