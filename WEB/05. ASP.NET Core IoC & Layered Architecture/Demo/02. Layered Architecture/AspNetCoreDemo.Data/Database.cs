﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Data
{
    public static class Database
    {
        static Database()
        {
            Beers = new List<Beer>();

            SeedData();
        }

        public static List<Beer> Beers { get; }

        private static void SeedData()
        {
            Beers.AddRange(new List<Beer>
            {
                new Beer
                {
                    Id = 1,
                    Name = "London Pride",
                    Abv = 4.7
                },
                new Beer
                {
                    Id = 2,
                    Name = "Frontier",
                    Abv = 4.2
                },
                new Beer
                {
                    Id = 3,
                    Name = "Honey Dew",
                    Abv = 4.8
                }
            });
        }
    }
}
