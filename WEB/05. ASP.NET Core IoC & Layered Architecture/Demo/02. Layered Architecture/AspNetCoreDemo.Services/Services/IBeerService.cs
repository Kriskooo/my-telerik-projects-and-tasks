﻿using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services.Services
{
    public interface IBeerService
    {
        Beer Get(int id);
        
        IEnumerable<Beer> GetAll();
        
        Beer Create(Beer beer);
        
        Beer Update(int id, Beer beer);
        
        void Delete(int id);
    }
}
