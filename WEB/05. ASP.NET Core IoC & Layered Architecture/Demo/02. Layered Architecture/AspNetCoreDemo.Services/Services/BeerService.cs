﻿using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using AspNetCoreDemo.Services.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Services.Services
{
    public class BeerService : IBeerService
    {
        public Beer Get(int id)
        {
            var beer = Database.Beers.FirstOrDefault(x => x.Id == id);

            return beer ?? throw new EntityNotFoundException();
        }

        public IEnumerable<Beer> GetAll()
        {
            var beers = Database.Beers;

            return beers;
        }

        public Beer Create(Beer beer)
        {
            Database.Beers.Add(beer);

            return beer;
        }

        public Beer Update(int id, Beer beer)
        {
            var beerToUpdate = this.Get(id);

            beerToUpdate.Name = beer.Name;
            beerToUpdate.Abv = beer.Abv;

            return beerToUpdate;
        }

        public void Delete(int id)
        {
            var beer = this.Get(id);

            Database.Beers.Remove(beer);
        }
    }
}
