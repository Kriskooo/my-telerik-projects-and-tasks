﻿using System;

namespace AspNetCoreDemo.Services
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
