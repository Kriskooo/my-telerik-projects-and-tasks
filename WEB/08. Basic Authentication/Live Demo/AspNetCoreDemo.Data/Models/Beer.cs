﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AspNetCoreDemo.Data.Models
{
    public class Beer
    {
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(64)]
        public string Name { get; set; }

        [Range(0, 35)]
        public double Abv { get; set; }

        public int BreweryId { get; set; }

        public Brewery Brewery { get; set; }

        [JsonIgnore]
        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
    }
}
