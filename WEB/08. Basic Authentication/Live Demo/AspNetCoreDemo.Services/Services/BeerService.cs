﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Data;
using AspNetCoreDemo.Data.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreDemo.Services.Services
{
    public class BeerService : IBeerService
    {
        private readonly BeersContext context;

        public BeerService(BeersContext context)
        {
            this.context = context;
        }

        public Beer Get(int id)
        {
            /* Include allows us to query additional relational data based on navigation properties 
             * and internally uses SQL JOIN. Here the first include does a LEFT JOIN to fetch the Ratings
             * related to a specific beer. ThenInclude does an INNER JOIN to fetch the users who created the ratings.
             * 
             * Start the application and observe the generated SQL in the console. */
            var beer = this.context.Beers
                .Include(x => x.Brewery)
                .Include(x => x.Ratings)
                .ThenInclude(x => x.User)
                .FirstOrDefault(x => x.Id == id);

            // Explore this object in debug
            var changeTracker = this.context.ChangeTracker;

            return beer ?? throw new EntityNotFoundException();
        }

        public IEnumerable<Beer> GetAll()
        {
            var beers = this.context.Beers
                .Include(x => x.Brewery);

            return beers;
        }

        public Beer Create(Beer beer)
        {
            this.context.Beers.Add(beer);
            this.context.SaveChanges();

            return beer;
        }

        public Beer Update(int id, Beer beer)
        {
            var beerToUpdate = this.Get(id);

            beerToUpdate.Name = beer.Name;
            beerToUpdate.Abv = beer.Abv;

            this.context.SaveChanges();

            return beer;
        }

        public void Delete(int id)
        {
            var beer = this.Get(id);

            this.context.Beers.Remove(beer);
            this.context.SaveChanges();
        }

        public IEnumerable<Beer> GetByUserId(int userId)
        {
            var beers = this.context
                .Beers
                .Include(b => b.Ratings)
                .Where(b => b.Ratings.Any(r => r.UserId == userId)).ToList();

            return beers;
        }
    }
}
