﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Web.Models;
using AspNetCoreDemo.Services.Services;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreDemo.Web.Helpers;
using AspNetCoreDemo.Web.Exceptions;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System;
using AspNetCoreDemo.Data.Models;

namespace AspNetCoreDemo.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;
        private readonly IUserService userService;
        private readonly ModelMapper modelMapper;
        private readonly IAuthHelper authHelper;

        public BeersController(
            IBeerService beerService,
            IUserService userService,
            ModelMapper modelMapper,
            IAuthHelper authHelper)
        {
            this.beerService = beerService;
            this.userService = userService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }

        [HttpGet("login")]
        public IActionResult Login([FromHeader] string credentials)
        {
            // this is bad
            if(credentials == null)
                return this.Unauthorized();

            var user = this.authHelper.TryGetUser(credentials);
            if(user != null)
            {
                CookieOptions options = new CookieOptions();
                options.Expires = DateTime.Now.AddSeconds(30);
                this.Response.Cookies.Append("userId", user.Id.ToString(), options);
                return this.Ok("Login successful");
            }

            return this.Unauthorized();
        }

        //GET api/beers
        [HttpGet("")]
        public IActionResult Get()
        {
            if (this.Request.Cookies.ContainsKey("userId"))
            {
                var id = int.Parse(this.Request.Cookies["userId"]);
                var beers = this.beerService.GetByUserId(id);
                return this.Ok(beers);
            }


            return this.BadRequest("Login first");


            // var userName = this.Request.Headers["user"];
            // var user = this.authHelper.TryGetUser(userName);
            // var isAdmin = user.Roles.Any(r => r.Role.Name == "Admin");
            // 
            // this.Response.Cookies.Append("isAdmin", isAdmin.ToString());


            // var userName = this.Request.Headers["user"];
            // var user = this.authHelper.TryGetUser(userName);
            // var isAdmin = user.Roles.Any(r => r.Role.Name == "Admin");
            // if (isAdmin)
            // {
            //     var beers = this.beerService.GetAll();
            //     return this.Ok(beers);
            // }
            // else
            // {
            //     return this.Unauthorized();
            // }
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var beer = this.beerService.Get(id);

                return this.Ok(beer);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        //POST api/beers
        [HttpPost("")]
        public IActionResult Post([FromHeader] string authorization, [FromBody] BeerWebModel beerWebModel)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }

            var beer = this.modelMapper.ToModel(beerWebModel);
            var result = this.beerService.Create(beer);

            return this.Created("post", result);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string authorization, int id, [FromBody] BeerWebModel beerWebModel)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);

                var beer = this.modelMapper.ToModel(beerWebModel);
                var result = this.beerService.Update(id, beer);

                return this.Ok(result);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string authorization, int id)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);

                this.beerService.Delete(id);
                return this.NoContent();
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
