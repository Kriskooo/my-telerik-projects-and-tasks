﻿using System;

namespace AspNetCoreDemo.Services.Services
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
