﻿using AspNetCoreDemo.Services.Exceptions;
using AspNetCoreDemo.Web.Models;
using AspNetCoreDemo.Services.Services;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreDemo.Web.Helpers;
using AspNetCoreDemo.Web.Exceptions;

namespace AspNetCoreDemo.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;
        private readonly ModelMapper modelMapper;
        private readonly IAuthHelper authHelper;

        public BeersController(
            IBeerService beerService,
            ModelMapper modelMapper,
            IAuthHelper authHelper)
        {
            this.beerService = beerService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }

        //GET api/beers
        [HttpGet("")]
        public IActionResult Get()
        {
            var beers = this.beerService.GetAll();

            return this.Ok(beers);
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var beer = this.beerService.Get(id);

                return this.Ok(beer);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        //POST api/beers
        [HttpPost("")]
        public IActionResult Post([FromHeader] string authorization, [FromBody] BeerWebModel beerWebModel)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }

            var beer = this.modelMapper.ToModel(beerWebModel);
            var result = this.beerService.Create(beer);

            return this.Created("post", result);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string authorization, int id, [FromBody] BeerWebModel beerWebModel)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);

                var beer = this.modelMapper.ToModel(beerWebModel);
                var result = this.beerService.Update(id, beer);

                return this.Ok(result);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string authorization, int id)
        {
            try
            {
                this.authHelper.TryGetUser(authorization);

                this.beerService.Delete(id);
                return this.NoContent();
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
